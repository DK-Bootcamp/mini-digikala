<?php

namespace App\Logger;

use Monolog\Level;
use Monolog\Logger;

class BaseLogger
{
    /**
     * @var Logger
     */
    protected Logger $logger;

    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
        // $this->logger->pushHandler(new RotatingFileHandler('dev', 10, \Monolog\Level::Debug));
    }

    /**
     * @param string $level
     * @param string $message 
     * @param array  $context
     */
    public function log(string $level, string $message, array $context): void
    {
       $this->logger->addRecord($level, $message, $context);
    }


    /**
     * @param string $message
     * @param array  $context
     */
    public function logError(string $message, array $context = []): void
    {
        $this->log(Level::Error->value, $message, $context);
    }

    /**
     * @param string $message
     * @param array  $context
     */
    public function logWarning(string $message, array $context = []): void
    {
        $this->log(Level::Warning->value, $message, $context);
    }

    /**
     * @param string $message
     * @param array  $context
     */
    public function logNotice(string $message, array $context = []): void
    {
        $this->log(Level::Notice->value, $message, $context);
    }

    /**
     * @param string $message
     * @param array  $context
     */
    public function logInfo(string $message, array $context = []): void
    {
        $this->log(Level::Info->value, $message, $context);
    }
}