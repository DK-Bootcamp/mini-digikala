<?php

namespace App\Gateways;


use App\Abstracts\Gateways;
use App\Entity\Payment;
use App\Enum\PaymentStatus;
use App\Service\PaymentService;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Shetabit\Multipay\Exceptions\InvalidPaymentException;
use Shetabit\Multipay\Exceptions\PurchaseFailedException;

class IDPay extends Gateways
{
    protected object $client;

    private string $apiKey;

    public function __construct(
        private readonly PaymentService $paymentService,
        string                          $callbackUrl
    )
    {
        $this->apiPurchaseUrl = 'https://api.idpay.ir/v1.1/payment';
        $this->apiPaymentUrl = "https://idpay.ir/p/ws/";
        $this->apiSandboxPaymentUrl = "https://idpay.ir/p/ws-sandbox/";
        $this->apiVerificationUrl = "https://api.idpay.ir/v1.1/payment/verify";
        $this->callbackUrl = $callbackUrl;
        $this->apiKey = $_ENV['API_KEY'];
        $this->sandbox = 1;
        $this->client = new Client();

    }

    /**
     * @throws GuzzleException
     * @throws PurchaseFailedException
     */
    public function purchase(Payment $payment)
    {
        $data = array(
            'order_id' => $payment->getUuid(),
            'amount' => $payment->getAmount(),
            'callback' => $this->callbackUrl,
        );

        $response = $this->client->request(
            "POST",
            $this->apiPurchaseUrl,
            [
                "json" => $data,
                "headers" => [
                    'X-API-KEY' => $this->apiKey,
                    'Content-Type' => 'application/json',
                    'X-SANDBOX' => $this->sandbox,
                ],
                "http_errors" => false,
            ]
        );

        $body = json_decode($response->getBody()->getContents(), true);
        if (empty($body['id'])) {
            // error has happened
            $message = $body['error_message'] ?? 'خطا در هنگام درخواست برای پرداخت رخ داده است.';
            throw new PurchaseFailedException($message);
        }

        $payment->setTransactionId($body['id']);
        $payment->setStatus("InProgress");

        $this->paymentService->update($payment);
    }

    public function pay(Payment $payment): string
    {
        $apiUrl = $this->apiPaymentUrl;

        if (!empty($this->sandbox)) {
            $apiUrl = $this->apiSandboxPaymentUrl;
        }

        return $apiUrl . $payment->getTransactionId();
    }

    /**
     * @throws GuzzleException
     * @throws InvalidPaymentException
     */
    public function verify(Payment $payment): Payment
    {
        $data = [
            'id' => $payment->getTransactionId(),
            'order_id' => $payment->getUuid()
        ];

        $response = $this->client->request(
            "POST",
            $this->apiVerificationUrl,
            [
                "json" => $data,
                "headers" => [
                    'X-API-KEY' => $this->apiKey,
                    'Content-Type' => 'application/json',
                    'X-SANDBOX' => $this->sandbox
                ],
                "http_errors" => false,
            ]
        );
        $body = json_decode($response->getBody()->getContents(), true);

        if (isset($body['error_code']) || $body['status'] != 100) {
            $errorCode = $body['status'] ?? $body['error_code'];

            $this->notVerified($errorCode);
        }

        $payment->setStatus(PaymentStatus::Finished);
        return $this->paymentService->update($payment);
    }

    /**
     * @throws InvalidPaymentException
     */
    private function notVerified($status)
    {
        $translations = array(
            "1" => "پرداخت انجام نشده است.",
            "2" => "پرداخت ناموفق بوده است.",
            "3" => "خطا رخ داده است.",
            "4" => "بلوکه شده.",
            "5" => "برگشت به پرداخت کننده.",
            "6" => "برگشت خورده سیستمی.",
            "10" => "در انتظار تایید پرداخت.",
            "100" => "پرداخت تایید شده است.",
            "101" => "پرداخت قبلا تایید شده است.",
            "200" => "به دریافت کننده واریز شد.",
            "11" => "کاربر مسدود شده است.",
            "12" => "API Key یافت نشد.",
            "13" => "درخواست شما از {ip} ارسال شده است. این IP با IP های ثبت شده در وب سرویس همخوانی ندارد.",
            "14" => "وب سرویس تایید نشده است.",
            "21" => "حساب بانکی متصل به وب سرویس تایید نشده است.",
            "31" => "کد تراکنش id نباید خالی باشد.",
            "32" => "شماره سفارش order_id نباید خالی باشد.",
            "33" => "مبلغ amount نباید خالی باشد.",
            "34" => "مبلغ amount باید بیشتر از {min-amount} ریال باشد.",
            "35" => "مبلغ amount باید کمتر از {max-amount} ریال باشد.",
            "36" => "مبلغ amount بیشتر از حد مجاز است.",
            "37" => "آدرس بازگشت callback نباید خالی باشد.",
            "38" => "درخواست شما از آدرس {domain} ارسال شده است. دامنه آدرس بازگشت callback با آدرس ثبت شده در وب سرویس همخوانی ندارد.",
            "51" => "تراکنش ایجاد نشد.",
            "52" => "استعلام نتیجه ای نداشت.",
            "53" => "تایید پرداخت امکان پذیر نیست.",
            "54" => "مدت زمان تایید پرداخت سپری شده است.",
        );
        if (array_key_exists($status, $translations)) {
            throw new InvalidPaymentException($translations[$status]);
        } else {
            throw new InvalidPaymentException('خطای ناشناخته ای رخ داده است.');
        }
    }
}