<?php

namespace App\Exception;

use Exception;
use Throwable;

class LogicException extends Exception
{

    public function __construct(array $payload, ?Throwable $previous = null)
    {
        parent::__construct($payload['message'], $payload['code'], $previous);
    }
}