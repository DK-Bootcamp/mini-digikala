<?php

namespace App\DTO;

trait BaseDtoInitTrait
{
    protected function init(array $fields): void
    {
        foreach ($fields as $field => $value) {
            if (property_exists($this, $field)) {
                $this->{$field} = $value;
            }
        }
    }
}