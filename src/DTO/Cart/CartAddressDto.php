<?php

namespace App\DTO\Cart;

use App\DTO\BaseDto;
use App\DTO\BaseDtoInitTrait;
use Symfony\Component\Validator\Constraints as Assert;

class CartAddressDto extends BaseDto
{
    use BaseDtoInitTrait;

    #[Assert\NotNull]
    #[Assert\Positive]
    public readonly int $addressId;

}