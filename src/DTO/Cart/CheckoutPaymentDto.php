<?php

namespace App\DTO\Cart;

use App\DTO\BaseDto;
use App\DTO\BaseDtoInitTrait;
use Symfony\Component\Validator\Constraints as Assert;


class CheckoutPaymentDto extends BaseDto
{
    use BaseDtoInitTrait;

    #[Assert\NotBlank]
    #[Assert\NotNull]
    #[Assert\Length(max: 255)]
    public readonly string $gateway;

}