<?php

namespace App\DTO\Cart;

use App\DTO\BaseDto;
use App\DTO\BaseDtoInitTrait;
use Symfony\Component\Validator\Constraints as Assert;


class CartVoucherDto extends BaseDto
{
    use BaseDtoInitTrait;

    #[Assert\NotNull]
    #[Assert\NotBlank]
    public readonly string $code;

}