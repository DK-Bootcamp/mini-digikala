<?php

namespace App\DTO\Category;

use App\DTO\BaseDto;
use App\DTO\BaseDtoInitTrait;
use Symfony\Component\Validator\Constraints as Assert;


class CategoryFilterQueryDto extends BaseDto
{
    use BaseDtoInitTrait;

    #[Assert\NotBlank]
    public readonly int $offset;
    #[Assert\NotBlank]
    public readonly int $limit;

}
