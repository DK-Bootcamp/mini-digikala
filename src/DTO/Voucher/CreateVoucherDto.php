<?php

namespace App\DTO\Voucher;

use App\DTO\BaseDto;
use App\DTO\BaseDtoInitTrait;
use Symfony\Component\Validator\Constraints as Assert;


class CreateVoucherDto extends BaseDto
{
    use BaseDtoInitTrait;

    #[Assert\NotBlank]
    #[Assert\NotNull]
    #[Assert\Positive]
    public readonly int $amount;

    #[Assert\NotBlank]
    #[Assert\NotNull]
    #[Assert\DateTime(format: 'Y-m-d')]
    #[Assert\Expression(expression: "this.startAt >= this.getCurrentDate()")]
    public readonly string $startAt;

    #[Assert\NotBlank]
    #[Assert\NotNull]
    #[Assert\DateTime(format: 'Y-m-d')]
    #[Assert\GreaterThanOrEqual(propertyPath: 'startAt')]
    public readonly string $expireAt;

    #[Assert\Type('int')]
    #[Assert\Positive]
    #[Assert\GreaterThan(propertyPath: 'amount')]
    public ?int $minOriginalTotalPrice = null;

    #[Assert\NotBlank]
    #[Assert\NotNull]
    #[Assert\Type(type: 'array')]
    #[Assert\All([
        new Assert\Positive,
        new Assert\Type('int')
    ])]
    public readonly array $usersId;

    public function getCurrentDate(): string
    {
        return date('Y-m-d');
    }
}