<?php

namespace App\DTO\Profile;

use App\DTO\BaseDto;
use App\DTO\BaseDtoInitTrait;
use App\Enum\OrderStatus;
use Symfony\Component\Validator\Constraints as Assert;


class UserOrdersSearchQueryDto extends BaseDto
{
    use BaseDtoInitTrait;


    #[Assert\Choice(
        choices: [OrderStatus::PROCESSING, OrderStatus::SENT, OrderStatus::DELIVERED],
        message: 'Choose a valid status.'
    )]
    public ?string $status = null;
}