<?php

namespace App\DTO\Profile;

use App\DTO\BaseDto;
use App\DTO\BaseDtoInitTrait;
use App\Interfaces\QueryDtoInterface;
use Symfony\Component\Validator\Constraints as Assert;

class UserCommentsSearchDto extends BaseDto implements QueryDtoInterface
{
    use BaseDtoInitTrait;

    #[Assert\Positive(
        message: 'invalid comment id'
    )]
    public int $id;
}