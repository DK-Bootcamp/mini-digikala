<?php

namespace App\DTO\User;

use App\DTO\BaseDto;
use App\DTO\BaseDtoInitTrait;
use Symfony\Component\Validator\Constraints as Assert;

class RegisterUserDto extends BaseDto
{
    use BaseDtoInitTrait;

    #[Assert\NotNull]
    #[Assert\NotBlank]
    #[Assert\Length(
        min: 11,
        max: 11,
    )]
    public readonly string $phoneNumber;

    #[Assert\NotNull]
    #[Assert\NotBlank]
    #[Assert\Length(
        min: 4,
        minMessage: 'Your password must be at least {{ limit }} characters long',
    )]
    public readonly string $password;
}