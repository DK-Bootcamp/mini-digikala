<?php

namespace App\DTO\User;

use App\DTO\BaseDto;
use App\DTO\BaseDtoInitTrait;
use App\Interfaces\PersonalInfoDtoInterface;
use App\Validator\PhoneNumber as Assert;

class PhoneNumberDto extends BaseDto implements PersonalInfoDtoInterface
{

    use BaseDtoInitTrait;

    #[Assert\PhoneNumber]
    public string $phoneNumber;

    public function getPhoneNumber(): string
    {
        return $this->phoneNumber;
    }
}