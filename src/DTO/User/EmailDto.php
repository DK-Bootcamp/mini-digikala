<?php

namespace App\DTO\User;

use App\DTO\BaseDto;
use App\DTO\BaseDtoInitTrait;
use App\Error\UserError;
use App\Interfaces\PersonalInfoDtoInterface;
use Symfony\Component\Validator\Constraints as Assert;

class EmailDto extends BaseDto implements PersonalInfoDtoInterface
{

    use BaseDtoInitTrait;

    #[Assert\Email(
        message: UserError::NOT_CORRECT_EMAIL_FORMAT
    )]
    public string $email;

    public function getEmail(): string
    {
        return $this->email;
    }
}