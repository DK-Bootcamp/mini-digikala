<?php

namespace App\DTO\User;

use App\DTO\BaseDto;
use App\DTO\BaseDtoInitTrait;
use App\Error\UserError;
use App\Interfaces\PersonalInfoDtoInterface;
use Symfony\Component\Validator\Constraints as Assert;

class PasswordDto extends BaseDto implements PersonalInfoDtoInterface
{

    use BaseDtoInitTrait;

    #[Assert\NotNull]
    #[Assert\NotBlank]
    #[Assert\Length(
        min: 4,
        minMessage: UserError::WEAK_PASSWORD,
    )]
    public readonly string $password;
}