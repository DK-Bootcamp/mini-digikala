<?php

namespace App\DTO\User;

use App\Constants\PersonalInfoMessage;
use App\DTO\BaseDto;
use App\DTO\BaseDtoInitTrait;
use App\Interfaces\PersonalInfoDtoInterface;
use Symfony\Component\Validator\Constraints as Assert;

class IdentificationInfoDto extends BaseDto implements PersonalInfoDtoInterface
{
    use BaseDtoInitTrait;

    #[Assert\NotNull(
        message: PersonalInfoMessage::NOT_NULL_FIRST_NAME
    )]
    #[Assert\NotBlank(
        message: PersonalInfoMessage::NOT_BLANK_FIRST_NAME
    )]
    #[Assert\Length(
        min: 2,
        minMessage: PersonalInfoMessage::MIN_LENGTH_FIRST_NAME
    )]
    public string $firstName;

    #[Assert\NotNull(
        message: PersonalInfoMessage::NOT_NULL_LAST_NAME
    )]
    #[Assert\NotBlank(
        message: PersonalInfoMessage::NOT_BLANK_LAST_NAME
    )]
    #[Assert\Length(
        min: 2,
        minMessage: PersonalInfoMessage::MIN_LENGTH_LAST_NAME
    )]
    public string $lastName;

    #[Assert\NotNull(
        message: PersonalInfoMessage::NOT_NULL_NIN
    )]
    #[Assert\NotBlank(
        message: PersonalInfoMessage::NOT_BLANK_NIN
    )]
    #[Assert\Regex(
        pattern: '/^\d+/',
        message: PersonalInfoMessage::INVALID_NIN
    )]
    #[Assert\Length(
        min: 6,
        max: 16,
        minMessage: PersonalInfoMessage::INVALID_NIN,
        maxMessage: PersonalInfoMessage::INVALID_NIN,
    )]
    public string $nin;

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function  getLastName(): string
    {
        return $this->lastName;
    }

    public function getNin(): string
    {
        return $this->nin;
    }
}