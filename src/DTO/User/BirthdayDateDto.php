<?php

namespace App\DTO\User;

use App\Constants\PersonalInfoMessage;
use App\DTO\BaseDto;
use App\DTO\BaseDtoInitTrait;
use App\Interfaces\PersonalInfoDtoInterface;
use Morilog\Jalali\CalendarUtils;
use Symfony\Component\Validator\Constraints as Assert;

class BirthdayDateDto extends BaseDto implements PersonalInfoDtoInterface
{
    public const BIRTHDAY_SEPARATOR = '-';

    use BaseDtoInitTrait;

    #[Assert\NotNull(
        message: PersonalInfoMessage::NOT_NULL_BIRTHDAY_DATE
    )]
    #[Assert\NotBlank(
        message: PersonalInfoMessage::NOT_BLANK_BIRTHDAY_DATE
    )]
    #[Assert\DateTime(
        format: 'Y-m-d',
        message: PersonalInfoMessage::INVALID_BIRTHDAY_DATE
    )]
    public string $birthDayDate;

    public function getBirthDayDate(): \DateTime
    {
        return $this->toGeorgian($this->birthDayDate);
    }

    public function toGeorgian(string $date): \DateTime
    {
        $dateInfo = explode(self::BIRTHDAY_SEPARATOR,$date);
        return CalendarUtils::toGregorianDate($dateInfo[0],$dateInfo[1],$dateInfo[2]);
    }

}