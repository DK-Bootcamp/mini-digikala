<?php

namespace App\DTO\Comment;

use App\DTO\BaseDto;
use App\DTO\BaseDtoInitTrait;
use Symfony\Component\Validator\Constraints as Assert;

class CommentOnProductDto extends BaseDto
{
    use BaseDtoInitTrait;

    #[Assert\Type(
        type: 'integer',
        message: 'Your vote must be Type Of {{ type }}'
    )]
    #[Assert\Range(
        notInRangeMessage: 'Your vote must be number in range(1,5)',
        min: 1,
        max: 5,
    )]
    public int $vote;

    #[Assert\NotNull]
    #[Assert\NotBlank]
    #[Assert\length(
        min: 10,
        minMessage: 'Your descriptions must be longer'
    )]
    public string $descriptions;
}