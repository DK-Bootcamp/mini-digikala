<?php

namespace App\DTO\Product;

use App\DTO\BaseDto;
use App\DTO\BaseDtoInitTrait;
use Symfony\Component\Validator\Constraints as Assert;


class ProductUpdateStockDto extends BaseDto
{
    use BaseDtoInitTrait;

    #[Assert\NotBlank]
    #[Assert\NotNull]
    public readonly int $productId;

    #[Assert\NotBlank]
    #[Assert\NotNull]
    public readonly int $changeNumber;
}
