<?php

namespace App\DTO\Product;

use App\DTO\BaseDto;
use App\DTO\BaseDtoInitTrait;
use Symfony\Component\Validator\Constraints as Assert;

class ProductAlgoliaDto extends BaseDto
{
    use BaseDtoInitTrait;

    #[Assert\NotBlank]
    public readonly string $name;

    #[Assert\NotBlank]
    public readonly int $page;

} 