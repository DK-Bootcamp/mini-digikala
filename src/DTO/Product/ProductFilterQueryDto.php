<?php

namespace App\DTO\Product;

use App\DTO\BaseDto;
use App\DTO\BaseDtoInitTrait;
use Symfony\Component\Validator\Constraints as Assert;

class ProductFilterQueryDto extends BaseDto
{
    use BaseDtoInitTrait;

    #[Assert\NotBlank]
    public readonly int $offset;
    #[Assert\NotBlank]
    public readonly int $limit;
    public readonly array $brands;
    public readonly string $sortPrice;
    public readonly string $date;
    public readonly int $minPrice;
    public readonly int $maxPrice;

} 