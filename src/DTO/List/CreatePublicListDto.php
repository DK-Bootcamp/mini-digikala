<?php

namespace App\DTO\List;

use App\DTO\BaseDto;
use App\DTO\BaseDtoInitTrait;
use App\Error\List\PublicListError;
use Symfony\Component\Validator\Constraints as Assert;

class CreatePublicListDto extends BaseDto
{
    use BaseDtoInitTrait;

    #[Assert\NotNull(
        message: PublicListError::EMPTY_NAME_ERROR
    )]
    #[Assert\NotBlank(
        message: PublicListError::EMPTY_NAME_ERROR
    )]
    public string $name;

    public string $descriptions;
}