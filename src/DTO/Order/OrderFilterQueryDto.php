<?php

namespace App\DTO\Order;

use App\DTO\BaseDto;
use App\DTO\BaseDtoInitTrait;
use Symfony\Component\Validator\Constraints as Assert;


class OrderFilterQueryDto extends BaseDto
{
    use BaseDtoInitTrait;

    #[Assert\NotBlank]
    public readonly int $offset;
    #[Assert\NotBlank]
    public readonly int $limit;

    public readonly int $userId;
    public readonly string $orderTotalPrice;
    public readonly string $status;

}
