<?php

namespace App\DTO\Address;

use App\DTO\BaseDto;
use App\DTO\BaseDtoInitTrait;
use Symfony\Component\Validator\Constraints as Assert;


class CreateAddressDto extends BaseDto
{
    use BaseDtoInitTrait;

    #[Assert\NotBlank]
    #[Assert\NotNull]
    #[Assert\Length(max: 255)]
    public readonly string $details;

    #[Assert\NotBlank]
    #[Assert\NotNull]
    #[Assert\Length(min: 10, max: 10)]
    #[Assert\Type(type: 'numeric')]
    public readonly string $postalCode;

    #[Assert\NotBlank]
    #[Assert\NotNull]
    #[Assert\Type(type: 'numeric')]
    #[Assert\Regex('/^[-]?([1-8]?\d(\.\d+)?|90(\.0+)?)$/')]
    public readonly string $latitude;

    #[Assert\NotBlank]
    #[Assert\NotNull]
    #[Assert\Type(type: 'numeric')]
    #[Assert\Regex('/\s*[-]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)$/')]
    public readonly string $longitude;
}