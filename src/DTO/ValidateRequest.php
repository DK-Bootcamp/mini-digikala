<?php

namespace App\DTO;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ValidateRequest implements ParamConverterInterface
{
    public function __construct(private readonly ValidatorInterface $validator)
    {
    }

    public function apply(Request $request, ParamConverter $configuration): bool
    {
        $className = $configuration->getClass();
        $dtoType = $configuration->getOptions()['type'] ?? 'body';
        $validatedRequest = $dtoType === 'query'
            ? new $className($request->query->all(), $this->validator)
            : new $className($request->toArray(), $this->validator);
        $request->attributes->set($configuration->getName(), $validatedRequest);
        return true;
    }

    public function supports(ParamConverter $configuration): bool
    {
        if (null === $configuration->getClass()) {
            return false;
        }

        return is_subclass_of($configuration->getClass(), BaseDto::class);
    }
}