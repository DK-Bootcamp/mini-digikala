<?php

namespace App\DTO;

use App\Exception\LogicException;
use Exception;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class BaseDto
{
    /**
     * @throws Exception
     */
    public function __construct(array $fields, ValidatorInterface $validator)
    {
        $this->init($fields);
        $this->validate($validator);
    }

    /**
     * @throws Exception
     */
    private function validate(ValidatorInterface $validator): void
    {
        $errors = $validator->validate($this);

        if ($errors->count()) {
            $errorsString = (string)$errors;
            throw new LogicException(['message' => $errorsString, 'code' => 400]);
        }
    }

    abstract protected function init(array $fields): void;
}