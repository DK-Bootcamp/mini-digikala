<?php

namespace App\DTO\Wallet;

use App\DTO\BaseDto;
use App\DTO\BaseDtoInitTrait;
use Symfony\Component\Validator\Constraints as Assert;

class ChargePaymentDto extends BaseDto
{
    use BaseDtoInitTrait;

    #[Assert\NotBlank]
    #[Assert\NotNull]
    #[Assert\Length(max: 255)]
    public readonly string $gateway;

    #[Assert\NotBlank]
    #[Assert\NotNull]
    #[Assert\Positive]
    public readonly int $amount;

}