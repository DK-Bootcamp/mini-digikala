<?php

namespace App\Validator\PhoneNumber;

use Attribute;
use Symfony\Component\Validator\Attribute\HasNamedArguments;
use Symfony\Component\Validator\Constraint;

#[Attribute]
class PhoneNumber extends Constraint
{

    public string $message = 'فرمت شماره همراه وارد شده صحیح نمی باشد';

    #[HasNamedArguments]
    public function __construct(array $groups = null, mixed $payload = null)
    {
        parent::__construct([], $groups, $payload);
    }
}