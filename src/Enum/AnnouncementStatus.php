<?php

namespace App\Enum;

final class AnnouncementStatus
{
    public const OPEN = 'open';
    public const CLOSE = 'close';
}