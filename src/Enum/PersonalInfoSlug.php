<?php

namespace App\Enum;

enum PersonalInfoSlug: string
{
    case IDENTIFICATION_INFO = 'identification-info';

    case BIRTHDAY_DATE = 'birthday-date';

    case EMAIL = 'email';

    case PHONE_NUMBER = 'phone-number';

    case PASSWORD = 'password';

    case SLUGS = 'identification-info|birthday-date|email|phone-number|password';
}