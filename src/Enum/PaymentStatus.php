<?php

namespace App\Enum;

final class PaymentStatus
{
    public const Pending = 'pending';
    public const InProgress = 'InProgress';
    public const Finished = 'Finished';
}