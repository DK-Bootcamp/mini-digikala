<?php

namespace App\Enum;

final class WalletStatus
{
    public const ACTIVE = 'active';
    public const INACTIVE = 'inactive';
}