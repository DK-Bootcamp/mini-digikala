<?php

namespace App\Enum;

final class CartStatus
{
    public const OPEN = 'open';
    public const CLOSE = 'close';
}