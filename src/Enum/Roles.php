<?php

namespace App\Enum;

final class Roles
{
    public const USER = 'ROLE_USER';
    public const ADMIN = 'ROLE_ADMIN';
    public const SELLER = 'ROLE_SELLER';
}