<?php

namespace App\Enum;

enum ListsActiveTabSlug: string
{
    case PUBLIC = 'public';

    case FAVORITES = 'favorites';

    case ANNOUNCEMENT = 'announcements';
}