<?php

namespace App\Enum;

final class OrderStatus
{
    public const PENDING = "pending";
    public const PROCESSING = 'processing';
    public const SENT = 'sent';
    public const DELIVERED = 'delivered';
    public const CANCELED = 'canceled';
    public const FAILED = 'failed';
}