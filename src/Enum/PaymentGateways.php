<?php

namespace App\Enum;

use App\Gateways\IDPay;

class PaymentGateways
{
    public array $gateways = [
        "idPay" => IDPay::class,
    ];
}

