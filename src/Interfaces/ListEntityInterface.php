<?php

namespace App\Interfaces;

use App\Entity\Product;
use App\Entity\User;
use Doctrine\Common\Collections\Collection;

interface ListEntityInterface
{
    public function getUser(): ?User;

    public function setUser(User $user): self;

    public function getProducts(): Collection;

    public function addProduct(Product $product): self;

    public function removeProduct(Product $product): self;
}