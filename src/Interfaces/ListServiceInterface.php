<?php

namespace App\Interfaces;

use App\Entity\Product;
use App\Entity\User;

interface ListServiceInterface
{
    public function findListsBy(array $criteria, array $orderBy = [],bool $orFail = true): array;

    public function findOneListBy(array $criteria, $orFail = true): ?ListEntityInterface;

    public function addProduct(ListEntityInterface $list,Product $product): void;

    public function getList(User $user,array $criteria): ?ListEntityInterface;

    public function deleteProduct(ListEntityInterface $list, Product $product): void;
}