<?php

namespace App\Interfaces;

use Doctrine\Common\Collections\Collection;

interface ListViewInterface
{
    public function getBaseData(ListEntityInterface $list): array;

    public function viewOne(ListEntityInterface $list): array;

    public function view(array|Collection $lists): array;
}