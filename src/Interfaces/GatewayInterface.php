<?php

namespace App\Interfaces;

use App\Entity\Payment;

interface GatewayInterface
{
    public function purchase(Payment $payment);

    public function pay(Payment $payment);

    public function verify(Payment $payment);
}