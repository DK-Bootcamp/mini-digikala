<?php

namespace App\Repository;

use App\DTO\Product\ProductFilterQueryDto;
use App\DTO\Product\ProductSearchQueryDto;
use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Product>
 *
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function add(Product $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Product $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function search(ProductSearchQueryDto $query): array
    {
        return $this->createQueryBuilder('p')
            ->where('p.name LIKE :name')
            ->setParameter('name', "%$query->name%")
            ->getQuery()
            ->getResult();
    }

    public function filter(ProductFilterQueryDto $query): array
    {
        $qb = $this->createQueryBuilder('p');

        if (isset($query->brands)) {
            foreach ($query->brands as $index => $value) {
                $qb->orWhere("identity(p.category) LIKE :category$index")
                    ->setParameter("category$index", $value);
            }
        }

        if (isset($query->sortPrice) && $query->sortPrice == "asc") {
            $qb->OrderBy('p.price', 'ASC');
        } else if (isset($query->sortPrice) && $query->sortPrice == "desc") {
            $qb->OrderBy('p.price', 'DESC');
        }

        if (isset($query->date) && $query->date == "new") {
            $qb->OrderBy('p.createdAt', 'DESC');
        } else if (isset($query->date) && $query->date == "old") {
            $qb->OrderBy('p.createdAt', 'ASC');
        }

        if (isset($query->minPrice) && $query->minPrice != '') {
            $qb->andWhere("p.price >= :minPrice")
                ->setParameter("minPrice", $query->minPrice);
        }

        if (isset($query->maxPrice) && $query->maxPrice != '') {
            $qb->andWhere("p.price <= :maxPrice")
                ->setParameter("maxPrice", $query->maxPrice);
        }


        $total = count($qb->getQuery()->getResult());
        $max = $query->limit;
        $offset = $query->offset;
        $remain = $total - $offset - $max;

        $result['data'] = $qb->getQuery()->setFirstResult($offset)->setMaxResults($max)->getResult();
        $result['total'] = $total;
        $result['offset'] = $offset;
        $result['remain'] = $remain;
        // dd($result['data']);
        return $result;
    }
}
