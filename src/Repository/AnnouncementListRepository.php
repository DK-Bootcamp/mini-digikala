<?php

namespace App\Repository;

use App\Entity\AnnouncementList;
use App\Interfaces\ListRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<AnnouncementList>
 *
 * @method AnnouncementList|null find($id, $lockMode = null, $lockVersion = null)
 * @method AnnouncementList|null findOneBy(array $criteria, array $orderBy = null)
 * @method AnnouncementList[]    findAll()
 * @method AnnouncementList[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnnouncementListRepository extends ServiceEntityRepository implements ListRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AnnouncementList::class);
    }

    public function add(AnnouncementList $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(AnnouncementList $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findListsThatContainProduct(int $productId)
    {
        return $this->createQueryBuilder('q')
            ->innerjoin('q.products', 'p', 'with', 'p.id = :productId')
            ->setParameter('productId', $productId)
            ->getQuery()
            ->getResult()
        ;
    }
}
