<?php

namespace App\Repository;

use App\Entity\NotifyForBackStock;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<NotifyForBackStock>
 *
 * @method NotifyForBackStock|null find($id, $lockMode = null, $lockVersion = null)
 * @method NotifyForBackStock|null findOneBy(array $criteria, array $orderBy = null)
 * @method NotifyForBackStock[]    findAll()
 * @method NotifyForBackStock[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NotifyForBackStockRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NotifyForBackStock::class);
    }

    public function add(NotifyForBackStock $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(NotifyForBackStock $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
}
