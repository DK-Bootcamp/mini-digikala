<?php

namespace App\Repository;

use App\Entity\FavoritesList;
use App\Interfaces\ListEntityInterface;
use App\Interfaces\ListRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<FavoritesList>
 *
 * @method FavoritesList|null find($id, $lockMode = null, $lockVersion = null)
 * @method FavoritesList|null findOneBy(array $criteria, array $orderBy = null)
 * @method FavoritesList[]    findAll()
 * @method FavoritesList[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FavoritesListRepository extends ServiceEntityRepository implements ListRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FavoritesList::class);
    }

    public function add(ListEntityInterface $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(ListEntityInterface $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return FavoritesList[] Returns an array of FavoritesList objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('f')
//            ->andWhere('f.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('f.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?FavoritesList
//    {
//        return $this->createQueryBuilder('f')
//            ->andWhere('f.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
