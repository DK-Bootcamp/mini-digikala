<?php

namespace App\Repository;

use App\DTO\Order\OrderFilterQueryDto;
use App\Entity\Order;
use App\Entity\User;
use App\Enum\OrderStatus;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\Query\Parameter;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Order>
 *
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Order::class);
    }

    public function add(Order $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Order $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function filter(OrderFilterQueryDto $query): array
    {
        $qb = $this->createQueryBuilder('o');

        if (isset($query->userId)) {
            $qb->andWhere("identity(o.user) = $query->userId");
        }

        if (isset($query->orderTotalPrice) && $query->orderTotalPrice == "desc") {
            $qb->OrderBy('o.totalPrice', 'DESC');
        } else if (isset($query->orderTotalPrice) && $query->orderTotalPrice == "asc") {
            $qb->OrderBy('o.totalPrice', 'ASC');
        }

        if (isset($query->status)) {
            $qb->andWhere("o.status = $query->status");
        }


        $orders = $qb->getQuery()->setFirstResult($query->offset)->setMaxResults($query->limit)->getResult();;

        return $orders;
    }

    /**
     * @throws Exception
     */
    public function findByDate(): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = "select id, status from `order` where updated_at < DATE_SUB(now(), INTERVAL 6 HOUR) and status = 'pending'";
        $stmt = $conn->prepare($sql)->executeQuery();

        return $stmt->fetchAllKeyValue();
    }

}
