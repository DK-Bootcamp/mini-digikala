<?php

namespace App\View\Order;

use App\Entity\Order;

class OrderView
{
    public function __construct(private readonly OrderItemView $orderItemView)
    {
    }

    public function getOrderBaseData(Order $order): array
    {
        $itemsData = $this->orderItemView->getItemsBaseData($order->getOrderItems());
        return [
            'id' => $order->getId(),
            'userId' => $order->getUser()->getId(),
            'addressId' => $order->getAddress()?->getId(),
            'status' => $order->getStatus(),
            'items' => $itemsData,
            "totalPrice"=> $order->getTotalPrice(),
            "discountPrice"=>$order->getDiscountPrice(),
            "shipmentPrice"=>$order->getShipmentPrice(),
            "finalPrice"=>$order->getFinalPrice(),
            "deliveryDate"=>$order->getDeliveryDate(),
            "paymentMethod"=>$order->getPayment()->getGateway(),
            "transactionId" => $order->getTransactionId(),
            'createdAt' => $order->getCreatedAt(),
            'updatedAt' => $order->getUpdatedAt(),
        ];
    }

    public function getOrdersBaseData(array $orders): array
    {
        $data = [];
        foreach ($orders as $order) {
            $data[] = $this->getOrderBaseData($order);
        }

        return $data;

    }

    public function getFilteredData(array $orders): array
    {
        return $this->getOrdersBaseData($orders);
    }

}