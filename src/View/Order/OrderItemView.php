<?php

namespace App\View\Order;

use App\Entity\OrderItem;
use Doctrine\Common\Collections\Collection;

class OrderItemView
{
    public function getItemBaseData(OrderItem $item): array
    {
        return [
            'orderId' => $item->getInvoice()->getId(),
            'productId' => $item->getProduct()->getId(),
            'quantity' => $item->getQuantity(),
            'unitPrice' => $item->getUnitPrice(),
        ];

    }

    public function getItemsBaseData(Collection $items): array
    {
        $data = [];
        foreach ($items as $item) {
            $data[] = $this->getItemBaseData($item);
        }
        return $data;

    }

}