<?php

namespace App\View\Address;

use App\Entity\Address;

class AddressView
{
    public function getAddressBaseData(Address $address): array
    {
        return [
            'userId' => $address->getUser()->getId(),
            'id' => $address->getId(),
            'details' => $address->getDetails(),
            'postalCode' => $address->getPostalCode(),
            'latitude' => $address->getLatitude(),
            'longitude' => $address->getLongitude(),
        ];
    }

    public function getAddressesBaseData(array $addresses): array
    {
        $data = [];

        foreach ($addresses as $address) {
            $data[] = $this->getAddressBaseData($address);
        }

        return $data;
    }
}