<?php

namespace App\View\Comment;

use App\Entity\Comment;
use Doctrine\Common\Collections\Collection;

class CommentView
{

    public function viewOneComment(Comment $comment) : array
    {
        return [
            'comment_id' => $comment->getId(),
            'descriptions' => $comment->getDescriptions(),
            'vote' => $comment->getVote(),
            'user_id' => $comment->getUser()->getId(),
            'product' => $comment->getProduct()->getName(),
        ];
    }

    public function viewComments(Collection|array $comments) : array
    {
        $view = [];

        foreach($comments as $comment){
            $view[] = $this->viewOneComment($comment);
        }

        return $view;
    }
}