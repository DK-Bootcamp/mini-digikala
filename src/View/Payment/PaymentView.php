<?php

namespace App\View\Payment;

use App\Entity\Payment;

class PaymentView
{
    public function initialPaymentInfo(Payment $payment): array
    {
        return [
            'id' => $payment->getId(),
            'uuid' => $payment->getUuid(),
            'amount' => $payment->getAmount(),
            'gateway' => $payment->getGateway(),
            'transaction_id' => $payment->getTransactionId()
        ];
    }
}