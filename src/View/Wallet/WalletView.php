<?php

namespace App\View\Wallet;

use App\Entity\Wallet;

class WalletView
{

    public function getBaseData(Wallet $wallet): array
    {
        return [
            'id' => $wallet->getId(),
            'userId' => $wallet->getUser()->getId(),
            'balance' => $wallet->getBalance(),
            'status' => $wallet->getStatus(),
            'createdAt' => $wallet->getCreatedAt(),
            'updatedAt' => $wallet->getUpdatedAt()
        ];

    }

}