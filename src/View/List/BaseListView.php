<?php

namespace App\View\List;

use App\Interfaces\ListEntityInterface;
use App\Interfaces\ListViewInterface;
use App\View\Product\ProductView;
use Doctrine\Common\Collections\Collection;

class BaseListView implements ListViewInterface
{
    public function getBaseData(ListEntityInterface $list): array
    {
        return [
            'list id' => $list->getId(),
            'user id' => $list->getUser()->getId(),
            'createdAt' => $list->getCreatedAt(),
        ];
    }

    public function viewOne(ListEntityInterface $list): array
    {
        $view = $this->getBaseData($list);
        $view += ['products' => (new productView())->getProductsBaseData($list->getProducts()),];

        return $view;
    }

    public function view(array|Collection $lists): array
    {
        $View = [];

        foreach($lists as $list){
            $View[] = $this->viewOne($list);
        }

        return $View;
    }
}