<?php

namespace App\View\List;

use App\Interfaces\ListEntityInterface;

class PublicListView extends BaseListView
{
    public function viewOne(ListEntityInterface $list): array
    {
        $view['name'] = $list->getName();
        $view['descriptions'] = $list->getDescriptions() ?? 'No descriptions';
        $view += parent::viewOne($list);

        return $view;
    }
}