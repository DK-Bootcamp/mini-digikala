<?php

namespace App\View\Product;

use App\Entity\Product;
use Doctrine\Common\Collections\Collection;

class ProductView
{
    
    public function getProductBaseData(Product $product): array
    {
        return [
            'id' => $product->getId(),
            'name' => $product->getName(),
            'price' => $product->getPrice(),
            'stock' => $product->getStock(),
            'category' => $product->getCategory()->getName()
        ];
    }
    public function getProductFullData(Product $product): array
    {
        $data = $this->getProductBaseData($product);
        $data['category'] = $product->getCategory()->getName();
        return $data;
    }

    public function getProductsBaseData(Collection $products): array
    {
        $data = [];
        foreach ($products as $product) {
            $data[] = $this->getProductBaseData($product);
        }

        return $data;
    }

    public function getProductsFullData(Collection $products): array
    {
        $data = [];
        foreach ($products as $product) {
            $data[] = $this->getProductBaseData($product);
        }

        return $data;
    }

    public function getFilteredData(Collection $data): array {
        $result = [];
        foreach ($data['data'] as $product) {
            $result['products'][] = $this->getProductFullData($product);
        }

        $result['total'] = $data['total'];
        $result['offset'] = $data['offset'];
        $result['remain'] = $data['remain'];

        return $result;
    }
}