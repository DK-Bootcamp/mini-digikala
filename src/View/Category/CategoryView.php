<?php

namespace App\View\Category;

use App\Entity\Category;
use App\View\Product\ProductView;
use Doctrine\Common\Collections\Collection;

class CategoryView
{
    public function __construct(private readonly ProductView $productView) {
    }
    public function getCategoryBaseData(Category $product): array
    {
        return [
            'id' => $product->getId(),
            'name' => $product->getName(),
        ];
    }

    public function getCategoriesBaseData(Collection $categories): array
    {
        $data = [];
        foreach ($categories as $category) {
            $data[] = $this->getCategoryBaseData($category);
        }

        return $data;
    }

    public function getFilteredData(array $products): array
    {
        $data = [];
        foreach ($products as $product) {
            $data[] = (new ProductView())->getProductFullData($product);
        }

        return $data;
    }


}