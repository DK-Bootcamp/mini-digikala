<?php

namespace App\View\Profile;

use App\Entity\UserPersonalInfo;
use App\View\Address\AddressView;
use App\View\Order\OrderView;
use App\View\User\UserView;

class ProfileView
{
    public function __construct(
        private readonly OrderView   $orderView,
        private readonly AddressView $addressView,
        private readonly UserView $userView,
    )
    {
    }

    public function viewPersonalInfo(UserPersonalInfo $userPersonalInfo): array
    {
        return $this->userView->viewUserPersonalInfo($userPersonalInfo);
    }

    public function getOrdersData(array $orders): array
    {
        return $this->orderView->getOrdersBaseData($orders);
    }

    public function getAddressesData(array $addresses): array
    {
        return $this->addressView->getAddressesBaseData($addresses);

    }
}