<?php

namespace App\View\Cart;

use App\Entity\Cart;

class CartView
{
    public function __construct(private readonly CartItemView $cartItemView)
    {
    }

    public function getBaseData(Cart $cart): array
    {
        $itemsData = $this->cartItemView->getItemsBaseData($cart->getCartItems());
        $shipmentInfo = $cart->getAddress()?->getShipmentInfo();
        return [
            'id' => $cart->getId(),
            'userId' => $cart->getUser()->getId(),
            'addressId' => $cart->getAddress()?->getId(),
            'voucherCode' => $cart->getVoucherCode()?->getCode(),
            'status' => $cart->getStatus(),
            'items' => $itemsData,
            'shipmentInfo' => $shipmentInfo,
            'originalTotalPrice' => $cart->getOriginalTotalPrice(),
            'discountAmount' => $cart->getDiscountAmount(),
            'finalPrice' => $cart->getFinalPrice(),
            'createdAt' => date_format($cart->getCreatedAt(), 'Y-m-d'),
            'updatedAt' => date_format($cart->getUpdatedAt(), 'Y-m-d'),
        ];
    }
}