<?php

namespace App\View\Cart;

use App\Entity\CartItem;
use Doctrine\Common\Collections\Collection;

class CartItemView
{

    public function getItemBaseData(CartItem $item): array
    {
        return [
            'cartId' => $item->getCart()->getId(),
            'productId' => $item->getProduct()->getId(),
            'quantity' => $item->getQuantity(),
            'unitPrice' => $item->getUnitPrice(),
            'totalPrice' => $item->getTotalPrice(),
        ];

    }

    public function getItemsBaseData(Collection $items): array
    {
        $data = [];
        foreach ($items as $item) {
            $data[] = $this->getItemBaseData($item);
        }
        return $data;

    }

}