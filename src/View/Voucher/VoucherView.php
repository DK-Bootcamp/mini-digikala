<?php

namespace App\View\Voucher;

use App\Entity\Voucher;

class VoucherView
{
    public function getBaseData(Voucher $voucher): array
    {
        return [
            'id' => $voucher->getId(),
            'amount' => $voucher->getAmount(),
            'startAt' => date_format($voucher->getStartAt(), 'Y-m-d'),
            'expireAt' => date_format($voucher->getExpireAt(), 'Y-m-d'),
            'minOriginalTotalPrice' => $voucher->getMinOriginalTotalPrice(),
            'createdAt' => date_format($voucher->getCreatedAt(), 'Y-m-d'),
            'updatedAt' => date_format($voucher->getCreatedAt(), 'Y-m-d'),
        ];
    }

}