<?php

namespace App\View\User;

use App\Entity\User;
use App\Entity\UserPersonalInfo;

class UserView
{

    public function getUserData(User $user): array
    {
        return [
            'id' => $user->getId(),
            'phoneNumber' => $user->getPhoneNumber(),
            'email' => $user->getEmail(),
        ];
    }

    public function viewUserPersonalInfo(UserPersonalInfo $userPersonalInfo): array
    {
        $personalInfo = $this->getUserData($userPersonalInfo->getUser());

        $personalInfo += [
            'firstName' => $userPersonalInfo->getFirstName(),
            'lastName' => $userPersonalInfo->getLastName(),
            'nin' => $userPersonalInfo->getNin(),
            'birthDayDate' => $userPersonalInfo->getBirthDayDate(),
        ];

        return $personalInfo;
    }
}