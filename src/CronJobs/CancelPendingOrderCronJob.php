<?php

namespace App\CronJobs;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use App\Repository\OrderRepository;
use App\Service\OrderService;
use Exception;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


#[AsCommand(
    name: 'app:cancel-pending-order',
    description: 'Cancels order with pending status.',
    aliases: ['app:cancel-pending-order'],
    hidden: false
)]
class CancelPendingOrderCronJob extends Command
{
    public function __construct(
        private readonly OrderRepository $orderRepository,
        private readonly OrderService $orderService
    )
    {
        parent::__construct();
    }

    /**
     * @throws \Doctrine\DBAL\Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $orders = $this->orderRepository->findByDate();

        $output->writeln([
            "orders to be canceled are :",
        ]);
        try {
            foreach ($orders as $order => $value){
                $output->writeln(
                    "$order",
                );
                try {
                    $this->orderService->cancelPendingOrders($order);
                    $output->writeln(
                        "SUCCESS"
                    );
                }
                catch (Exception $exception){
                    $output->writeln(
                        "FAILED"
                    );
                    echo $exception;
                }
            }
            return Command::SUCCESS;
        }
        catch (Exception){
            return Command::FAILURE;
        }

    }
}