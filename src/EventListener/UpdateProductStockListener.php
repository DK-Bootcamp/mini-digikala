<?php

namespace App\EventListener;

use App\Event\UpdateProductStockEvent;
use App\Factory\ListServiceFactory;
use App\Model\Notify\NotifyUserForBackStockModel;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;

#[AsEventListener(event: UpdateProductStockEvent::class, method: 'onAppUpdateProductStockEvent')]
class UpdateProductStockListener
{

    public function __construct(
        private readonly ListServiceFactory          $listServiceFactory,
        private readonly NotifyUserForBackStockModel $notifyUserForBackStockModel,
    )
    {
    }

    public function onAppUpdateProductStockEvent(UpdateProductStockEvent $event): void
    {
        $announcementsList = $this->listServiceFactory->getService('announcements')
            ->findListsThatContainProduct($event->getProduct());

        foreach ($announcementsList as $list) {
            $this->notifyUserForBackStockModel->notify($list, $event->getProduct(), $event->getPrevStock());
        }
    }

}