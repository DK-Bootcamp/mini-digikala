<?php

namespace App\DataFixtures;

use App\Tests\Factory\CommentFactory;
use App\Tests\Factory\PublicListFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class PublicListFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        PublicListFactory::createMany(3);
    }
}
