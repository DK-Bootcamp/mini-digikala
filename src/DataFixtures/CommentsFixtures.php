<?php

namespace App\DataFixtures;

use App\Tests\Factory\CommentFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CommentsFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        CommentFactory::createOne();
    }
}
