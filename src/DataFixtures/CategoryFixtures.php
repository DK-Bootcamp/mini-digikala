<?php

namespace App\DataFixtures;

use App\Tests\Factory\CategoryFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {
        CategoryFactory::createMany(2);
    }
}