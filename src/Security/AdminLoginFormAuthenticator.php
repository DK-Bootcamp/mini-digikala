<?php

namespace App\Security;

use App\Error\LoginError;
use App\Repository\UserRepository;
use App\Trait\LoginFormAuthenticatorTrait;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Http\Authenticator\AbstractLoginFormAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\CsrfTokenBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\PasswordCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

class AdminLoginFormAuthenticator extends AbstractLoginFormAuthenticator
{
    public const DEFAULT_TARGET_PATH = 'app_admin_dashboard';

    public const LOGIN = 'app_security_admin_login';

    use TargetPathTrait;

    use LoginFormAuthenticatorTrait;

    public function __construct(
        private UserRepository $userRepository,
        private RouterInterface $router,
    )
    {
    }

    /**
     * @throws Exception
     */
    public function authenticate(Request $request): Passport
    {
        $userIdentifier = $request->request->get('userIdentifier');
        $userIdentifierProperty = $this->getUserIdentifierProperty($userIdentifier);
        if(!$user = $this->userRepository->findOneBy([$userIdentifierProperty => $userIdentifier])) {
            throw new CustomUserMessageAuthenticationException(LoginError::NOT_SIGNED_UP_YET);
        }
        $password = $request->request->get('password');

        return new Passport(
            new UserBadge($userIdentifier, function($userIdentifier) use($userIdentifierProperty){
                return $this->userRepository->findOneBy([$userIdentifierProperty => $userIdentifier]);
            }),
            new PasswordCredentials($password),
            [
                new CsrfTokenBadge(
                    'authenticate',
                    $request->request->get('_csrf_token')
                ),
            ],
        );
    }
}