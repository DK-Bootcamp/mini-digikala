<?php

namespace App\Service;

use App\Entity\Comment;
use App\Entity\User;
use App\Entity\UserPersonalInfo;
use App\Enum\Roles;
use App\Error\UserError;
use App\Exception\LogicException;
use App\Interfaces\PersonalInfoDtoInterface;
use App\Repository\UserPersonalInfoRepository;
use App\Repository\UserRepository;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use function Symfony\Component\String\u;

class UserService
{
    public function __construct(
        private readonly UserRepository              $userRepository,
        private readonly UserPersonalInfoRepository  $userPersonalInfoRepository,
        private readonly Security                    $security,
        private readonly UserPasswordHasherInterface $userPasswordHarsher,
    )
    {
    }

    public function setComment(User $user, Comment $comment, $flush = false) : User
    {
        $user = $user->addComment($comment);
        $this->userRepository->add($user);

        if($flush) {
            $this->userRepository->add($user, true);
        }

        return $user;
    }

    public function getUser(): ?UserInterface
    {
        return $this->security->getUser();
    }

    public function register(FormInterface $form, User $user, string $role = Roles::USER): User
    {
        $user->setPassword(
            $this->userPasswordHarsher->hashPassword(
                $user,
                $form->get('plainPassword')->getData()
            )
        )->setRoles([$role]);

        $this->userRepository->add($user,true);

        return $user;
    }

    public function findUserById(int $useId): ?User
    {
        return $this->userRepository->find($useId);
    }

    /**
     * @throws LogicException
     */
    public function findUserBy(array $criteria, $orFAil = true): ?User
    {
        $user = $this->userRepository->findOneBy($criteria);

        if(!$user && $orFAil){
            throw new LogicException(UserError::USER_NOT_FOUND);
        }

        return $user;
    }

    public function resetPassword(User $user, string $hashedPassword): User
    {
        $user->setPassword($hashedPassword);

        $this->userRepository->add($user, true);
        return $user;
    }

    public function getUserPersonalInfo(User $user): UserPersonalInfo
    {
        $personalInfo = $this->userPersonalInfoRepository->findOneBy(['user' => $user]);

        if(!$personalInfo){
            $personalInfo = new UserPersonalInfo();
            $personalInfo->setUser($user);
            $this->userPersonalInfoRepository->add($personalInfo,true);
        }

        return $personalInfo;
    }

    public function editUserPersonalInfo(User $user, PersonalInfoDtoInterface $personalInfoDto): UserPersonalInfo
    {
        $personalInfo = $this->getUserPersonalInfo($user);

        foreach($personalInfoDto as $property => $value){
            $propertyCamelCase = u($property)->camel()->title()->toString();
            $setter = 'set' . $propertyCamelCase;
            $dtoGetter = 'get' . $propertyCamelCase;
            $personalInfo->{$setter}($personalInfoDto->{$dtoGetter}());
        }
        $this->userPersonalInfoRepository->add($personalInfo,true);

        return $personalInfo;
    }

    /**
     * @throws LogicException
     */
    function findOne(int $id, bool $orFail = true): ?User
    {
        $user = $this->userRepository->find($id);
        if ($orFail && !$user) {
            throw new LogicException(UserError::USER_NOT_FOUND);
        }
        return $user;
    }
}