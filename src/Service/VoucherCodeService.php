<?php

namespace App\Service;

use App\Entity\User;
use App\Entity\Voucher;
use App\Entity\VoucherCode;
use App\Error\VoucherCodeError;
use App\Exception\LogicException;
use App\Repository\VoucherCodeRepository;
use App\Utils\StringUtil;

class VoucherCodeService
{
    public function __construct(private readonly VoucherCodeRepository $voucherCodeRepository)
    {
    }

    public function create(Voucher $voucher, User $user): VoucherCode
    {
        $voucherCode = new VoucherCode();
        $voucherCode->setCode(StringUtil::random_strings());
        $voucherCode->setUser($user);
        $voucherCode->setVoucher($voucher);
        $this->voucherCodeRepository->save($voucherCode, true);
        return $voucherCode;
    }


    /**
     * @throws LogicException
     */
    public function findOne(int $id, bool $orFail = true): ?VoucherCode
    {
        $voucherCode = $this->voucherCodeRepository->find($id);
        if ($orFail && !$voucherCode) {
            throw new LogicException(VoucherCodeError::VOUCHER_CODE_NOT_FOUND);
        }
        return $voucherCode;
    }

    /**
     * @throws LogicException
     */
    public function findOneBy(array $criteria, bool $orFail = true): ?VoucherCode
    {
        $voucherCode = $this->voucherCodeRepository->findOneBy($criteria);
        if ($orFail && !$voucherCode) {
            throw new LogicException(VoucherCodeError::VOUCHER_CODE_NOT_FOUND);
        }
        return $voucherCode;
    }

}