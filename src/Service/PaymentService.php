<?php

namespace App\Service;

use App\DTO\Cart\CheckoutPaymentDto;
use App\DTO\Wallet\ChargePaymentDto;
use App\Entity\Order;
use App\Entity\Payment;
use App\Entity\User;
use App\Enum\OrderStatus;
use App\Enum\PaymentGateways;
use App\Enum\PaymentStatus;
use App\Error\PaymentError;
use App\Exception\LogicException;
use App\Repository\PaymentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Request;

class PaymentService
{
    public function __construct(
        private readonly PaymentRepository      $paymentRepository,
        private readonly CartService            $cartService,
        private readonly OrderService           $orderService,
        private readonly EntityManagerInterface $entityManager,
        private readonly PaymentGateways        $paymentGateways,
    )
    {
    }

    public function uuid(): string
    {
        return Uuid::uuid4()->toString();
    }

    /**
     * @throws LogicException
     */
    public function findOneBy(array $criteria, bool $orFail = true): ?Payment
    {
        $payment = $this->paymentRepository->findOneBy($criteria);
        if ($orFail && !$payment) {
            throw new LogicException(PaymentError::PAYMENT_NOT_FOUND);
        }
        return $payment;
    }

    public function requestGateway(string $gateway, string $callbackUrl)
    {
        return new $this->paymentGateways->gateways[$gateway]($this, $callbackUrl);
    }

    public function createChargePayment(User $user, ChargePaymentDto $data): Payment
    {
        $payment = new Payment();
        $payment->setUser($user);
        $payment->setStatus(PaymentStatus::Pending);
        $payment->setUuid($this->uuid());
        $payment->setAmount($data->amount);
        $payment->setGateway($data->gateway);
        $this->paymentRepository->add($payment, true);
        return $payment;
    }

    /**
     * @throws LogicException
     */
    public function createCheckoutPayment(User $user, CheckoutPaymentDto $data): Payment
    {
        $order = $this->orderService->findOneBy(["user" => $user, "status" => OrderStatus::PENDING], false);
        if ($order) {
            $payment = $order->getPayment();
        } else {
            $payment = new Payment();
            $payment->setUser($user);
            $payment->setStatus(PaymentStatus::Pending);
            $payment->setUuid($this->uuid());
            $payment->setAmount($this->cartService->getTotalAmount($user));
        }
        $payment->setGateway($data->gateway);
        $this->paymentRepository->add($payment, true);
        return $payment;
    }

    public function getPayment(string $transactionId): Payment
    {
        return $this->paymentRepository->findOneBy(["transactionId" => $transactionId]);
    }

    public function update(Payment $payment): Payment
    {
        $this->paymentRepository->add($payment, true);
        return $payment;
    }

    /**
     * @throws Exception
     */
    public function checkoutPaymentRequest(User $user, CheckoutPaymentDto $data): string
    {
        try {
            $this->entityManager->beginTransaction();
            $payment = $this->createCheckoutPayment($user, $data);
            $gateway = $this->requestGateway($data->gateway, $_ENV['CART_CHECKOUT_CALLBACK_URL']);
            $gateway->purchase($payment);
            $apiUrl = $gateway->pay($payment);
            $this->orderService->create($user, $payment);
            $this->entityManager->commit();
            $this->entityManager->flush();
            return $apiUrl;
        } catch (Exception $exception) {
            $this->entityManager->rollback();
            throw $exception;
        }
    }

    /**
     * @throws LogicException
     */
    public function checkoutPaymentWallet(User $user, CheckoutPaymentDto $data): Order
    {
        try {
            $this->entityManager->beginTransaction();
            $payment = $this->createCheckoutPayment($user, $data);
            $wallet = $user->getWallet();
            $wallet->checkBalance($payment->getAmount());
            $order = $this->orderService->create($user, $payment);
            $wallet->updateBalance(-(int)$order->getFinalPrice());
            $payment->setStatus(PaymentStatus::Finished);
            $order = $this->orderService->finalize($payment);
            $this->entityManager->commit();
            $this->entityManager->flush();
            return $order;
        } catch (Exception $e) {
            $this->entityManager->rollback();
            throw $e;
        }
    }

    /**
     * @throws LogicException
     */
    public function checkout(Request $request): Order
    {
        try {
            $this->entityManager->beginTransaction();
            $payment = $this->getPayment($request->request->get("id"));
            if ($payment->getStatus() != PaymentStatus::InProgress) {
                throw new LogicException(PaymentError::INVALID_INFO);
            }
            $gateway = $this->requestGateway($payment->getGateway(), $_ENV['CART_CHECKOUT_CALLBACK_URL']);
            $gateway->verify($payment);
            $this->entityManager->commit();
            $this->entityManager->flush();
            return $this->orderService->finalize($payment);
        } catch (Exception $exception) {
            $this->entityManager->rollback();
            throw $exception;
        }
    }
}
