<?php

namespace App\Service;

use App\DTO\Comment\CommentOnProductDto;
use App\Entity\Comment;
use App\Entity\Product;
use App\Entity\User;
use App\Error\CommentError;
use App\Exception\LogicException;
use App\Repository\CommentRepository;

class CommentService
{

    public function __construct(
        private readonly CommentRepository $commentRepository,
    )
    {
    }

    /**
     * @throws LogicException
     */
    public function findUserCommentOnProduct(User $user, Product $product, bool $orFail = true) : ?Comment
    {
        $comment = $this->commentRepository->findOneBy([
            'user' => $user,
            'product' => $product,
        ]);

        if(!$comment && $orFail){
            throw new LogicException(CommentError::NOT_COMMENT_ON_PRODUCT_YET);
        }

        return $comment;
    }

    public function create(User $user,Product $product,CommentOnProductDto $commentOnProductDto) : Comment
    {
        $comment = new Comment();
        $comment->setVote($commentOnProductDto->vote);
        $comment->setDescriptions($commentOnProductDto->descriptions);
        $comment->setUser($user);
        $comment->setProduct($product);

        return $comment;
    }

    public function save(Comment $comment): void
    {
        $this->commentRepository->add($comment,true);
    }

    public function change(Comment $comment, CommentOnProductDto $commentOnProductDto) : Comment
    {
        $comment->setDescriptions($commentOnProductDto->descriptions);
        $comment->setVote($commentOnProductDto->vote);

        return $comment;
    }

    /**
     * @throws LogicException
     */
    public function hasUserCommentedOnProductYet(User $user, Product $product) : bool
    {
        if($this->findUserCommentOnProduct($user,$product,false)){
            return true;
        }
        return false;
    }

    public function findComment($criteria): array
    {
        return $this->commentRepository->findBy($criteria);
    }

    public function getAllCommentsOfProduct(Product $product) : array
    {
        return $this->commentRepository->findBy(
            [
                'product' => $product,
            ],
            [
                'createdAt' => 'DESC',
            ],
        );
    }

    /**
     * @throws LogicException
     */
    public function findOne(array $criteria, $orFail = true): ?Comment
    {
        $comment = $this->commentRepository->findOneBy($criteria);

        if(!$comment && $orFail){
            throw new LogicException(CommentError::NOT_COMMENT_FIND);
        }

        return $comment;
    }

    public function delete(Comment $comment): void
    {
        $this->commentRepository->remove($comment,true);
    }
}
