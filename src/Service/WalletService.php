<?php

namespace App\Service;

use App\DTO\Wallet\ChargePaymentDto;
use App\Entity\User;
use App\Entity\Wallet;
use App\Enum\PaymentStatus;
use App\Enum\WalletStatus;
use App\Error\PaymentError;
use App\Error\WalletError;
use App\Exception\LogicException;
use App\Repository\WalletRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpFoundation\Request;

class WalletService
{

    public function __construct(
        private readonly WalletRepository       $walletRepository,
        private readonly PaymentService         $paymentService,
        private readonly EntityManagerInterface $entityManager,
    )
    {
    }

    public function create(User $user): Wallet
    {
        $wallet = $user->getWallet();
        if (!$wallet){
            $wallet = new Wallet();
            $wallet->setUser($user);
            $wallet->setStatus(WalletStatus::ACTIVE);
            $this->walletRepository->save($wallet, true);
        }
        return $wallet;
    }

    /**
     * @throws LogicException
     */
    public function findOne(int $id, bool $orFail = true): ?Wallet
    {
        $wallet = $this->walletRepository->find($id);
        if ($orFail && !$wallet) {
            throw new LogicException(WalletError::WALLET_NOT_FOUND);
        }
        return $wallet;
    }

    /**
     * @throws LogicException
     */
    public function findOneBy(array $criteria, bool $orFail = true): ?Wallet
    {
        $wallet = $this->walletRepository->findOneBy($criteria);
        if ($orFail && !$wallet) {
            throw new LogicException(WalletError::WALLET_NOT_FOUND);
        }
        return $wallet;
    }

    /**
     * @throws LogicException
     */
    public function activate(User $user): ?Wallet
    {
        $wallet = $this->findOneBy(['user' => $user]);
        if ($wallet->getStatus() === WalletStatus::INACTIVE) {
            $wallet->setStatus(WalletStatus::ACTIVE);
            $this->walletRepository->save($wallet, true);
        }

        return $wallet;
    }

    /**
     * @throws LogicException
     */
    public function inactivate(User $user): ?Wallet
    {
        $wallet = $this->findOneBy(['user' => $user]);
        if ($wallet->getStatus() === WalletStatus::ACTIVE) {
            $wallet->setStatus(WalletStatus::INACTIVE);
            $this->walletRepository->save($wallet, true);
        }

        return $wallet;
    }

    /**
     * @throws LogicException
     * @throws Exception
     */
    public function requestPayment(User $user, ChargePaymentDto $data)
    {
        $wallet = $this->findOneBy([
            'user' => $user,
        ]);
        if ($wallet->getStatus() === WalletStatus::INACTIVE) {
            throw new LogicException(WalletError::WALLET_IS_NOT_ACTIVE);
        }

        try {
            $this->entityManager->beginTransaction();
            $payment = $this->paymentService->createChargePayment($user, $data);
            $gateway = $this->paymentService->requestGateway($data->gateway, $_ENV['WALLET_CHARGE_CALLBACK_URL']);
            $gateway->purchase($payment);
            $apiUrl = $gateway->pay($payment);
            $this->entityManager->commit();
            $this->entityManager->flush();
            return $apiUrl;
        } catch (Exception $exception) {
            $this->entityManager->rollback();
            throw $exception;
        }
    }

    /**
     * @throws LogicException
     */
    public function charge(Request $request): ?Wallet
    {
        try {
            $this->entityManager->beginTransaction();
            $payment = $this->paymentService->getPayment($request->request->get("id"));
            if ($payment->getStatus() != PaymentStatus::InProgress) {
                throw new LogicException(PaymentError::INVALID_INFO);
            }
            $gateway = $this->paymentService->requestGateway($payment->getGateway(), $_ENV['WALLET_CHARGE_CALLBACK_URL']);
            $gateway->verify($payment);
            $wallet = $this->findOneBy([
                'user' => $payment->getUser(),
                'status' => WalletStatus::ACTIVE
            ]);
            $this->updateBalance($wallet, $payment->getAmount());
            $this->entityManager->commit();
            $this->entityManager->flush();
            return $wallet;
        } catch (Exception $exception) {
            $this->entityManager->rollback();
            throw $exception;
        }
    }

    public function updateBalance(Wallet $wallet, int $amount): Wallet
    {
        $wallet->setBalance($wallet->getBalance() + $amount);
        $this->walletRepository->save($wallet, true);
        return $wallet;
    }

}