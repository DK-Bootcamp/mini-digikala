<?php

namespace App\Service\List;

use App\Entity\FavoritesList;
use App\Entity\User;
use App\Exception\LogicException;
use App\Interfaces\ListEntityInterface;

class FavoriteListService extends BaseListService
{

    public function getList(User $user,array $criteria): ListEntityInterface
    {
        try{
            $list = parent::getList($user,$criteria);
        }catch (LogicException){
            $list = new FavoritesList();
            $list->setUser($user);
            $this->repository->add($list,true);
        }

        return $list;
    }
}