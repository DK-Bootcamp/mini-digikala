<?php

namespace App\Service\List;

use App\Entity\Product;
use App\Entity\User;
use App\Error\List\BaseListError;
use App\Exception\LogicException;
use App\Interfaces\ListEntityInterface;
use App\Interfaces\ListRepositoryInterface;
use App\Interfaces\ListServiceInterface;

class BaseListService implements ListServiceInterface
{

    public function __construct(
        protected ListRepositoryInterface $repository,
    )
    {
    }

    /**
     * @throws LogicException
     */
    public function findListsBy(array $criteria, array $orderBy = [], bool $orFail = true): array
    {
        $lists = $this->repository->findBy($criteria,$orderBy);

        if($lists === [] && $orFail){
            throw new LogicException(BaseListError::LIST_NOT_FOUND);
        }

        return $lists;
    }

    /**
     * @throws LogicException
     */
    public function findOneListBY(array $criteria, $orFail = true): ?ListEntityInterface
    {
        $list  =$this->repository->findOneBy($criteria);

        if(!$list && $orFail){
            throw new LogicException(BaseListError::LIST_NOT_FOUND);
        }

        return $list;
    }

    public function addProduct(ListEntityInterface $list, Product $product): void
    {
        $list->addProduct($product);
        $this->repository->add($list,true);
    }

    public function isListHaveProduct(ListEntityInterface $list, Product $product): bool
    {
        foreach($list->getProducts() as $listProduct){
            if($product->getId() == $listProduct->getId()){
                return true;
            }
        }
        return false;
    }

    /**
     * @throws LogicException
     */
    public function deleteProduct(ListEntityInterface $list, Product $product): void
    {
        if(!$this->isListHaveProduct($list,$product)){
            throw new LogicException(BaseListError::PRODUCT_DOESNT_EXIST_IN_LIST);
        }
        $list->removeProduct($product);
        $this->repository->add($list,true);
    }

    /**
     * @throws LogicException
     */
    public function getList(User $user, array $criteria): ?ListEntityInterface
    {
        $criteria += ['user' => $user];
        return $this->findOneListBY($criteria);
    }
}