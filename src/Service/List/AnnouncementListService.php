<?php

namespace App\Service\List;

use App\Entity\AnnouncementList;
use App\Entity\Product;
use App\Entity\User;
use App\Exception\LogicException;
use App\Interfaces\ListEntityInterface;

class AnnouncementListService extends BaseListService
{
    public function getList(User $user,array $criteria): ListEntityInterface
    {
        try{
            $list = parent::getList($user,$criteria);
        }catch (LogicException){
            $list = new AnnouncementList();
            $list->setUser($user);
            $this->repository->add($list,true);
        }

        return $list;
    }

    public function findListsThatContainProduct(Product $product)
    {
        return $this->repository->findListsThatContainProduct($product->getId());
    }

}