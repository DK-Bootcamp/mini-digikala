<?php

namespace App\Service\List;

use App\DTO\List\CreatePublicListDto;
use App\Entity\PublicList;
use App\Entity\User;
use App\Error\List\BaseListError;
use App\Exception\LogicException;
use App\Interfaces\ListEntityInterface;

class PublicListService extends BaseListService
{

    /**
     * @throws LogicException
     */
    public function create(User $user, CreatePublicListDto $createPublicListDto): PublicList
    {
        if($this->isExistListWithSameName($user, $createPublicListDto->name)){
            throw new LogicException(BaseListError::LIST_ALREADY_EXIST);
        }
        $list = new PublicList();
        $list->setName($createPublicListDto->name);
        $list->setDescriptions($createPublicListDto->descriptions);
        $list->setUser($user);
        $this->repository->add($list,true);

        return $list;
    }

    /**
     * @throws LogicException
     */
    public function isExistListWithSameName(User $user, string $name): bool
    {
        return (bool)$this->findOneListBY([
            'user' => $user,
            'name' => $name,
        ],false);
    }

    public function delete(ListEntityInterface $list)
    {
        foreach($list->getProducts() as $product){
            $list->removeProduct($product);
            $this->repository->add($list,true);
        }

        $this->repository->remove($list,true);
    }
}