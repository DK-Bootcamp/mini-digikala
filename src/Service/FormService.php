<?php

namespace App\Service;

use App\Entity\User;
use App\Form\UserRegistrationFormType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormInterface;

class FormService
{

    public function __construct(
        private readonly FormFactoryInterface $formFactory,
    )
    {
    }

    public function createRegistrationUserForm(Request $request,User $user): FormInterface
    {
        $form = $this->formFactory->create(UserRegistrationFormType::class, $user);
        $form->handleRequest($request);

        return $form;
    }
}