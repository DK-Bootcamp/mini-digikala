<?php

namespace App\Service;

use Psr\Cache\InvalidArgumentException;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

class CacheService
{
    public function __construct(private readonly CacheInterface $cache)
    {
    }

    /**
     * @throws InvalidArgumentException
     */
    public function get(string $key, callable $callable , ?int $exp = null) {
        return $this->cache->get($key, function (ItemInterface $item) use ($callable,$exp) {
            if (!is_null($exp)){
                $item->expiresAfter($exp);
            }

            return call_user_func($callable);

        });
    }

    /**
     * @throws InvalidArgumentException
     */
    public function delete(string $key): bool
    {
        return $this->cache->delete($key);
    }

    // this part is not necessary

    /**
     * @throws InvalidArgumentException
     */
    public function set(string $key, callable $callable, ?int $exp = null) {
        $this->delete($key);
        return $this->get($key, $callable, $exp);
    }

}