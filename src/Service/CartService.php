<?php

namespace App\Service;

use App\DTO\Cart\CartAddressDto;
use App\DTO\Cart\CartItemDto;
use App\DTO\Cart\CartVoucherDto;
use App\Entity\Cart;
use App\Entity\User;
use App\Enum\CartStatus;
use App\Error\CartError;
use App\Error\PaymentError;
use App\Error\ProductError;
use App\Error\VoucherError;
use App\Exception\LogicException;
use App\Repository\CartRepository;

class CartService
{

    public function __construct(
        private readonly CartRepository     $cartRepository,
        private readonly CartItemService    $cartItemService,
        private readonly ProductService     $productService,
        private readonly AddressService     $addressService,
        private readonly VoucherCodeService $voucherCodeService,
    )
    {
    }

    public function create(User $user): Cart
    {
        $cart = new cart();
        $cart->setUser($user);
        $cart->setStatus(CartStatus::OPEN);
        $this->cartRepository->save($cart, true);
        return $cart;
    }


    /**
     * @throws LogicException
     */
    public function findOne(int $id, bool $orFail = true): ?Cart
    {
        $cart = $this->cartRepository->find($id);
        if ($orFail && !$cart) {
            throw new LogicException(CartError::CART_NOT_FOUND);
        }
        return $cart;
    }

    /**
     * @throws LogicException
     */
    public function findOneBy(array $criteria, bool $orFail = true): ?Cart
    {
        $cart = $this->cartRepository->findOneBy($criteria);
        if ($orFail && !$cart) {
            throw new LogicException(CartError::CART_NOT_FOUND);
        }
        return $cart;
    }

    /**
     * @throws LogicException
     */
    public function addItem(User $user, CartItemDto $data): ?Cart
    {
        $cart = $this->findOneBy([
            'user' => $user,
            'status' => CartStatus::OPEN
        ], false) ?? $this->create($user);
        $product = $this->productService->findOne($data->productId);

        if (!$product->getStock()) {
            throw new LogicException(ProductError::PRODUCT_NOT_AVAILABLE);
        }

        $item = $this->cartItemService->findOneBy(
            [
                'cart' => $cart,
                'product' => $product,
            ],
            false
        );

        if ($item) {
            $this->cartItemService->updateQuantity($item, 1);
        } else {
            $createdItem = $this->cartItemService->create($cart, $product, $product->getPrice());
            $cart->addCartItem($createdItem);
            $this->cartRepository->save($cart);
        }
        return $cart;
    }

    /**
     * @throws LogicException
     */
    public function removeItem(User $user, CartItemDto $data): ?Cart
    {
        $cart = $this->findOneBy([
            'user' => $user,
            'status' => CartStatus::OPEN
        ]);
        $product = $this->productService->findOne($data->productId);

        $item = $this->cartItemService->findOneBy([
            'cart' => $cart,
            'product' => $product,
        ]);

        if ($item->getQuantity() === 1) {
            $this->cartItemService->remove($item);
            $cart->removeCartItem($item);
        } else {
            $this->cartItemService->updateQuantity($item, -1);
        }

        return $cart;
    }

    /**
     * @throws LogicException
     */
    public function setAddress(User $user, CartAddressDto $data): ?Cart
    {
        $address = $this->addressService->findOneBy([
            'user' => $user,
            'id' => $data->addressId,
        ]);

        $cart = $this->findOneBy([
            'user' => $user,
            'status' => CartStatus::OPEN
        ]);
        $cart->setAddress($address);
        $this->cartRepository->save($cart, true);

        return $cart;
    }

    public function closeCart(Cart $cart): Cart
    {
        $cart->setStatus(CartStatus::CLOSE);
        $this->cartRepository->save($cart, true);
        return $cart;
    }

    /**
     * @throws LogicException
     */
    public function addVoucher(User $user, CartVoucherDto $data): ?Cart
    {
        $voucherCode = $this->voucherCodeService->findOneBy([
            'user' => $user,
            'code' => $data->code
        ]);
        $voucher = $voucherCode->getVoucher();
        $now = date('Y-m-d');
        if (
            $now < date_format($voucher->getStartAt(), 'Y-m-d')
            || $now > date_format($voucher->getExpireAt(), 'Y-m-d')
        ) {
            throw new LogicException(VoucherError::INVALID_VOUCHER);
        }

        $cart = $this->findOneBy([
            'user' => $user,
            'status' => CartStatus::OPEN
        ]);
        if (
            (
                $voucher->getMinOriginalTotalPrice()
                && $cart->getOriginalTotalPrice() < $voucher->getMinOriginalTotalPrice()
            )
            || $voucher->getAmount() > $cart->getOriginalTotalPrice()
        ) {
            throw new LogicException(VoucherError::VOUCHER_IS_NOT_APPLICABLE);

        }

        $cart->setVoucherCode($voucherCode);
        $this->cartRepository->save($cart, true);

        return $cart;
    }

    /**
     * @throws LogicException
     */
    public function removeVoucher(User $user): ?Cart
    {
        $cart = $this->findOneBy([
            'user' => $user,
            'status' => CartStatus::OPEN
        ]);

        if ($cart->getVoucherCode()) {
            $cart->setVoucherCode(null);
            $this->cartRepository->save($cart, true);
        }

        return $cart;
    }

    /**
     * @throws LogicException
     */
    public function isCartPayable(User $user): array
    {
        $cart = $this->findOneBy([
            'user' => $user,
            'status' => CartStatus::OPEN
        ]);
        $isPayable = true;
        foreach ($cart->getCartItems() as $cartItem) {
            if ($cartItem->getProduct()->getStock() < $cartItem->getQuantity()) {
                $isPayable = false;
            }
        }

        return array($isPayable, $cart);
    }

    /**
     * @throws LogicException
     */
    public function getTotalAmount(User $user): string
    {
        list($isPayable, $cart) = $this->isCartPayable($user);
        if (!$isPayable) {
            throw new LogicException(PaymentError::CART_NOT_PAYABLE);
        }
        if (!$cart->getFinalPrice()) {
            throw new LogicException(PaymentError::EMPTY_CART);
        }
        return strval($cart->getFinalPrice());
    }
}