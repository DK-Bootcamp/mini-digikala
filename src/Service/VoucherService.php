<?php

namespace App\Service;

use App\DTO\Voucher\CreateVoucherDto;
use App\Entity\User;
use App\Entity\Voucher;
use App\Error\VoucherError;
use App\Exception\LogicException;
use App\Repository\VoucherRepository;
use Exception;

class VoucherService
{
    public function __construct(
        private readonly VoucherRepository  $voucherRepository,
        private readonly VoucherCodeService $voucherCodeService,
        private readonly UserService        $userService,
        private readonly MailService        $mailService
    )
    {
    }

    /**
     * @throws LogicException
     */
    public function findOne(int $id, bool $orFail = true): ?Voucher
    {
        $voucher = $this->voucherRepository->find($id);
        if ($orFail && !$voucher) {
            throw new LogicException(VoucherError::VOUCHER_NOT_FOUND);
        }
        return $voucher;
    }

    /**
     * @throws LogicException
     */
    public function findOneBy(array $criteria, bool $orFail = true): ?Voucher
    {
        $voucher = $this->voucherRepository->findOneBy($criteria);
        if ($orFail && !$voucher) {
            throw new LogicException(VoucherError::VOUCHER_NOT_FOUND);
        }
        return $voucher;
    }

    public function create(CreateVoucherDto $data): Voucher
    {
        $voucher = new Voucher();
        $voucher->setAmount($data->amount);
        $voucher->setStartAt(date_create_immutable_from_format('Y-m-d', $data->startAt));
        $voucher->setExpireAt(date_create_immutable_from_format('Y-m-d', $data->expireAt));
        $voucher->setMinOriginalTotalPrice($data->minOriginalTotalPrice);
        $this->voucherRepository->save($voucher, true);
        $this->createVoucherCodes($voucher, $data->usersId);
        return $voucher;
    }

    public function createVoucherCodes(Voucher $voucher, array $usersId): void
    {
        $voucherExpireAt = jdate(date_format($voucher->getExpireAt(), 'Y-m-d'))->format('%d %B');
        $voucherAmount = $voucher->getAmount();
        $voucherMinOriginalTotalPrice = $voucher->getMinOriginalTotalPrice();

        foreach ($usersId as $userId) {
            try {
                $user = $this->userService->findOne($userId);
                $voucherCode = $this->voucherCodeService->create($voucher, $user);
                $this->notifyVoucherCode($user, $voucherAmount, $voucherCode->getCode(), $voucherExpireAt, $voucherMinOriginalTotalPrice);
            } catch (Exception $e) {
                echo 'Message: ' . $e->getMessage();
            }

        }
    }

    /**
     * @throws LogicException
     */
    public function notifyVoucherCode(User $user, int $voucherAmount, string $voucherCode, string $expireAt, ?int $minOriginalTotalPrice): void
    {
        $this->mailService->sendEmail([
            'to' => $user->getEmail(),
            'subject' => 'کد تخفیف مینی دیجیکالا',
            'text' => $this->mailService->generateVoucherCodeEmailText(
                $voucherAmount,
                $voucherCode,
                $expireAt,
                $minOriginalTotalPrice
            ),
        ]);

    }
}