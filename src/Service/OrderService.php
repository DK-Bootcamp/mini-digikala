<?php

namespace App\Service;

use App\DTO\Order\OrderFilterQueryDto;
use App\Entity\Order;
use App\Entity\Payment;
use App\Entity\User;
use App\Enum\CartStatus;
use App\Enum\OrderStatus;
use App\Error\OrderError;
use App\Error\PaymentError;
use App\Exception\LogicException;
use App\Repository\OrderRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

class OrderService
{
    public function __construct(
        private readonly OrderRepository        $orderRepository,
        private readonly CartService            $cartService,
        private readonly OrderItemService       $orderItemService,
        private readonly ProductService         $productService,
        private readonly EntityManagerInterface $entityManager
    )
    {
    }

    public function findBy(array $criteria, array $orderBy = null): array
    {
        return $this->orderRepository->findBy($criteria, $orderBy);
    }

    /**
     * @throws LogicException
     */
    public function findOneBy(array $criteria, bool $failIfNotFound = true): ?Order
    {
        $order = $this->orderRepository->findOneBy($criteria);

        if (!$order && $failIfNotFound) {
            throw new LogicException(OrderError::ORDER_NOT_FOUND);
        }

        return $order;
    }

    /**
     * @throws LogicException
     */
    public function findOne(int $id, bool $orFail = true): ?Order
    {
        $order = $this->orderRepository->find($id);
        if ($orFail && !$order) {
            throw new LogicException(OrderError::ORDER_NOT_FOUND);
        }
        return $order;
    }

    /**
     * @throws LogicException
     */
    public function create(User $user, Payment $payment): Order
    {
        $order = $this->findOneBy(["payment" => $payment, "status" => OrderStatus::PENDING], false);
        if ($payment->getUser() !== $user) {
            throw new LogicException(OrderError::ORDER_NOT_FOUND);
        }
        if (!$order) {
            list($isPayable, $cart) = $this->cartService->isCartPayable($payment->getUser());
            if ($isPayable) {
                $order = new Order();

                $order->setUser($user);
                $order->setPayment($payment);
                $order->setAddress($cart->getAddress());
                $order->setStatus(OrderStatus::PENDING);
                $order->setTotalPrice($cart->getOriginalTotalPrice());
                $order->setDiscountPrice($cart->getDiscountAmount());
                $order->setShipmentPrice($cart->getShipmentCost());
                $order->setFinalPrice($cart->getFinalPrice());
                $order->setDeliveryDate(($cart->getAddress()->estimateShipmentDate()));
                $order->setPaymentMethod($payment->getGateway());
                $order->setTransactionId($payment->getUuid());
                $this->orderRepository->add($order, flush: true);
                $this->orderItemService->create($order, $cart);
                $this->orderRepository->add($order);
            } else {
                throw new LogicException(PaymentError::CART_NOT_PAYABLE);
            }
        }
        return $order;
    }

    /**
     * @throws LogicException
     */
    public function finalize(Payment $payment): Order
    {
        $order = $this->findOneBy(["status" => OrderStatus::PENDING, "payment" => $payment]);
        $cart = $this->cartService->findOneBy([
            'user' => $payment->getUser(),
            'status' => CartStatus::OPEN
        ]);
        $this->cartService->closeCart($cart);
        $order->setStatus(OrderStatus::PROCESSING);
        $this->orderRepository->add($order, true);
        return $order;
    }

    /**
     * @throws LogicException
     */
    public function cancelOrder(User $user, int $orderID): Order
    {
        try {
            $this->entityManager->beginTransaction();
            $order = $this->findOneBy(["user" => $user, "id" => $orderID]);
            if ($order->getStatus() == OrderStatus::SENT){
                throw new LogicException(OrderError::ORDER_IS_SENT);
            }
            $this->revertOrderItems($order->getOrderItems());
            $wallet = $user->getWallet();
            $wallet->updateBalance($order->getFinalPrice());
            $order->setStatus(OrderStatus::CANCELED);
            $this->orderRepository->add($order, true);
            $this->entityManager->commit();
            $this->entityManager->flush();
            return $order;
        } catch (LogicException $e) {
            $this->entityManager->rollback();
            throw $e;
        }
    }

    /**
     * @throws LogicException
     */
    public function cancelPendingOrders(int $orderID): Order
    {
        try {
            $this->entityManager->beginTransaction();
            $order = $this->findOne($orderID);
            $this->revertOrderItems($order->getOrderItems());
            $order->setStatus(OrderStatus::FAILED);
            $this->orderRepository->add($order, true);
            $this->entityManager->commit();
            $this->entityManager->flush();
            return $order;
        } catch (Exception $e) {
            $this->entityManager->rollback();
            throw $e;
        }
    }

    /**
     * @throws LogicException
     */
    public function revertOrderItems(Collection $orderItems): void
    {
        foreach ($orderItems as $orderItem) {
            $this->productService->updateQuantity($orderItem->getProduct()->getId(), $orderItem->getQuantity());
        }
    }

    public function filter(OrderFilterQueryDto $query) : array
    {
        return $this->orderRepository->filter($query);
    }
}
