<?php

namespace App\Service;

use App\Exception\LogicException;
use Kavenegar\Exceptions\ApiException;
use Kavenegar\Exceptions\HttpException;
use Kavenegar\KavenegarApi;

class SMSService
{

    /**
     * @throws LogicException
     */
    public function sendSMS(string $receptor, string $message): void
    {

        try {

            $api = new KavenegarApi($_ENV['KAVENEGAR_API_KEY']);
            $sender = $_ENV['KAVENEGAR_SENDER'];

            $result = $api->Send($sender, $receptor, $message);

            if ($result) {
                foreach ($result as $r) {
                    echo "messageid = $r->messageid";
                    echo "message = $r->message";
                    echo "status = $r->status";
                    echo "statustext = $r->statustext";
                    echo "sender = $r->sender";
                    echo "receptor = $r->receptor";
                    echo "date = $r->date";
                    echo "cost = $r->cost";
                }
            }
        } catch (ApiException $e) {
            // در صورتی که خروجی وب سرویس 200 نباشد این خطا رخ می دهد
            throw new LogicException([
                'message' => $e->errorMessage(),
                'code' => 31_000
            ]);
        } catch (HttpException $e) {
            // در زمانی که مشکلی در برقرای ارتباط با وب سرویس وجود داشته باشد این خطا رخ می دهد
            throw new LogicException([
                'message' => $e->errorMessage(),
                'code' => 32_000
            ]);
        }

    }
}