<?php

namespace App\Service;

use App\Entity\Cart;
use App\Entity\CartItem;
use App\Entity\Product;
use App\Error\CartItemError;
use App\Exception\LogicException;
use App\Repository\CartItemRepository;

class CartItemService
{
    public function __construct(
        private readonly CartItemRepository $cartItemRepository,
    )
    {
    }

    /**
     * @throws LogicException
     */
    public function findOneBy(array $criteria, bool $orFail = true): ?CartItem
    {
        $item = $this->cartItemRepository->findOneBy($criteria);

        if (!$item && $orFail) {
            throw new LogicException(CartItemError::ITEM_NOT_FOUND);
        }

        return $item;
    }

    public function create(Cart $cart, Product $product, string $unitPrice, int $quantity = 1): CartItem
    {
        $item = new CartItem();
        $item->setCart($cart);
        $item->setProduct($product);
        $item->setQuantity($quantity);
        $item->setUnitPrice($unitPrice);
        $this->cartItemRepository->save($item, true);
        return $item;
    }

    /**
     * @throws LogicException
     */
    public function updateQuantity(CartItem $item, int $quantity): CartItem
    {
        if ($item->getQuantity() + $quantity > $item->getProduct()->getStock()) {
            throw new LogicException(CartItemError::ITEM_QUANTITY_EXCEEDS);
        }
        $item->setQuantity($item->getQuantity() + $quantity);
        $this->cartItemRepository->save($item, true);
        return $item;
    }

    public function remove(CartItem $item): void
    {
        $this->cartItemRepository->remove($item, true);
    }
}