<?php

namespace App\Service;

use App\Entity\AnnouncementList;
use App\Entity\NotifyForBackStock;
use App\Entity\Product;
use App\Repository\NotifyForBackStockRepository;
use DateTimeInterface;

class NotifierService
{

    public function __construct(
        private readonly NotifyForBackStockRepository $notifyForBackStockRepository,
    )
    {
    }

    public function notifyForBackStock(AnnouncementList $list, Product $product): void
    {

    }

    public function getLestTimeNotifyForBackStock(AnnouncementList $list, Product $product): ?DateTimeInterface
    {
        $lastNotify = $this->notifyForBackStockRepository->findOneBy([
            'announcementList' => $list,
            'product' => $product,
        ]);

        if(!$lastNotify){
            $lastNotify = new NotifyForBackStock();
            $lastNotify->setProduct($product);
            $lastNotify->setAnnouncementList($list);
            $this->notifyForBackStockRepository->add($lastNotify, true);
        }

        return $lastNotify->getNotifyAt();
    }


}