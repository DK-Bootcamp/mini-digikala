<?php

namespace App\Service;

use Predis\ClientInterface;
use Symfony\Component\Cache\Adapter\RedisAdapter;

class RedisService
{

    private static ClientInterface $client;

    private function __construct()
    {
    }


    public static function getClient(): ClientInterface
    {
        {
            if (!isset(static::$client)) {
                static::$client = RedisAdapter::createConnection(
                    'redis://REDIS_PASSWORD@redis'
                );
            }
            return static::$client;
        }
    }

}