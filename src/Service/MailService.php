<?php

namespace App\Service;

use App\Constants\EmailText;
use App\Constants\EmailSubject;
use App\Entity\Product;
use App\Entity\User;
use App\Error\MailError;
use App\Exception\LogicException;
use App\Message\EmailNotification;
use Scheb\TwoFactorBundle\Mailer\AuthCodeMailerInterface;
use Scheb\TwoFactorBundle\Model\Email\TwoFactorInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use SymfonyCasts\Bundle\VerifyEmail\Model\VerifyEmailSignatureComponents;
use SymfonyCasts\Bundle\VerifyEmail\VerifyEmailHelperInterface;

class MailService implements AuthCodeMailerInterface
{

    public function __construct(
        private readonly MailerInterface             $mailer,
        private readonly VerifyEmailHelperInterface  $verifyEmailHelper,
        private readonly UserPasswordHasherInterface $userPasswordHarsher,
        private readonly MessageBusInterface         $messageBus,
    )
    {
    }

    /**
     * @throws LogicException
     */
    public function send(Email $email): void
    {
        try {
            $this->mailer->send($email);
        } catch (TransportExceptionInterface) {
            throw new LogicException(MailError::MAIL_NOT_SEND);
        }
    }

    /**
     * @throws LogicException
     */
    public function sendEmail(array $infos, bool $async = true): void
    {
        if ($async) {
            $this->messageBus->dispatch(new EmailNotification($infos));
        } else {
            $email = $this->createEmail($infos);
            $this->send($email);
        }
    }

    public function createEmail(array $infos): Email
    {
        $email = new Email();
        foreach ($infos as $func => $arg) {
            $email->{$func}($arg);
        }

        return $email;
    }

    public function getSignatureComponent(User $user): VerifyEmailSignatureComponents
    {
        return $this->verifyEmailHelper->generateSignature(
            'app_register_verify_email',
            $this->userPasswordHarsher->hashPassword($user, $user->getId()),
            $user->getEmail(),
            ['id' => $user->getId()]
        );
    }

    public function createVerifyingEmailInfos(User $user): array
    {
        $signatureComponents = $this->getSignatureComponent($user);

        return [
            'to' => $user->getEmail(),
            'subject' => EmailSubject::VERIFYING_EMAIL_ADDRESS,
            'text' => EmailText::VERIFYING_EMAIL_ADDRESS . $signatureComponents->getSignedUrl(),
        ];
    }

    public function createAuthCodeEmail(TwoFactorInterface $user): array
    {
        return [
            'to' => $user->getEmailAuthRecipient(),
            'subject' => EmailSubject::OTP,
            'text' => EmailText::OTP . $user->getEmailAuthCode(),
        ];
    }

    /**
     * @throws LogicException
     */
    public function sendAuthCode(TwoFactorInterface $user): void
    {
        $emailInfos = $this->createAuthCodeEmail($user);
        $this->sendEmail($emailInfos);
    }

    public function generateVoucherCodeEmailText(int $amount, string $code, string $expireAt, ?int $minOriginalTotalPrice): string
    {
        $message = "$amount تومان تخفیف مینی دیجیکالا!" . PHP_EOL .
            "کد اختصاصی برای حساب کاربری شما: $code";
        if ($minOriginalTotalPrice) {
            $message = $message . PHP_EOL . "حداقل خرید $minOriginalTotalPrice تومان";
        }
        return $message . PHP_EOL . "مهلت: $expireAt";
    }

    public function generateNotifyBackStockMail(User $user, Product $product): array
    {
        return [
            'to' => $user->getEmail(),
            'subject' => 'موجود شدن' . $product->getName(),
            'text' => 'تعدادی' . $product->getName() . 'در مینی دیجیکالا موجود شده است',
        ];
    }
}