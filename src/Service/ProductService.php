<?php

namespace App\Service;

use App\Entity\Comment;
use App\DTO\Product\ProductFilterQueryDto;
use App\DTO\Product\ProductSearchQueryDto;
use App\Entity\Product;
use App\Error\ProductError;
use App\Event\UpdateProductStockEvent;
use App\Exception\LogicException;
use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class ProductService
{
    public function __construct(
        private readonly ProductRepository        $productRepository,
        private readonly EventDispatcherInterface $eventDispatcher,
    )
    {
    }


    /**
     * @throws LogicException
     */
    public function findOne(int $id, bool $orFail = true): ?Product
    {
        $product = $this->productRepository->find($id);
        if ($orFail && !$product) {
            throw new LogicException(ProductError::PRODUCT_NOT_FOUND);
        }
        return $product;
    }

    /**
     * @throws LogicException
     */
    public function findOneBy(array $criteria, bool $orFail = true): ?Product
    {
        $product = $this->productRepository->findOneBy($criteria);
        if ($orFail && !$product) {
            throw new LogicException(ProductError::PRODUCT_NOT_FOUND);
        }
        return $product;
    }

    public function findAll(): Collection
    {
        return new ArrayCollection($this->productRepository->findAll());
    }

    public function addComment(Product $product, Comment $comment, $flush = false): void
    {
        $product->addComment($comment);
        if ($flush) {
            $this->productRepository->add($product);
        }
    }

    public function search(ProductSearchQueryDto $query): Collection
    {
        return new ArrayCollection($this->productRepository->search($query));
    }

    /**
     * @throws LogicException
     */
    public function updateQuantity(int $id, int $changeNumber): Product
    {
        $product = $this->findOne($id);

        $currentQuantity = $product->getStock();
        $newStock = $currentQuantity + $changeNumber;
        $product->setStock($newStock);

        $newQuantity = $product->getStock();
        if ($newQuantity < 0) {
            throw new LogicException(ProductError::PRODUCT_NEGATIVE_QUANTITY);
        }
        $this->productRepository->add($product, true);

        $this->eventDispatcher
            ->dispatch(
                new UpdateProductStockEvent($product, $currentQuantity),
                UpdateProductStockEvent::NAME
            );

        return $product;
    }

    public function filter(ProductFilterQueryDto $query): Collection
    {
        return new ArrayCollection($this->productRepository->filter($query));
    }
}
