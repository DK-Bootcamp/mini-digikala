<?php

namespace App\Service;

use App\DTO\Address\CreateAddressDto;
use App\Entity\Address;
use App\Entity\User;
use App\Error\AddressError;
use App\Exception\LogicException;
use App\Repository\AddressRepository;

class AddressService
{
    public function __construct(
        private readonly AddressRepository $addressRepository,
    )
    {
    }

    /**
     * @throws LogicException
     */
    public function findOrCreate(User $user, CreateAddressDto $data): Address
    {
        return $this->findOneBy([
            'user' => $user,
            'details' => $data->details,
            'postalCode' => $data->postalCode,
            'latitude' => $data->latitude,
            'longitude' => $data->longitude,
        ], false) ?? $this->create($user, $data);

    }

    public function create(User $user, CreateAddressDto $data): Address
    {
        $address = new Address();
        $address->setUser($user);
        $address->setDetails($data->details);
        $address->setPostalCode($data->postalCode);
        $address->setLatitude($data->latitude);
        $address->setLongitude($data->longitude);
        $this->addressRepository->add($address, true);
        return $address;
    }

    public function findBy(array $criteria, array $orderBy = null): array
    {
        return $this->addressRepository->findBy($criteria, $orderBy);
    }

    /**
     * @throws LogicException
     */
    public function findOne(int $id, bool $orFail = true): ?Address
    {
        $address = $this->addressRepository->find($id);
        if ($orFail && !$address) {
            throw new LogicException(AddressError::ADDRESS_NOT_FOUND);
        }
        return $address;
    }

    /**
     * @throws LogicException
     */
    public function findOneBy(array $criteria, bool $orFail = true): ?Address
    {
        $address = $this->addressRepository->findOneBy($criteria);
        if ($orFail && !$address) {
            throw new LogicException(AddressError::ADDRESS_NOT_FOUND);
        }
        return $address;
    }
}