<?php

namespace App\Service;

use App\DTO\Category\CategoryFilterQueryDto;
use App\Entity\Category;
use App\Error\CategoryError;
use App\Exception\LogicException;
use App\Repository\CategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class CategoryService
{
    public function __construct(private readonly CategoryRepository $categoryRepository)
    {
    }


    /**
     * @throws LogicException
     */
    public function findOne(int $id, bool $orFail = true): Category
    {
        $category = $this->categoryRepository->find($id);

        if (!$category && $orFail) {
            throw new LogicException(CategoryError::CATEGORY_NOT_FOUND);
        }

        return $category;
    }

    /**
     * @throws LogicException
     */
    public function findProducts(int $id): Collection
    {
        $category = $this->findOne($id);
        return $category->getProducts();
    }

    public function findCategories(): Collection
    {
        return new ArrayCollection($this->categoryRepository->findAll());
    }

    public function filter(CategoryFilterQueryDto $query, int $id): array {
        return $this->categoryRepository->filter($query, $id);
    }

}
