<?php

namespace App\Service;

use App\Entity\Cart;
use App\Entity\Order;
use App\Entity\OrderItem;
use App\Exception\LogicException;
use App\Repository\OrderItemRepository;

class OrderItemService
{
    public function __construct(
        private readonly OrderItemRepository $orderItemRepository,
        private readonly ProductService $productService
    )
    {
    }

    /**
     * @throws LogicException
     */
    public function create(Order $order, Cart $cart): Order
    {
        foreach ($cart->getCartItems() as $cartItem) {
            $orderItem = new OrderItem();
            $orderItem->setInvoice($order);
            $orderItem->setProduct($cartItem->getProduct());
            $orderItem->setQuantity($cartItem->getQuantity());
            $orderItem->setUnitPrice($cartItem->getUnitPrice());
            $this->productService->updateQuantity($cartItem->getProduct()->getId(), -$cartItem->getQuantity());
            $order->addOrderItem($this->save($orderItem));
        }
        return $order;
    }

    public function save(OrderItem $orderItem): OrderItem
    {
        $this->orderItemRepository->add($orderItem, flush: true);
        return $orderItem;
    }
}