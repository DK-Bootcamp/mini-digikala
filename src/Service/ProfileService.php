<?php

namespace App\Service;


use App\DTO\Profile\UserOrdersSearchQueryDto;
use App\Entity\User;

class ProfileService
{

    public function __construct(
        private readonly OrderService   $orderService,
        private readonly AddressService $addressService,
    )
    {
    }

    public function findUserOrders(User $user, UserOrdersSearchQueryDto $query): array
    {
        $criteria = ['user' => $user];
        if ($query->status) {
            $criteria['status'] = $query->status;
        }

        return $this->orderService->findBy(
            $criteria,
            ['createdAt' => 'DESC']
        );
    }

    public function findUserAddresses(User $user): array
    {
        return $this->addressService->findBy(
            ['user' => $user],
            ['createdAt' => 'DESC']
        );
    }

}