<?php

namespace App\Message;

class EmailNotification
{

    public function __construct(
        private readonly array $infos,
    )
    {
    }

    public function getInfos(): array
    {
        return $this->infos;
    }
}