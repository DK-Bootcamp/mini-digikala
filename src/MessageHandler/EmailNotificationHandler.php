<?php

namespace App\MessageHandler;

use App\Message\EmailNotification;
use App\Service\MailService;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
class EmailNotificationHandler
{

    public function __construct(
        private readonly MailService     $mailService,
        private readonly MailerInterface $mailer,
    )
    {
    }

    public function __invoke(EmailNotification $emailNotification)
    {
        $emailInfos = $emailNotification->getInfos();
        $email = $this->mailService->createEmail($emailInfos);
        $this->mailer->send($email);
    }

}