<?php

namespace App\Form;

use App\Entity\User;
use App\Error\UserError;
use App\Validator\PhoneNumber\PhoneNumber;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Unique;

class UserRegistrationFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('phoneNumber', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => UserError::NOT_PHONE_NUMBER_ENTER
                    ]),
                    new PhoneNumber(),
                ]
            ])
            ->add('plainPassword', PasswordType::class, [
                'mapped' => false,
                'attr' => ['autocomplete' => 'new-password'],
                'constraints' => [
                    new NotBlank([
                        'message' => UserError::NOT_PASSWORD_ENTER,
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => UserError::WEAK_PASSWORD,
                        'max' => 100,
                        'maxMessage' => UserError::TO_LONG_PASSWORD,
                    ]),
                ],
            ])
            ->add('email', TextType::class, [
                'constraints' => [
                    new Email([
                        'message' => UserError::NOT_CORRECT_EMAIL_FORMAT,
                    ]),
                    new NotBlank([
                        'message' => UserError::NOT_EMAIL_ENTER,
                    ]),
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
