<?php

namespace App\Model\Comment;

use App\DTO\Comment\CommentOnProductDto;
use App\DTO\Profile\UserCommentsSearchDto;
use App\Entity\Comment;
use App\Entity\Product;
use App\Entity\User;
use App\Exception\LogicException;
use App\Service\CommentService;
use App\Service\ProductService;
use App\Service\UserService;
use App\Trait\GetCriteriaFromQueryTrait;
use App\View\Comment\CommentView;

class CommentModel
{
    use GetCriteriaFromQueryTrait;

    public function __construct(
        private readonly ProductService $productService,
        private readonly UserService    $userService,
        private readonly CommentService $commentService,
        private readonly CommentView    $commentView,
    )
    {
    }

    /**
     * @throws LogicException
     */
    public function comment(User $user, int $productId, CommentOnProductDto $commentOnProductDto) : array
    {
        $product = $this->productService->findOne($productId);
        if($this->commentService->hasUserCommentedOnProductYet($user,$product)){
            $comment = $this->changeComment($user,$product,$commentOnProductDto);
        }else {
            $comment = $this->commentService->create($user, $product, $commentOnProductDto);
            $this->userService->setComment($user, $comment);
            $this->productService->addComment($product, $comment);
        }
        $this->commentService->save($comment);

        return $this->commentView->viewOneComment($comment);
    }

    /**
     * @throws LogicException
     */
    public function changeComment(
        User $user, Product $product, CommentOnProductDto $commentOnProductDto
    ): Comment
    {
        $comment = $this->commentService->findUserCommentOnProduct($user,$product);
        return $this->commentService->change($comment, $commentOnProductDto);
    }

    /**
     * @throws LogicException
     */
    public function showProductComments(int $productId): array
    {
        $product = $this->productService->findOne($productId);
        $comments = $this->commentService->getAllCommentsOfProduct($product);

        return $this->commentView->viewComments($comments);
    }

    public function showUserComments(User $user, UserCommentsSearchDto $query) : array
    {
        $criteria['user'] = $user;
        $criteria += $this->getCriteriaFromQuery($query);
        $comments = $this->commentService->findComment($criteria);
        return $this->commentView->viewComments($comments);
    }

    /**
     * @throws LogicException
     */
    public function delete(User $user, int $commentId): array
    {
        $comment = $this->commentService->findOne(
            [
                'user' => $user,
                'id' => $commentId,
            ],
        );

        $this->commentService->delete($comment);

        return [
            'message' => 'comment deleted successfully',
        ];
    }
}