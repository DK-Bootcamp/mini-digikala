<?php

namespace App\Model\User;

use App\Constants\UserMessage;
use App\DTO\User\PasswordDto;
use App\Entity\User;
use App\Service\UserService;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserResetPasswordModel
{

    /**
     * @param UserPasswordHasherInterface $userPasswordHarsher
     * @param UserService $userService
     */
    public function __construct(
        private readonly UserPasswordHasherInterface $userPasswordHarsher,
        private readonly UserService                 $userService,
    )
    {
    }

    public function reset(User $user, PasswordDto $passwordDto): array
    {
        $hashedPassword = $this->userPasswordHarsher->hashPassword($user,$passwordDto->password);

        $this->userService->resetPassword($user,$hashedPassword);

        return UserMessage::PASSWORD_RESET_SUCCESSFULLY;
    }

}