<?php

namespace App\Model\User;

use App\Constants\Dto;
use App\Entity\User;
use App\Service\UserService;
use App\View\User\UserView;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use function Symfony\Component\String\u;

class UserEditPersonalInfoModel
{

    public function __construct(
        private readonly ValidatorInterface $validator,
        private readonly UserService        $userService,
        private readonly UserView           $userView,
    )
    {
    }

    public function edit(User $user, Request $request): array
    {
        $dtoName = Dto::DTO_DIR . u($request->get('slug'))->camel()->title()->toString() . Dto::DTO_POSTFIX;
        $dto = (new $dtoName($request->toArray(), $this->validator));
        $personalInfo = $this->userService->editUserPersonalInfo($user,$dto);
        return $this->userView->viewUserPersonalInfo($personalInfo);
    }
}