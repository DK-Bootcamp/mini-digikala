<?php

namespace App\Model\Notify;

use App\Entity\AnnouncementList;
use App\Entity\Product;
use App\Exception\LogicException;
use App\Service\MailService;
use App\Service\NotifierService;
use DateTime;
use DateTimeInterface;

class NotifyUserForBackStockModel
{

    public const MIN_DAYS_BETWEEN_EACH_NOTIFY_FOR_BACK_STOCK = 1;


    public function __construct(
        private readonly NotifierService $notifierService,
        private readonly MailService     $mailService,
    )
    {
    }

    /**
     * @throws LogicException
     */
    public function notify(AnnouncementList $list, Product $product, int $prevStock): void
    {
        $lastNotify = $this->notifierService->getLestTimeNotifyForBackStock($list,$product);
        if($this->shouldNotifyUserForBackStock($lastNotify, $product, $prevStock)){
            $emailInfo = $this->mailService->generateNotifyBackStockMail($list->getUser(),$product);
            $this->mailService->sendEmail($emailInfo);
        }
    }

    public function shouldNotifyUserForBackStock(?\DateTimeInterface $lastNotify,Product $product,int $prevStock): bool
    {
        $now = new DateTime("now");
        if($lastNotify){
            return (
                $lastNotify->diff($now)->format('%a') >= self::MIN_DAYS_BETWEEN_EACH_NOTIFY_FOR_BACK_STOCK &&
                $product->getStock() &&
                !$prevStock
            );
        }
        return true;
    }

}