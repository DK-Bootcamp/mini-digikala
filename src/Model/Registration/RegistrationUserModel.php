<?php

namespace App\Model\Registration;

use App\Entity\User;
use App\Error\UserError;
use App\Exception\LogicException;
use App\Security\UserLoginFormAuthenticator;
use App\Service\FormService;
use App\Service\MailService;
use App\Service\UserService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Authentication\UserAuthenticatorInterface;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;
use SymfonyCasts\Bundle\VerifyEmail\VerifyEmailHelperInterface;

class RegistrationUserModel
{

    public ?FormInterface $form = null;

    public function __construct(
        private readonly UserService                $userService,
        private readonly FormService                $formService,
        private readonly UserAuthenticatorInterface $userAuthenticator,
        private readonly UserLoginFormAuthenticator $userLoginFormAuthenticator,
        private readonly MailService                $mailService,
    )
    {
    }


    /**
     * @throws LogicException
     */
    public function register(Request $request, bool $authenticationUserAfterRegistration = true): ?User
    {
        $user = new User();
        $this->form = $this->formService->createRegistrationUserForm($request,$user);

        if ($this->form->isSubmitted() && $this->form->isValid()){
            $user = $this->userService->register($this->form, $user);

            $verifyingEmailInfos = $this->mailService->createVerifyingEmailInfos($user);
            $this->mailService->sendEmail($verifyingEmailInfos);

            if($authenticationUserAfterRegistration) {
                $this->authenticateUserAfterRegistration($request, $user);
            }
            return $user;
        }
        return null;
    }

    public function authenticateUserAfterRegistration(Request $request,User $user): void
    {
        $this->userAuthenticator->authenticateUser(
            $user,
            $this->userLoginFormAuthenticator,
            $request
        );
    }

    public function createView(): FormView
    {
        return $this->form->createView();
    }

    /**
     * @throws LogicException
     */
    public function verifyingEmail(Request $request, VerifyEmailHelperInterface $verifyEmailHelper, EntityManagerInterface $entityManager): bool
    {
        $user = $this->userService->findUserById($request->query->get('id'));

        if(!$user){
            throw new LogicException(UserError::USER_NOT_FOUND);
        }

        try {
            $verifyEmailHelper->validateEmailConfirmation(
                $request->getUri(),
                $user->getId(),
                $user->getEmail(),
            );
        } catch (VerifyEmailExceptionInterface) {
            return false;
        }

        $user->setIsEmailVerified(true);
        $entityManager->flush();

        return true;
    }
}