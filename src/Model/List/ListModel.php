<?php

namespace App\Model\List;

use App\Constants\ListMessage;
use App\DTO\List\CreatePublicListDto;
use App\Entity\User;
use App\Exception\LogicException;
use App\Factory\ListServiceFactory;
use App\Factory\ListViewFactory;
use App\Service\ProductService;
use App\Trait\GetCriteriaFromQueryTrait;
use App\Trait\GetCriteriaFromRequestTrait;
use Symfony\Component\HttpFoundation\InputBag;
use Symfony\Component\HttpFoundation\Request;

class ListModel
{
    use GetCriteriaFromQueryTrait;

    use GetCriteriaFromRequestTrait;

    public function __construct(
        private readonly ListServiceFactory $listServiceFactory,
        private readonly ListViewFactory    $listViewFactory,
        private readonly ProductService $productService
    )
    {
    }

    public function show(Request $request,User $user): array
    {
        $request->attributes->set('user',$user);
        $criteria = $this->getCriteriaFromRequest($request);
        $activeTab = $request->get('activeTab');
        $listService = $this->listServiceFactory->getService($activeTab);
        $lists = $listService->findListsBy($criteria, ['createdAt' => 'DESC']);
        $listView = $this->listViewFactory->getViewer($activeTab);

        return $listView->view($lists);
    }

    public function deletePublicList(User $user,int $listId): array
    {
        $listService = $this->listServiceFactory->getService('public');
        $list = $listService->findOneListBy(['user' => $user, 'id' => $listId]);
        $listService->delete($list);
        return ListMessage::DELETE_SUCCESSFULLy;
    }

    public function createPublicList(User $user,CreatePublicListDto $createPublicListDto): array
    {
        $list = $this->listServiceFactory->getService('public')->create($user,$createPublicListDto);
        return $this->listViewFactory->getViewer('public')->getBaseData($list);
    }

    /**
     * @throws LogicException
     */
    public function deleteProduct(User $user, int $productId, string $activeTab, InputBag $query): array
    {
        $product = $this->productService->findOne($productId);
        $listService = $this->listServiceFactory->getService($activeTab);
        $criteria = $this->getCriteriaFromQuery($query);
        $list = $listService->getList($user,$criteria);
        $listService->deleteProduct($list,$product);
        return ListMessage::PRODUCT_DELETE_SUCCESSFULLY;
    }

    /**
     * @throws LogicException
     */
    public function addProduct(User $user, int $productId, string $activeTab, InputBag $query): array
    {
        $product = $this->productService->findOne($productId);
        $listService = $this->listServiceFactory->getService($activeTab);
        $criteria = $this->getCriteriaFromQuery($query);
        $list = $listService->getList($user, $criteria);
        $listService->addProduct($list, $product);
        return $this->listViewFactory->getViewer($activeTab)->viewOne($list);
    }
}