<?php

namespace App\Abstracts;

use App\Interfaces\GatewayInterface;
use App\Entity\Payment;
use App\Service\PaymentService;

abstract class Gateways implements GatewayInterface
{
    public ?string $apiPurchaseUrl;
    public ?string $apiPaymentUrl;
    public ?string $apiSandboxPaymentUrl;
    public ?string $apiVerificationUrl;
    public ?string $callbackUrl;
    public ?bool $sandbox;

    abstract public function __construct(PaymentService $paymentService, string $callbackUrl);

    abstract public function purchase(Payment $payment);

    abstract public function pay(Payment $payment);

    abstract public function verify(Payment $payment);
}