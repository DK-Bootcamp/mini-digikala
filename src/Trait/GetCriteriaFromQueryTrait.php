<?php

namespace App\Trait;

use App\Interfaces\QueryDtoInterface;
use Symfony\Component\HttpFoundation\InputBag;

trait GetCriteriaFromQueryTrait
{

    public function getCriteriaFromQuery(InputBag|QueryDtoInterface $query): array
    {
        $criteria = [];

        foreach($query as $field => $value){
            $criteria[$field] = $value;
        }

        return $criteria;
    }

}