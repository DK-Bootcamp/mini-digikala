<?php

namespace App\Trait;

use Symfony\Component\HttpFoundation\Request;

trait GetCriteriaFromRequestTrait
{

    public function getCriteriaFromRequest(Request $request): array
    {
        $criteria = [];

        if($user = $request->get('user')){
            $criteria = ['user' => $user];
        }

        foreach($request->query as $field => $value){
            $criteria[$field] = $value;
        }

        return $criteria;
    }

}