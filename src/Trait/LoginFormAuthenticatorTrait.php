<?php

namespace App\Trait;

use App\Constants\UserProperty;
use App\Error\LoginError;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;

trait LoginFormAuthenticatorTrait
{
    public function getUserIdentifierProperty(string $userIdentifier): string
    {

        if(preg_match('/^09[0-9]{9}$/',$userIdentifier)){
            return UserProperty::PHONE_NUMBER;
        }
        else if(preg_match("/^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/",$userIdentifier)) {
            return UserProperty::EMAIL;
        }
        throw new CustomUserMessageAuthenticationException(LoginError::INVALID_USER_IDENTIFIER);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        if($target = $this->getTargetPath($request->getSession(), $firewallName)){
            return new RedirectResponse($target);
        }

        return new RedirectResponse($this->router->generate(self::DEFAULT_TARGET_PATH));
    }

    protected function getLoginUrl(Request $request): string
    {
        return $this->router->generate(self::LOGIN);
    }

}