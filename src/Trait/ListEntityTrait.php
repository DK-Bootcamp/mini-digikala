<?php

namespace App\Trait;

use App\Entity\Product;
use App\Entity\User;
use App\Error\List\BaseListError;
use App\Exception\LogicException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

trait ListEntityTrait
{
    #[ORM\ManyToMany(targetEntity: Product::class, inversedBy: 'lists' )]
    private Collection $products;

    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    /**
     * @return Collection<int, Product>
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    /**
     * @throws LogicException
     */
    public function addProduct(Product $products): self
    {
        if ($this->products->contains($products)) {
            throw new LogicException(BaseListError::PRODUCT_ALREADY_EXIST_IN_LIST);
        }
        $this->products->add($products);

        return $this;
    }

    public function removeProduct(Product $products): self
    {
        $this->products->removeElement($products);

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}