<?php

namespace App\Error;

final class CommentError
{

    public const NOT_COMMENT_ON_PRODUCT_YET = [
        'message' => 'شما هنوز نظری در مورد این محصول ثبت نکرده اید',
        'code' => 20_000, //TODO: change code
    ];

    public const NOT_COMMENT_FIND = [
        'message' => 'کامنت مورد نظر یافت نشد',
        'code' => 20_0001, //TODO: change code
    ];
}