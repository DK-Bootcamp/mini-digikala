<?php

namespace App\Error\List;

class PublicListError extends BaseListError
{
    public const EMPTY_NAME_ERROR = 'باید برای لیست خود حتما یک اسم انتخاب کنید';
}
