<?php

namespace App\Error\List;

class BaseListError
{

    public const LIST_NOT_FOUND = [
        'message' => 'لیست مورد نظر یافت نشد',
        'code' => 1000_000,
    ];

    public const PRODUCT_ALREADY_EXIST_IN_LIST = [
        'message' => 'محصول مورد نظر در لیست موجود میباشد',
        'code' => 1000_001,
    ];

    public const PRODUCT_DOESNT_EXIST_IN_LIST = [
        'message' => 'محصول مورد نظر در لیست موجود نمیباشد',
        'code' => 100_002,
    ];

    public const LIST_ALREADY_EXIST = [
        'message' => 'شما در حال حاضر لیستی با چنین اسمی دارید',
        'code' => 100_003,
    ];
}