<?php

namespace App\Error;

class PaymentError
{
    public const PAYMENT_NOT_FOUND = [
        'message' => 'payment not found',
        'code' => 11_000
    ];

    public const PAYMENT_NOT_FINISHED = [
        'message' => 'Payment status is not finished',
        'code' => 12_000
    ];

    public const EMPTY_CART = [
        'message' => 'Cart is empty',
        'code' => 13_000
    ];

    public const CART_NOT_PAYABLE = [
        'message' => 'Cart is not payable',
        'code' => 14_000
    ];

    public const INVALID_INFO = [
        'message' => "Invalid information",
        'code' => 15_000
    ];
}