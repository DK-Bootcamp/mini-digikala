<?php

namespace App\Error;

class VoucherCodeError
{
    public const VOUCHER_CODE_NOT_FOUND = [
        'message' => 'کد تخفیف پیدا نشد',
        'code' => 17_000
    ];

}