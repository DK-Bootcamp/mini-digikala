<?php

namespace App\Error;

final class UserError
{
    public const NOT_PASSWORD_ENTER = 'لطفا رمز عبور خود را انتخاب کنید';

    public const NOT_PHONE_NUMBER_ENTER = 'لطفا تلفن همراه خود را وارد کنید';

    public const WEAK_PASSWORD = 'رمزعبور حداقل باید شامل {{ limit }} کاراکتر باشد';

    public const TO_LONG_PASSWORD = 'رمز عبور نباید شامل بیشتر از {{ limit }} کراکتر باشد';

    public const NOT_CORRECT_EMAIL_FORMAT = 'فرمت ایمیل وارد شده صحیح نمی باشد';

    public const DUPLICATED_USER_PHONE_NUMBER = 'اکانت کاربری با این شماره همراه قبلا ثبت شده است';

    public const DUPLICATED_USER_EMAIL = 'اکانت کاربری با این ایمیل قبلا ثبت شده است';

    public const USER_NOT_FOUND = [
        'message' => 'کاربر مورد نظر یافت نشد',
        'code' => 300_000,
    ];

    public const NOT_EMAIL_ENTER = 'لطفا ایمیل خود را وارد کنید';
}