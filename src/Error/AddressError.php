<?php

namespace App\Error;

final class AddressError
{
    public const ADDRESS_NOT_FOUND = [
        'message' => 'آدرس پیدا نشد',
        'code' => 10_000
    ];

}