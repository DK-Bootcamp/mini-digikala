<?php

namespace App\Error;

final class MailError
{

    public const MAIL_NOT_SEND = [
        'message' => 'خطا در ارسال ایمیل',
        'code' => 30_000
    ];

    public const VERIFYING_FAILED = [
        'message' => 'failed to verifying email',
        'code' => 30_001,
    ];
}