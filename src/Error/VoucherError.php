<?php

namespace App\Error;

final class VoucherError
{
    public const VOUCHER_NOT_FOUND = [
        'message' => 'کد تخفیف پیدا نشد',
        'code' => 18_000
    ];

    public const INVALID_VOUCHER = [
        'message' => 'کد تخفیف معتبر نیست',
        'code' => 18_001
    ];

    public const VOUCHER_IS_NOT_APPLICABLE = [
        'message' => 'کد تخفیف روی مبلغ سفارش شما قابل اعمال نیست',
        'code' => 18_002
    ];
}