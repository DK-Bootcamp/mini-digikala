<?php

namespace App\Error;

final class ProductError
{
    public const PRODUCT_NOT_FOUND = [
        'message' => 'محصول پیدا نشد',
        'code' => 15_000
    ];
    public const PRODUCT_NOT_AVAILABLE = [
        'message' => 'محصول موجود نیست',
        'code' => 15_001
    ];

    public const PRODUCT_NEGATIVE_QUANTITY = [
        'message' => 'موجودی محصول نمی تواند کمتر از صفر باشد.',
        'code' => 15_002
    ];
}