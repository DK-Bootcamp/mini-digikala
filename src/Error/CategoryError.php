<?php

namespace App\Error;

final class CategoryError
{

    public const CATEGORY_NOT_FOUND =[
        'message' => 'چنین دسته بندی وجود ندارد!',
        'code' => 13_000,
    ];
}