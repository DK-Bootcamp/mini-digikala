<?php

namespace App\Error;

final class CartError
{
    public const CART_NOT_FOUND = [
        'message' => 'سبد خرید پیدا نشد',
        'code' => 11_000
    ];

}