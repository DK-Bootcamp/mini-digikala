<?php

namespace App\Error;

class LoginError
{

    public const INVALID_CREDENTIALS = 'نام کاربری یا رمز عبور نا متعبر است';

    public const INVALID_USER_IDENTIFIER  = 'شماره یا ایمیل وارد شده نامعتبر است';

    public const NOT_SIGNED_UP_YET = 'شما هنوز ثبت نام نکرده اید';

    public const NOT_EMAIL_VERIFYING_YET = 'لطفا ابتدا ایمیل خود را تایید کنید';
}