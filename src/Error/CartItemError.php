<?php

namespace App\Error;

final class CartItemError
{
    public const ITEM_NOT_FOUND = [
        'message' => 'محصول در سبد خرید پیدا نشد',
        'code' => 12_000
    ];

    public const ITEM_QUANTITY_EXCEEDS = [
        'message' => 'تعداد موجود در سفارش بیشتر از تعداد موجودی محصول است',
        'code' => 12_001
    ];
}