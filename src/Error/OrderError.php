<?php

namespace App\Error;

final class OrderError
{

    public const ORDER_NOT_FOUND = [
        'message' => 'سفارش یافت نشد',
        'code' => 14_000,
    ];

    public const ORDER_IS_SENT = [
        'message' => 'سفارش ارسال شده است',
        'code' => 14_001
    ];
}