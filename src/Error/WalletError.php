<?php

namespace App\Error;

class WalletError
{
    public const WALLET_NOT_FOUND = [
        'message' => 'کیف پول پیدا نشد',
        'code' => 19_000
    ];

    public const WALLET_IS_NOT_ACTIVE = [
        'message' => 'کیف پول شما فعال نیست',
        'code' => 19_001,
    ];

    public const NOT_ENOUGH_BALANCE = [
        'message' => 'اعتبار کافی نیست',
        'code' => 19_002
    ];
}