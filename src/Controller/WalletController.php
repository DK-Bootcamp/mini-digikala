<?php

namespace App\Controller;

use App\DTO\Wallet\ChargePaymentDto;
use App\Entity\User;
use App\Entity\Wallet;
use App\Exception\LogicException;
use App\Service\WalletService;
use App\View\Wallet\WalletView;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use OpenApi\Attributes as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;


#[Route('wallet')]
#[OA\Tag(name: 'wallet')]
class WalletController extends AbstractController
{

    public function __construct(
        private readonly WalletService $walletService,
        private readonly WalletView    $walletView
    )
    {
    }

    #[Route('', methods: 'POST')]
    #[OA\Response(
        response: Response::HTTP_CREATED,
        description: 'Create wallet for user',
        content: new OA\JsonContent(
            ref: new Model(type: Wallet::class),
        )
    )]
    public function create(#[CurrentUser] User $user): JsonResponse
    {
        $wallet = $this->walletService->create($user);
        return $this->json($this->walletView->getBaseData($wallet));
    }

    /**
     * @throws LogicException
     */
    #[Route('/activate', methods: 'PATCH')]
    #[OA\Response(
        response: Response::HTTP_OK,
        description: 'Activate wallet status',
        content: new OA\JsonContent(
            ref: new Model(type: Wallet::class),
        )
    )]
    public function activate(#[CurrentUser] User $user): JsonResponse
    {
        $wallet = $this->walletService->activate($user);
        return $this->json($this->walletView->getBaseData($wallet));
    }

    /**
     * @throws LogicException
     */
    #[Route('/inactivate', methods: 'PATCH')]
    #[OA\Response(
        response: Response::HTTP_OK,
        description: 'Inactivate wallet status',
        content: new OA\JsonContent(
            ref: new Model(type: Wallet::class),
        )
    )]
    public function inactivate(#[CurrentUser] User $user): JsonResponse
    {
        $wallet = $this->walletService->inactivate($user);
        return $this->json($this->walletView->getBaseData($wallet));
    }

    /**
     * @throws LogicException
     */
    #[Route('/charge/payment', methods: 'POST')]
    #[OA\Response(
        response: Response::HTTP_TEMPORARY_REDIRECT,
        description: "Redirect to payment gateway",
    )]
    #[OA\RequestBody(
        content: new OA\JsonContent(
            ref: new Model(type: ChargePaymentDto::class)
        )
    )]
    #[ParamConverter('dto', ChargePaymentDto::class)]
    public function requestPayment(#[CurrentUser] User $user, ChargePaymentDto $dto): RedirectResponse
    {
        $apiUrl = $this->walletService->requestPayment($user, $dto);
        return $this->redirect($apiUrl);
    }

    /**
     * @throws LogicException
     */
    #[Route('/charge', methods: 'POST')]
    public function charge(Request $request): JsonResponse
    {
        $wallet = $this->walletService->charge($request);
        return $this->json($this->walletView->getBaseData($wallet));
    }

}