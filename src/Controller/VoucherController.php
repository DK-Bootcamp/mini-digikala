<?php

namespace App\Controller;

use App\DTO\Voucher\CreateVoucherDto;
use App\Entity\Voucher;
use App\Enum\Roles;
use App\Service\VoucherService;
use App\View\Voucher\VoucherView;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Attributes as OA;


#[Route('vouchers')]
#[OA\Tag(name: 'vouchers')]
class VoucherController extends AbstractController
{
    public function __construct(
        private readonly VoucherService $voucherService,
        private readonly VoucherView    $voucherView
    )
    {
    }

    /**
     */
    #[Route('', methods: 'POST')]
    #[IsGranted(Roles::ADMIN)]
    #[ParamConverter('dto', class: CreateVoucherDto::class)]
    #[OA\Response(
        response: Response::HTTP_CREATED,
        description: 'Create a voucher',
        content: new OA\JsonContent(
            ref: new Model(type: Voucher::class),
        )
    )]
    #[OA\RequestBody(
        content: new OA\JsonContent(
            properties: [
                new OA\Property('amount', type: 'integer'),
                new OA\Property('startAt', type: 'date'),
                new OA\Property('expireAt', type: 'date'),
                new OA\Property('minOriginalTotalPrice', type: 'integer', nullable: true),
                new OA\Property(property: 'usersId', type: "array", items: new OA\Items(type: "integer"))
            ]
        )
    )]
    public function create(CreateVoucherDto $dto): JsonResponse
    {
        $voucher = $this->voucherService->create($dto);
        return $this->json($this->voucherView->getBaseData($voucher), Response::HTTP_CREATED);
    }

}