<?php

namespace App\Controller;

use App\Service\SMSService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Attributes as OA;

#[Route(path: 'sms', name: 'app_sms_')]
#[OA\Tag(name: 'sms')]
class SMSController extends AbstractController
{

    public function __construct(
        private readonly SMSService $smsService,
    )
    {
    }

    #[Route('/send', methods: ["GET"])]
    public function sendsms(): Response
    {

        $receptor = "09152236820";
        $message = "تست پیام کوتاه";

        $this->smsService->sendSMS($receptor, $message);

        return new Response(
            "Sent",
            Response::HTTP_OK,
        );
    }
}
