<?php

namespace App\Controller;

use App\DTO\List\CreatePublicListDto;
use App\Entity\PublicList;
use App\Entity\User;
use App\Model\List\ListModel;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use OpenApi\Attributes as OA;

#[Route(path: 'lists', name: 'app_list_')]
#[OA\tag(name: 'lists')]
class ListController extends AbstractController
{

    public function __construct(
        private readonly ListModel $listModel,
    )
    {
    }

    #[OA\Response(
        response: Response::HTTP_CREATED,
        description: 'Create new public list',
        content: new OA\JsonContent(
            ref: new Model(type: PublicList::class),
        )
    )]
    #[OA\RequestBody(
        content: new OA\JsonContent(
            ref: new Model(type: CreatePublicListDto::class),
        )
    )]
    #[Route(path: '/public', name: 'create_public_list', methods: ['POST'])]
    #[ParamConverter('createPublicListDto', class: CreatePublicListDto::class)]
    public function createPublicList(#[CurrentUser] User $user, CreatePublicListDto $createPublicListDto): JsonResponse
    {
        return $this->json($this->listModel->createPublicList($user, $createPublicListDto));
    }

    #[Route(path: '/public/{listId}', name: 'delete_public_list', methods: ['DELETE'])]
    #[OA\Response(
        response: Response::HTTP_OK,
        description: 'delete the chosen public list',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(property: 'message', type: 'string'),
                new OA\Property(property: 'code', type: 'number'),
            ],
            type: 'object',
        )
    )]
    public function deletePublicList(#[CurrentUser] User $user, int $listId): JsonResponse
    {
        return $this->json($this->listModel->deletePublicList($user, $listId));
    }
}