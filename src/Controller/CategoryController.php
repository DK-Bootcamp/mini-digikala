<?php

namespace App\Controller;

use Algolia\AlgoliaSearch\Exceptions\AlgoliaException;
use Algolia\SearchBundle\SearchService;
use App\DTO\Category\CategoryAlgoliaDto;
use App\DTO\Category\CategoryFilterQueryDto;
use App\Service\CategoryService;
use App\View\Category\CategoryView;
use App\Entity\Category;
use App\Logger\AppLogger;
use Doctrine\Persistence\ManagerRegistry;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Attributes as OA;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('categories')]
#[OA\Tag(name: 'categories')]
class CategoryController extends AbstractController
{

    public function __construct(
        private readonly CategoryService $categoryService,
        private readonly CategoryView    $categoryView,
        private readonly SearchService   $searchService,
        private readonly AppLogger       $logger,
    )
    {
    }

    #[OA\Parameter(
        name: "offset",
        description: "item's offset",
        in: "query",
        required: true,
        schema: new OA\Schema(type: 'integer'),
    )]
    #[OA\Parameter(
        name: "limit",
        description: "item's max limit",
        in: "query",
        required: true,
        schema: new OA\Schema(type: 'integer'),
    )]
    #[Route('/{id}/products', methods: 'GET')]
    #[ParamConverter('query', class: CategoryFilterQueryDto::class, options: ['type' => 'query'])]
    public function filter(Request $request, CategoryFilterQueryDto $query, int $id): JsonResponse
    {
        $products = $this->categoryService->filter($query, $id);

        $ip = $request->getClientIp();

        $this->logger->logInfo('[GET] - ' . $ip . ' - /categories/' . $id . '/products - ' . 'params: offset = ' . $query->offset . ', limit = ' . $query->limit);

        return $this->json($this->categoryView->getFilteredData($products));
    }


    #[Route('', methods: 'GET')]
    #[OA\Response(
        response: Response::HTTP_OK,
        description: 'Returns all of categories',
        content: new OA\JsonContent(
            type: 'array',
            items: new OA\Items(ref: new Model(type: Category::class))
        )
    )]
    public function findCategories(): JsonResponse
    {
        $categories = $this->categoryService->findCategories();
        return $this->json($this->categoryView->getCategoriesBaseData($categories));
    }

    /**
     * @throws AlgoliaException
     */
    #[Route('/algolia', methods: ['GET'])]
    #[ParamConverter('query', class: CategoryAlgoliaDto::class, options: ['type' => 'query'])]
    #[OA\Parameter(
        name: "name",
        description: "category's name",
        in: "query",
        required: true,
        schema: new OA\Schema(type: 'string'),
    )]
    #[OA\Parameter(
        name: "name",
        description: "page's number",
        in: "query",
        required: true,
        schema: new OA\Schema(type: 'integer'),
    )]
    public function algolia(Request $request, CategoryAlgoliaDto $query): JsonResponse
    {
        $categories = $this->searchService->rawSearch(Category::class, $query->name, [
            'page' => $query->page
          ]);

        $ip = $request->getClientIp();

        $this->logger->logInfo('[GET] - ' . $ip . ' - /categories/algolia - params: name = ' . $query->name);

        return $this->json($categories);
    }
}
