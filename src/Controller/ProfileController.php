<?php

namespace App\Controller;

use App\DTO\Profile\UserCommentsSearchDto;
use App\DTO\Profile\UserOrdersSearchQueryDto;
use App\DTO\User\PasswordDto;
use App\Entity\Address;
use App\Entity\Comment;
use App\Entity\Order;
use App\Entity\PublicList;
use App\Entity\User;
use App\Enum\ListsActiveTabSlug;
use App\Enum\OrderStatus;
use App\Enum\PersonalInfoSlug;
use App\Model\Comment\CommentModel;
use App\Model\List\ListModel;
use App\Model\User\UserEditPersonalInfoModel;
use App\Model\User\UserResetPasswordModel;
use App\Service\ProfileService;
use App\Service\UserService;
use App\View\Profile\ProfileView;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use OpenApi\Attributes as OA;

#[Route(path: 'profile', name: 'app_profile_')]
#[OA\Tag(name: 'profile')]
class ProfileController extends UserBaseController
{

    public function __construct(
        private readonly ProfileService $profileService,
        private readonly ProfileView $profileView,
        private readonly CommentModel $commentModel,
        private readonly UserService $userService,
    )
    {
    }

    #[Route(path: '/personal-info', name: 'personal_info', methods: ['GET'])]
    #[OA\Response(
        response: Response::HTTP_CREATED,
        description: "Returns user's personal information",
        content: new OA\JsonContent(
            ref: new Model(type: User::class),
        )
    )]
    public function personalInfo(#[CurrentUser] User $user): JsonResponse
    {
        $userPersonalInfo = $this->userService->getUserPersonalInfo($user);
        return $this->json(
            $this->profileView->viewPersonalInfo($userPersonalInfo),
        );
    }

    #[Route(path: '/orders', name: 'show_orders', methods: 'GET')]
    #[OA\Response(
        response: Response::HTTP_OK,
        description: "Returns user's orders",
        content: new OA\JsonContent(
            type: 'array',
            items: new OA\Items(ref: new Model(type: Order::class))
        )
    )]
    #[OA\Parameter(
        name: 'status',
        description: "The field used to search user's orders by status",
        in: 'query',
        required: false,
        schema: new OA\Schema(type: 'string', enum: [OrderStatus::PROCESSING, OrderStatus::SENT, OrderStatus::DELIVERED])
    )]
    #[ParamConverter('query', class: UserOrdersSearchQueryDto::class, options: ['type' => 'query'])]
    public function findUserOrders(#[CurrentUser] User $user, UserOrdersSearchQueryDto $query): JsonResponse
    {
        $orders = $this->profileService->findUserOrders($user, $query);
        return $this->json($this->profileView->getOrdersData($orders));
    }

    #[Route(path: '/addresses', name: 'show_all_addresses', methods: 'GET')]
    #[OA\Response(
        response: Response::HTTP_OK,
        description: "Returns user's addresses",
        content: new OA\JsonContent(
            type: 'array',
            items: new OA\Items(ref: new Model(type: Address::class))
        )
    )]
    public function findUserAddresses(#[CurrentUser] User $user): JsonResponse
    {
        $addresses = $this->profileService->findUserAddresses($user);
        return $this->json($this->profileView->getAddressesData($addresses));
    }

    #[OA\Response(
        response: Response::HTTP_OK,
        description: "Show all comments of user",
        content: new OA\JsonContent(
            type: 'array',
            items: new OA\Items(ref: new Model(type: Comment::class))
        )
    )]
    #[OA\Parameter(
        name: 'id',
        description: "The field used to show desired comment",
        in: 'query',
        required: false,
        schema: new OA\Schema(type: 'int')
    )]
    #[Route(path: '/comments', name: 'show_all_comments', methods: ['GET'])]
    #[ParamConverter('query', class: UserCommentsSearchDto::class, options: ['type' => 'query'])]
    public function showAllComments(#[CurrentUser] User $user, UserCommentsSearchDto $query): JsonResponse
    {
        return $this->json(
            $this->commentModel->showUserComments($user, $query)
        );
    }

    #[OA\Response(
        response: Response::HTTP_OK,
        description: "Delete user comment",
        content: new OA\JsonContent(
            type: 'array',
            items: new OA\Items(ref: new Model(type: Comment::class))
        )
    )]
    #[Route(path: '/comments/{commentId}', name: 'delete_comment', methods: ['DELETE'])]
    public function deleteComment(
        #[CurrentUser] User $user,
        int $commentId,
    ): JsonResponse
    {
        return $this->json($this->commentModel->delete($user, $commentId));
    }

    #[Route(
        path: '/lists/{activeTab}',
        name: 'show_lists',
        requirements: ['activeTab' => 'public|favorites|announcements'],
        methods: ['GET']
    )]
    #[OA\Response(
        response: Response::HTTP_OK,
        description: 'show list or lists of chosen type',
        content: new OA\JsonContent(
            type: 'array',
            items: new OA\Items(ref: new Model(type: PublicList::class))
        )
    )]
    #[OA\Parameter(
        name: 'activeTab',
        description: "The field used to detect list type",
        in: 'path',
        required: true,
        schema: new OA\Schema(type: 'string', enum: [
            ListsActiveTabSlug::FAVORITES,
            ListsActiveTabSlug::PUBLIC,
            ListsActiveTabSlug::ANNOUNCEMENT,
        ])
    )]
    #[OA\Parameter(
        name: 'name',
        description: 'The field used to search public list that you want to show',
        in: 'query',
        required: false,
        schema: new OA\Schema(type: 'string')
    )]
    public function showLists(Request $request, ListModel $listModel, #[CurrentUser] User $user): JsonResponse
    {
        return $this->json($listModel->show($request, $user));
    }

    #[OA\Response(
        response: Response::HTTP_OK,
        description: 'editing personal info',
        content: new OA\JsonContent(
            type: 'array',
            items: new OA\Items(ref: new Model(type: User::class))
        )
    )]
    #[OA\RequestBody(
        content: new OA\JsonContent(
            properties: [
                new OA\Property(property: 'firstName', type: 'string'),
                new OA\Property(property: 'lastName', type: 'string'),
                new OA\Property(property: 'nin', type: 'string'),
                new OA\Property(property: 'birthDayDate', type: 'string'),
                new OA\Property(property: 'email', type: 'string'),
                new OA\Property(property: 'phoneNumber', type: 'string'),
            ],
            type: 'object',
        )
    )]
    #[OA\Parameter(
        name: 'slug',
        description: "The field used to edit the desired part of personal ifo",
        in: 'path',
        required: true,
        schema: new OA\Schema(type: 'string', enum: [
            PersonalInfoSlug::IDENTIFICATION_INFO,
            PersonalInfoSlug::BIRTHDAY_DATE,
            PersonalInfoSlug::EMAIL,
            PersonalInfoSlug::PHONE_NUMBER,
        ])
    )]
    #[Route(
        path: '/personal-info/{slug}',
        name: 'change_personal_info',
        requirements: ['slug' => 'identification-info|birthday-date|email|phone-number'],
        methods: ['PATCH']
    )]
    public function editPersonalInfo(#[CurrentUser] User $user, Request $request, UserEditPersonalInfoModel $userEditPersonalInfoModel): JsonResponse
    {
        return $this->json($userEditPersonalInfoModel->edit($user, $request));
    }

    #[Route(path: '/personal-info/password', name: 'changing_password', methods: ['PATCH'])]
    #[OA\Response(
        response: Response::HTTP_CREATED,
        description: 'Update product stock',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(property: 'message', type: 'string'),
                new OA\Property(property: 'code', type: 'string'),
                ]
        )
    )]
    #[OA\RequestBody(
        content: new OA\JsonContent(
            ref: new Model(type: PasswordDto::class),
        )
    )]
    #[ParamConverter('passwordDto', class: PasswordDto::class)]
    public function resetPassword(#[CurrentUser] User $user, PasswordDto $passwordDto, UserResetPasswordModel $userResetPasswordModel): JsonResponse
    {
        return $this->json($userResetPasswordModel->reset($user, $passwordDto));
    }
}
