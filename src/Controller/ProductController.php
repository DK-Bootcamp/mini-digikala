<?php

namespace App\Controller;

use Algolia\AlgoliaSearch\Exceptions\AlgoliaException;
use App\DTO\Comment\CommentOnProductDto;
use App\DTO\Product\ProductFilterQueryDto;
use App\DTO\Product\ProductSearchQueryDto;
use App\Entity\Comment;
use App\Entity\FavoritesList;
use App\DTO\Product\ProductUpdateStockDto;
use App\Entity\Product;
use App\Entity\User;
use App\Enum\ListsActiveTabSlug;
use App\Exception\LogicException;
use App\Model\Comment\CommentModel;
use App\Model\List\ListModel;
use App\Service\ProductService;
use App\View\Product\ProductView;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Attributes as OA;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Algolia\SearchBundle\SearchService;
use App\DTO\Product\ProductAlgoliaDto;
use App\Logger\AppLogger;
use Doctrine\Persistence\ManagerRegistry;

#[Route('products', name: 'app_product')]
#[OA\Tag(name: 'products')]
class ProductController extends AbstractController
{
    public function __construct(
        private readonly ProductService $productService,
        private readonly ProductView $productView,
        private readonly CommentModel $commentModel,
        private readonly SearchService $searchService,
        private readonly AppLogger $logger,
        private ManagerRegistry $doctrine
    )
    {
        $this->request = Request::createFromGlobals();
    }

    #[Route(path: '/search', name: 'basic_search', methods: 'GET')]
    #[OA\Response(
        response: Response::HTTP_OK,
        description: 'Returns matched products with search query',
        content: new OA\JsonContent(
            type: 'array',
            items: new OA\Items(ref: new Model(type: Product::class))
        )
    )]
    #[OA\Parameter(
        name: 'name',
        description: 'The field used to search products by name',
        in: 'query',
        required: true,
        schema: new OA\Schema(type: 'string')
    )]
    #[ParamConverter('query', class: ProductSearchQueryDto::class, options: ['type' => 'query'])]
    public function searching(ProductSearchQueryDto $query): JsonResponse
    {
        $products = $this->productService->search($query);
        return $this->json($this->productView->getProductsFullData($products));
    }

    #[Route('/', methods: 'GET')]
    #[ParamConverter('query', class: ProductFilterQueryDto::class, options: ['type' => 'query'])]
    #[OA\Response(
        response: Response::HTTP_OK,
        description: 'Returns all of products with filters',
        content: new OA\JsonContent(
            ref: new Model(type: Product::class),
        )
    )]
    #[OA\Parameter(
        name: "offset",
        description: "item's offset",
        in: "query",
        required: true,
        schema: new OA\Schema(type: 'integer'),
    )]
    #[OA\Parameter(
        name: "limit",
        description: "item's max limit",
        in: "query",
        required: true,
        schema: new OA\Schema(type: 'integer'),
    )]
    #[OA\Parameter(
        name: "brands[]",
        description: "add brands id",
        in: "query",
        schema: new OA\Schema(type: "array", items: new OA\Items(type: "integer"))
    )]
    #[OA\Parameter(
        name: "sortPrice",
        description: "sort based on price | values: asc, desc",
        in: "query",
        schema: new OA\Schema(type: 'string'),
    )]
    #[OA\Parameter(
        name: "minPrice",
        description: "minimum price",
        in: "query",
        schema: new OA\Schema(type: 'int'),
    )]
    #[OA\Parameter(
        name: "maxPrice",
        description: "maximum price",
        in: "query",
        schema: new OA\Schema(type: 'int'),
    )]
    #[OA\Parameter(
        name: "date",
        description: "sort based on created_at | values: new, old",
        in: "query",
        schema: new OA\Schema(type: 'string'),
    )]
    public function filterProducts(ProductFilterQueryDto $query): JsonResponse
    {
        if ($query == '') {
            $products = $this->productService->findAll();
        } else {
            $products = $this->productService->filter($query);
        }

        return $this->json($this->productView->getFilteredData($products));
    }
    /**
     * @throws AlgoliaException
     */
    #[Route('/algolia', methods: ['GET'])]
    #[ParamConverter('query', class: ProductAlgoliaDto::class, options: ['type' => 'query'])]
    #[OA\Parameter(
        name: "name",
        description: "product's name",
        in: "query",
        required: true,
        schema: new OA\Schema(type: 'string'),
    )]
    #[OA\Parameter(
        name: "name",
        description: "page's number",
        in: "query",
        required: true,
        schema: new OA\Schema(type: 'integer'),
    )]
    public function algolia(ProductAlgoliaDto $query): JsonResponse
    {

        // $em = $this->doctrine->getManagerForClass(Product::class);

        $posts = $this->searchService->rawSearch(Product::class, $query->name, [
            'page' => $query->page
          ]);

        $ip = $this->request->getClientIp();

        $this->logger->logInfo('[GET] - ' . $ip . ' - /products/algolia - params: name = ' . $query->name);

        return $this->json($posts);
    }

    /**
     * @throws LogicException
     */
    #[Route(path: '/update/stock', name: "change-stock", methods: ['POST'])]
    #[OA\Response(
        response: Response::HTTP_CREATED,
        description: 'Update product stock',
        content: new OA\JsonContent(
            ref: new Model(type: Product::class),
        )
    )]
    #[OA\RequestBody(
        content: new OA\JsonContent(
            ref: new Model(type: productUpdateStockDto::class),
        )
    )]
    #[ParamConverter('productUpdateStockDto', class: ProductUpdateStockDto::class)]
    public function changeStock(ProductUpdateStockDto $productUpdateStockDto): JsonResponse
    {
        $product = $this->productService->updateQuantity($productUpdateStockDto->productId, $productUpdateStockDto->changeNumber);
        return $this->json($this->productView->getProductFullData($product));
    }

    #[Route(path: '/detail/{id}/comments', name: 'all_comments', methods: ['GET'])]
    #[OA\Response(
        response: Response::HTTP_OK,
        description: 'Returns all comments about product with given id',
        content: new OA\JsonContent(
            type: 'array',
            items: new OA\Items(ref: new Model(type: Comment::class))
        )
    )]
    public function showAllComments(int $id): JsonResponse
    {
        return $this->json(
            $this->commentModel->showProductComments($id)
        );
    }

    #[Route(path: '/detail/{id}/comments', name: 'comment', methods: ['PUT'])]
    #[OA\Response(
        response: Response::HTTP_OK,
        description: 'Post comment on product',
        content: new OA\JsonContent(
            type: 'array',
            items: new OA\Items(ref: new Model(type: Comment::class))
        )
    )]
    #[OA\RequestBody(
        content: new OA\JsonContent(
            ref: new Model(type: CommentOnProductDto::class),
        )
    )]
    #[ParamConverter('commentOnProductDto', class: CommentOnProductDto::class)]
    public function commentOnProducts(
        #[CurrentUser] User $user,
        int                 $id,
        CommentOnProductDto $commentOnProductDto,
    ): JsonResponse
    {
        return $this->json($this->commentModel->comment($user, $id, $commentOnProductDto));
    }

    /**
     * @throws LogicException
     */
    #[Route(
        path: '/{productId}/lists/{activeTab}',
        name: 'add_to_list',
        requirements: ['activeTab' => 'public|favorites|announcements'],
        methods: ['POST']
    )]
    #[OA\Response(
        response: Response::HTTP_OK,
        description: 'Add product to chosen list',
        content: new OA\JsonContent(
            type: 'array',
            items: new OA\Items(ref: new Model(type: FavoritesList::class))
        )
    )]
    #[OA\Parameter(
        name: 'activeTab',
        description: "The field used to detect list type",
        in: 'path',
        required: true,
        schema: new OA\Schema(type: 'string', enum: [
            ListsActiveTabSlug::FAVORITES,
            ListsActiveTabSlug::PUBLIC,
            ListsActiveTabSlug::ANNOUNCEMENT,
        ])
    )]
    #[OA\Parameter(
        name: 'name',
        description: 'The field used to search public list that you want add product to(this is required if our list type is public)',
        in: 'query',
        required: false,
        schema: new OA\Schema(type: 'string')
    )]
    public function addToList(Request $request, #[CurrentUser] User $user, ListModel $listModel): JsonResponse
    {
        return $this->json($listModel->addProduct(
            $user,
            $request->get('productId'),
            $request->get('activeTab'),
            $request->query,
        ));
    }

    /**
     * @throws LogicException
     */
    #[Route(
        path: '/{productId}/lists/{activeTab}',
        name: 'delete_from_list',
        requirements: ['activeTab' => 'public|favorites|announcements'],
        methods: ['DELETE'])
    ]
    #[OA\Response(
        response: Response::HTTP_OK,
        description: "Delete product" . " from " . "chosen list",
        content: new OA\JsonContent(
            properties: [
                new OA\Property(property: 'message', type: 'string'),
                new OA\Property(property: 'code', type: 'number'),
            ],
            type: 'object',
        )
    )]
    #[OA\Parameter(
        name: 'activeTab',
        description: "The field used to detect list type",
        in: 'path',
        required: true,
        schema: new OA\Schema(type: 'string', enum: [
            ListsActiveTabSlug::FAVORITES,
            ListsActiveTabSlug::PUBLIC,
            ListsActiveTabSlug::ANNOUNCEMENT,
        ])
    )]
    #[OA\Parameter(
        name: 'name',
        description: 'The field used to search public list that you want delete product from(this is required if our list type is public)',
        in: 'query',
        required: false,
        schema: new OA\Schema(type: 'string')
    )]
    public function deleteFromList(Request $request, #[CurrentUser] User $user, ListModel $listModel): JsonResponse
    {
        return $this->json($listModel->deleteProduct(
            $user,
            $request->get('productId'),
            $request->get('activeTab'),
            $request->query,
        ));
    }
}
