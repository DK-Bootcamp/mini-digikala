<?php

namespace App\Controller;

use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

#[Route(name: 'app_security_')]
class SecurityController extends AbstractController
{

    #[Route(path: '/login', name: 'login')]
    public function userLogin(AuthenticationUtils $authenticationUtils): Response
    {
        return $this->render('security/user_login.html.twig', [
            'error' => $authenticationUtils->getLastAuthenticationError(),
            'last_userIdentifier' => $authenticationUtils->getLastUsername(),
        ]);
    }

    #[Route(path: '/admin/login', name: 'admin_login')]
    public function adminLogin(AuthenticationUtils $authenticationUtils): Response
    {
        return $this->render('security/admin_login.html.twig', [
            'error' => $authenticationUtils->getLastAuthenticationError(),
            'last_userIdentifier' => $authenticationUtils->getLastUsername(),
        ]);
    }

    /**
     * @throws Exception
     */
    #[Route(path: '/logout', name: 'user_logout')]
    public function userLogout()
    {
        throw new Exception('logout should not be execute');
    }

    /**
     * @throws Exception
     */
    #[Route(path: '/admin/logout', name: 'admin_logout')]
    public function adminLogout()
    {
        throw new Exception('logout should not be execute');
    }
}
