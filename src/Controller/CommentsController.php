<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Exception\LogicException;
use App\Service\CommentService;
use App\View\Comment\CommentView;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Attributes as OA;

#[Route('/comments', name: 'app_comment_')]
#[OA\Tag(name: 'comment')]
class CommentsController extends AbstractController
{

    public function __construct(
        private readonly CommentService $commentService,
        private readonly CommentView    $commentView,
    )
    {
    }

    /**
     * @throws LogicException
     */
    #[OA\Response(
        response: Response::HTTP_OK,
        description: "Show comment with given id",
        content: new OA\JsonContent(
            type: 'array',
            items: new OA\Items(ref: new Model(type: Comment::class))
        )
    )]
    #[Route(path: '/{commentId}', name: 'show_one', methods: ['GET'])]
    public function showOneComment(int $commentId): JsonResponse
    {
        $comment = $this->commentService->findOne(['id' => $commentId]);
        return $this->json(
            $this->commentView->viewOneComment($comment)
        );
    }
}