<?php

namespace App\Controller;

use App\DTO\Address\CreateAddressDto;
use App\Entity\Address;
use App\Entity\User;
use App\Exception\LogicException;
use App\Service\AddressService;
use App\View\Address\AddressView;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use OpenApi\Attributes as OA;

#[OA\Tag(name: 'addresses')]
#[Route(path: 'addresses', name: 'app_addresses')]
class AddressController extends AbstractController
{
    public function __construct(
        private readonly AddressService $addressService,
        private readonly AddressView    $addressView)
    {
    }

    /**
     * @throws LogicException
     */
    #[Route(path: '', name: 'create', methods: 'POST')]
    #[OA\Response(
        response: Response::HTTP_CREATED,
        description: 'Create an address',
        content: new OA\JsonContent(
            ref: new Model(type: Address::class),
        )
    )]
    #[OA\RequestBody(
        content: new OA\JsonContent(
            ref: new Model(type: CreateAddressDto::class),
        )
    )]
    #[ParamConverter('dto', class: CreateAddressDto::class)]
    public function create(#[CurrentUser] User $user, CreateAddressDto $dto): JsonResponse
    {
        $address = $this->addressService->findOrCreate($user, $dto);
        return $this->json($this->addressView->getAddressBaseData($address), Response::HTTP_CREATED);
    }
}