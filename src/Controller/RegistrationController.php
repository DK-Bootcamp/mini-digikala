<?php

namespace App\Controller;

use App\Constants\PathName;
use App\Error\MailError;
use App\Exception\LogicException;
use App\Model\Registration\RegistrationUserModel;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use SymfonyCasts\Bundle\VerifyEmail\VerifyEmailHelperInterface;

#[Route('register', name: 'app_register_')]
class RegistrationController extends AbstractController
{

    public function __construct(
        private readonly RegistrationUserModel $registrationUserModel,
    )
    {
    }

    #[Route('/user', name: 'user')]
    public function userRegistration(Request $request): Response
    {
        if ($user = $this->registrationUserModel->register($request)) {
            return $this->redirectToRoute(PathName::HOME_PAGE);
        }

        return $this->render('registration/UserRegister.html.twig', [
            'registrationForm' => $this->registrationUserModel->createView(),
        ]);
    }

    /**
     * @throws LogicException
     */
    #[Route('/verify', name: 'verify_email')]
    public function verifyUserEmail(Request $request, VerifyEmailHelperInterface $verifyEmailHelper, EntityManagerInterface $entityManager): RedirectResponse
    {
        if ($this->registrationUserModel->verifyingEmail($request, $verifyEmailHelper, $entityManager)) {
            return $this->redirectToRoute('app_profile_personal_info');
        }
        return $this->redirectToRoute('app_register_verifying_email_failed');
    }

    #[Route(path: '/verify/failed', name: 'verifying_email_failed')]
    public function verifyingEmailFailed(): JsonResponse
    {
        return $this->json(MailError::VERIFYING_FAILED);
    }
}
