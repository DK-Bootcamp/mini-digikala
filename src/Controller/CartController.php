<?php

namespace App\Controller;

use App\DTO\Cart\CartAddressDto;
use App\DTO\Cart\CartItemDto;
use App\DTO\Cart\CheckoutPaymentDto;
use App\DTO\Cart\CartVoucherDto;
use App\Entity\Cart;
use App\Entity\User;
use App\Enum\CartStatus;
use App\Exception\LogicException;
use App\Service\CartService;
use App\Service\PaymentService;
use App\View\Cart\CartView;
use App\View\Order\OrderView;
use Exception;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Attributes as OA;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;


#[Route('/cart', name: 'app_cart_')]
#[OA\Tag(name: 'cart')]
class CartController extends AbstractController
{
    public function __construct(
        private readonly CartService    $cartService,
        private readonly CartView       $cartView,
        private readonly PaymentService $paymentService,
        private readonly OrderView      $orderView,
    )
    {
    }

    /**
     * @throws LogicException
     */
    #[Route(path: '', name: 'show', methods: ['GET'])]
    #[OA\Response(
        response: Response::HTTP_OK,
        description: "Show user's open cart",
        content: new OA\JsonContent(
            ref: new Model(type: Cart::class),
        )
    )]
    public function showCart(#[CurrentUser] User $user): JsonResponse
    {
        return $this->json($this->cartView->getBaseData(
            $this->cartService->findOneBy(
                [
                    'user' => $user,
                    'status' => CartStatus::OPEN
                ],
            ),
        ));
    }

    /**
     * @throws LogicException
     */
    #[Route(path: '/add', name: 'add_item', methods: ['POST'])]
    #[OA\Response(
        response: Response::HTTP_OK,
        description: 'Add an item to cart',
        content: new OA\JsonContent(
            ref: new Model(type: Cart::class),
        )
    )]
    #[OA\RequestBody(
        content: new OA\JsonContent(
            ref: new Model(type: CartItemDto::class),
        )
    )]
    #[ParamConverter('dto', class: CartItemDto::class)]
    public function addItem(#[CurrentUser] User $user, CartItemDto $dto): JsonResponse
    {
        $cart = $this->cartService->addItem($user, $dto);
        return $this->json($this->cartView->getBaseData($cart));
    }

    /**
     * @throws LogicException
     */
    #[Route(path: '/remove', name: 'remove_item', methods: ['POST'])]
    #[OA\Response(
        response: Response::HTTP_OK,
        description: 'Remove an item from cart',
        content: new OA\JsonContent(
            ref: new Model(type: Cart::class),
        )
    )]
    #[OA\RequestBody(
        content: new OA\JsonContent(
            ref: new Model(type: CartItemDto::class),
        )
    )]
    #[ParamConverter('dto', class: CartItemDto::class)]
    public function removeItem(#[CurrentUser] User $user, CartItemDto $dto): JsonResponse
    {
        $cart = $this->cartService->removeItem($user, $dto);
        return $this->json($this->cartView->getBaseData($cart));
    }

    /**
     * @throws LogicException
     */
    #[Route(path: '/address', name: 'set_address', methods: ['POST'])]
    #[OA\Response(
        response: Response::HTTP_OK,
        description: 'Set cart address',
        content: new OA\JsonContent(
            ref: new Model(type: Cart::class),
        )
    )]
    #[OA\RequestBody(
        content: new OA\JsonContent(
            ref: new Model(type: CartAddressDto::class),
        )
    )]
    #[ParamConverter('dto', class: CartAddressDto::class)]
    public function setAddress(#[CurrentUser] User $user, CartAddressDto $dto): JsonResponse
    {
        $cart = $this->cartService->setAddress($user, $dto);
        return $this->json($this->cartView->getBaseData($cart));
    }

    /**
     * @throws LogicException
     */
    #[Route(path: '/voucher', name: 'add_voucher', methods: 'POST')]
    #[OA\Response(
        response: Response::HTTP_OK,
        description: 'Add voucher for cart',
        content: new OA\JsonContent(
            ref: new Model(type: Cart::class),
        )
    )]
    #[OA\RequestBody(
        content: new OA\JsonContent(
            ref: new Model(type: CartVoucherDto::class),
        )
    )]
    #[ParamConverter('dto', class: CartVoucherDto::class)]
    public function addVoucher(#[CurrentUser] User $user, CartVoucherDto $dto): JsonResponse
    {
        $cart = $this->cartService->addVoucher($user, $dto);
        return $this->json($this->cartView->getBaseData($cart));
    }

    /**
     * @throws LogicException
     */
    #[Route(path: '/voucher', name: 'remove_voucher', methods: 'DELETE')]
    #[OA\Response(
        response: Response::HTTP_OK,
        description: 'Remove voucher from cart',
        content: new OA\JsonContent(
            ref: new Model(type: Cart::class),
        )
    )]
    public function removeVoucher(#[CurrentUser] User $user): JsonResponse
    {
        $cart = $this->cartService->removeVoucher($user);
        return $this->json($this->cartView->getBaseData($cart));
    }

    /**
     * @throws LogicException
     */
    #[Route(path: '/check', name: 'check_cart', methods: ['POST'])]
    #[OA\Response(
        response: Response::HTTP_OK,
        description: 'Check if cart is payable',
        content: new OA\JsonContent(
            ref: new Model(type: Cart::class),
        )
    )]
    public function checkCart(#[CurrentUser] User $user): JsonResponse
    {
        list($isPayable, $cart) = $this->cartService->isCartPayable($user);
        return $this->json(array($isPayable, $this->cartView->getBaseData($cart)));
    }

    /**
     * @throws LogicException
     * @throws Exception
     */
    #[Route(path: '/checkout/payment', name: 'checkout', methods: ['POST'])]
    #[ParamConverter('dto', CheckoutPaymentDto::class)]
    #[OA\Response(
        response: Response::HTTP_TEMPORARY_REDIRECT,
        description: "Redirect to payment gateway",
    )]
    #[OA\RequestBody(
        content: new OA\JsonContent(
            ref: new Model(type: CheckoutPaymentDto::class)
        )
    )]
    public function requestPayment(#[CurrentUser] User $user, CheckoutPaymentDto $dto): JsonResponse|RedirectResponse
    {
        if ($dto->gateway == "wallet") {
            $order = $this->paymentService->checkoutPaymentWallet($user, $dto);
            return $this->json($this->orderView->getOrderBaseData($order));
        } else {
            $apiUrl = $this->paymentService->checkoutPaymentRequest($user, $dto);
            return $this->redirect($apiUrl);
        }
    }

    /**
     * @throws LogicException
     */
    #[Route(path: "/checkout", name: "verify_checkout", methods: ["POST"])]
    public function checkout(Request $request): JsonResponse
    {
        $order = $this->paymentService->checkout($request);
        return $this->json($this->orderView->getOrderBaseData($order));
    }
}