<?php

namespace App\Controller\Admin;

use App\Entity\Address;
use App\Entity\AnnouncementList;
use App\Entity\Cart;
use App\Entity\CartItem;
use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\FavoritesList;
use App\Entity\Order;
use App\Entity\OrderItem;
use App\Entity\Payment;
use App\Entity\Product;
use App\Entity\PublicList;
use App\Entity\User;
use App\Entity\UserPersonalInfo;
use App\Entity\Voucher;
use App\Entity\VoucherCode;
use Cron\CronBundle\Entity\CronJob;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminDashboardController extends AbstractDashboardController
{
    #[Route('/admin/dashboard', name: 'app_admin_dashboard')]
    public function index(): Response
    {
        return $this->render('Admin/dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Mini Digikala');
    }

    public function configureMenuItems(): iterable
    {
//        https://fontawesome.com/v4/icons/
        return [
            MenuItem::linkToDashboard('Dashboard', 'fa fa-home'),

            MenuItem::linkToCrud('Address', 'fa fa-address-card', Address::class),
            MenuItem::linkToCrud('User', 'fa fa-users', User::class),
            MenuItem::linkToCrud('User personal info', 'fa fa-info', UserPersonalInfo::class),
            MenuItem::linkToCrud('Category', 'fa fa-object-group', Category::class),
            MenuItem::linkToCrud('Product', 'fa fa-gift', Product::class),
            MenuItem::linkToCrud('Cart Item', 'fa fa-flag', CartItem::class),
            MenuItem::linkToCrud('Cart', 'fa fa-shopping-cart', Cart::class),
            MenuItem::linkToCrud('Order Item', 'fa fa-file', OrderItem::class),
            MenuItem::linkToCrud('Order', 'fa fa-shopping-basket', Order::class),
            MenuItem::linkToCrud('Payment', 'fa fa-credit-card-alt', Payment::class),
            MenuItem::linkToCrud('Comments', 'fa fa-comment', Comment::class),
            MenuItem::linkToCrud('Voucher', 'fa-duotone fa-percent', Voucher::class),
            MenuItem::linkToCrud('Voucher Code', 'fa-solid fa-hashtag', VoucherCode::class),
            MenuItem::linkToCrud('Favorites list','fa fa-heart', FavoritesList::class),
            MenuItem::linkToCrud('Announcements List', 'fa fa-bell', AnnouncementList::class),
            MenuItem::linkToCrud('Public List', 'fa fa-list', PublicList::class),
            MenuItem::linkToCrud('Cron Job', 'fa fa-shuffle', CronJob::class),
        ];
    }
}
