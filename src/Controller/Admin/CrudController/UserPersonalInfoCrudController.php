<?php

namespace App\Controller\Admin\CrudController;

use App\Entity\UserPersonalInfo;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class UserPersonalInfoCrudController extends AbstractCrudController
{

    public static function getEntityFqcn(): string
    {
        return UserPersonalInfo::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            AssociationField::new('user','User Id'),
            TextField::new('firstName','First Name'),
            TextField::new('lastName', 'Last Name'),
            DateField::new('birthDayDate', 'Birthday')->setFormat('YYYY-MM-dd'),
            TextField::new('nin','National Identification Number'),
        ];
    }
}