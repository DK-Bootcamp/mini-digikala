<?php

namespace App\Controller\Admin\CrudController;

use Cron\CronBundle\Entity\CronJob;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class CronJobCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return CronJob::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IntegerField::new('id')->onlyOnIndex(),
            TextField::new('name','Cron Job Name','')->setRequired(true),
            TextField::new('command')->setRequired(true),
            TextField::new('schedule')->setRequired(true),
            TextField::new('description')->setRequired(true),
        ];
    }
}
