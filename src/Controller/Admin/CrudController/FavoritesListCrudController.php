<?php

namespace App\Controller\Admin\CrudController;

use App\Entity\FavoritesList;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;

class FavoritesListCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return FavoritesList::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IntegerField::new('id')->onlyOnIndex(),
            AssociationField::new('user','User Id')->setRequired(true),
            AssociationField::new('products','num of products')->onlyOnIndex(),
        ];
    }
}
