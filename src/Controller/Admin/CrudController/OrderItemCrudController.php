<?php

namespace App\Controller\Admin\CrudController;

use App\Entity\OrderItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class OrderItemCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return OrderItem::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            AssociationField::new('invoice', 'Invoice ID')->setRequired(true),
            AssociationField::new('product', 'Product ID')->setRequired(true),
            TextField::new('unit_price')->setRequired(true),
            IntegerField::new('quantity')->setRequired(true),
        ];
    }
}
