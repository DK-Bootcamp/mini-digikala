<?php

namespace App\Controller\Admin\CrudController;

use App\Entity\User;
use App\Entity\UserPersonalInfo;
use App\Enum\Roles;
use App\Repository\UserPersonalInfoRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasher;

class UserCrudController extends AbstractCrudController
{

    public function __construct(
        private readonly UserPasswordHasher $passwordHasher,
    )
    {
    }

    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IntegerField::new('id')->onlyOnIndex(),
            TextField::new('phoneNumber')->setRequired(true),
            TextField::new('email')->setRequired(true),
            TextField::new('plainPassword', 'password')
                ->setFormType(PasswordType::class)->setRequired(true)->onlyWhenCreating(),
            ChoiceField::new('roles')
                ->setChoices(array_combine(['User', 'Admin', 'Seller'], [Roles::USER, Roles::ADMIN, Roles::SELLER]))
                ->allowMultipleChoices()->renderAsBadges()->setRequired(true),
            AssociationField::new('userPersonalInfo','personal info')
        ];
    }

    public function persistEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $this->encodePassword($entityInstance);
        parent::persistEntity($entityManager, $entityInstance);
    }

    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $this->encodePassword($entityInstance);
        parent::updateEntity($entityManager, $entityInstance);
    }

    private function encodePassword(User $user)
    {
        if ($user->getPlainPassword() !== null) {
            $user->setPassword($this->passwordHasher->hashPassword($user, $user->getPlainPassword()));
        }
    }
}
