<?php

namespace App\Controller\Admin\CrudController;

use App\Entity\PublicList;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class PublicListCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return PublicList::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IntegerField::new('id')->onlyOnIndex(),
            AssociationField::new('user','User Id')->setRequired(true),
            TextField::new('name')->setRequired(true),
            TextField::new('descriptions')->setRequired(true),
            AssociationField::new('products')->onlyOnIndex(),
        ];
    }
}
