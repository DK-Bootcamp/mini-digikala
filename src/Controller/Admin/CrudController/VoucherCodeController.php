<?php

namespace App\Controller\Admin\CrudController;

use App\Entity\VoucherCode;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class VoucherCodeController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return VoucherCode::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->remove(Crud::PAGE_INDEX, Action::NEW)
            ->remove(Crud::PAGE_INDEX, Action::EDIT);
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('code')->setRequired(true),
            AssociationField::new('user', 'User ID')->setRequired(true),
            AssociationField::new('voucher', 'Voucher ID')->setRequired(true),
        ];
    }

}