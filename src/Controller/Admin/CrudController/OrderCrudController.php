<?php

namespace App\Controller\Admin\CrudController;

use App\Entity\Order;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class OrderCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Order::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            AssociationField::new('user', 'User ID')->setRequired(true),
            AssociationField::new('payment', 'Payment ID')->setRequired(true),
            AssociationField::new('address', 'Address ID')->setRequired(true),
            TextField::new('status')->setRequired(true),
        ];
    }
}
