<?php

namespace App\Controller\Admin\CrudController;

use App\Entity\AnnouncementList;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;

class AnnouncementListCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return AnnouncementList::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IntegerField::new('id')->onlyOnindex(),
            AssociationField::new('user','User Id')->setRequired(true),
            AssociationField::new('products','num of products')->onlyOnIndex(),
        ];
    }
}
