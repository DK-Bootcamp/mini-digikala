<?php

namespace App\Controller\Admin\CrudController;

use App\Entity\Voucher;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;

class VoucherCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Voucher::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions->remove(Crud::PAGE_INDEX, Action::NEW);
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IntegerField::new('amount')->setRequired(true),
            IntegerField::new('minOriginalTotalPrice'),
            DateField::new('startAt')->setFormat('YYYY-MM-dd')->setRequired(true),
            DateField::new('expireAt')->setFormat('YYYY-MM-dd')->setRequired(true),
        ];
    }

}