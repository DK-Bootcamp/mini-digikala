<?php

namespace App\Controller\Admin\CrudController;

use App\Entity\Comment;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class CommentCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Comment::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            AssociationField::new('user', 'User ID')->setRequired(true),
            AssociationField::new('product', 'Product ID')->setRequired(true),
            TextField::new('descriptions')->setRequired(true),
            IntegerField::new('vote')->setRequired(true),
        ];
    }
}

