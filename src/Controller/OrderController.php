<?php

namespace App\Controller;

use App\DTO\Order\OrderFilterQueryDto;
use App\Entity\Order;
use App\Entity\User;
use App\Exception\LogicException;
use App\Service\OrderService;
use App\View\Order\OrderView;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Attributes as OA;
use Symfony\Component\Security\Http\Attribute\CurrentUser;


#[Route(path: 'orders', name: 'app_order_')]
#[OA\Tag(name: 'orders')]
class OrderController extends AbstractController
{
    public function __construct(
        private readonly OrderService $orderService,
        private readonly OrderView    $orderView,
    )
    {
    }

    #[Route('/', methods: 'GET')]
    #[ParamConverter('query', class: OrderFilterQueryDto::class, options: ['type' => 'query'])]
    #[OA\Response(
        response: Response::HTTP_OK,
        description: 'Returns all of orders with filters',
        content: new OA\JsonContent(
            ref: new Model(type: Order::class),
        )
    )]
    #[OA\Parameter(
        name: "offset",
        description: "item's offset",
        in: "query",
        required: true,
        schema: new OA\Schema(type: 'integer'),
    )]
    #[OA\Parameter(
        name: "limit",
        description: "item's max limit",
        in: "query",
        required: true,
        schema: new OA\Schema(type: 'integer'),
    )]
    #[OA\Parameter(
        name: "status",
        description: "order's status",
        in: "query",
        schema: new OA\Schema(type: 'string'),
    )]
    #[OA\Parameter(
        name: "orderTotalPrice",
        description: "order's total price | asc or desc",
        in: "query",
        schema: new OA\Schema(type: 'string'),
    )]
    #[OA\Parameter(
        name: "userId",
        description: "order's user id",
        in: "query",
        schema: new OA\Schema(type: 'integer'),
    )]
    public function filter(OrderFilterQueryDto $query): JsonResponse
    {
         return $this->json($this->orderView->getFilteredData($this->orderService->filter($query)));
    }



    /**
     * @throws LogicException
     */
    #[Route(path: '/{id}', name: 'show_one', methods: ['GET'])]
    #[OA\Response(
        response: Response::HTTP_OK,
        description: 'Returns the order with given id',
        content: new OA\JsonContent(
            ref: new Model(type: Order::class),
        )
    )]
    public function findOne(int $id): JsonResponse
    {
        $order = $this->orderService->findOne($id);
        return $this->json($this->orderView->getOrderBaseData($order));
    }

    /**
     * @throws LogicException
     */
    #[Route(path: '/{orderID}/cancel', name: 'cancel_orders', methods: ["POST"])]
    #[OA\Response(
        response: Response::HTTP_OK,
        description: "Cancel orders that are not sent",
        content: new OA\JsonContent(
            ref: new Model(type: Order::class),
        )
    )]
    public function cancelOrder(#[CurrentUser] User $user, int $orderID): JsonResponse
    {
        $order = $this->orderService->cancelOrder($user, $orderID);
        return $this->json($this->orderView->getOrderBaseData($order));
    }
}
