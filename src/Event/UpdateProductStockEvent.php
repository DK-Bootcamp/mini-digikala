<?php

namespace App\Event;

use App\Entity\Product;
use Symfony\Contracts\EventDispatcher\Event;

class UpdateProductStockEvent extends Event
{
    public const NAME = 'app.update_product_stock.event';

    public function __construct(
        private readonly Product $product,
        private readonly int     $prevStock,
    )
    {
    }

    public function getPrevStock(): int
    {
        return $this->prevStock;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }
}