<?php

namespace App\Factory;

use App\Entity\AnnouncementList;
use App\Entity\FavoritesList;
use App\Entity\PublicList;
use App\Interfaces\ListServiceInterface;
use App\Service\List\AnnouncementListService;
use App\Service\List\FavoriteListService;
use App\Service\List\PublicListService;
use Doctrine\ORM\EntityManager;

class ListServiceFactory
{

    public function __construct(
        private readonly EntityManager $entityManager
    )
    {
    }

    private array $listEntity = [
        'favorites' => FavoritesList::class,
        'public' => PublicList::class,
        'announcements' => AnnouncementList::class,
    ];

    private array $listServices = [
        'favorites' => FavoriteListService::class,
        'public' => PublicListService::class,
        'announcements' => AnnouncementListService::class,
    ];

    public function getService(string $activeTab): ListServiceInterface
    {
        return new $this->listServices[$activeTab]($this->entityManager->getRepository($this->listEntity[$activeTab]));
    }
}