<?php

namespace App\Factory;

use App\Interfaces\ListViewInterface;
use App\View\List\AnnouncementListView;
use App\View\List\FavoriteListView;
use App\View\List\PublicListView;

class ListViewFactory
{

    private array $listViews = [
        'favorites' => FavoriteListView::class,
        'public' => PublicListView::class,
        'announcements' => AnnouncementListView::class,
    ];

    public function getViewer(string $activeTab): ListViewInterface
    {
        return new $this->listViews[$activeTab]();
    }
}