<?php

namespace App\Entity;

use App\Interfaces\ListEntityInterface;
use App\Repository\FavoritesListRepository;
use App\Trait\ListEntityTrait;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: FavoritesListRepository::class)]
class FavoritesList extends BaseEntity implements ListEntityInterface
{
    use ListEntityTrait;

    #[ORM\OneToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(name: "user_id")]
    private ?User $user = null;
}
