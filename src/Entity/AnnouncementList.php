<?php

namespace App\Entity;

use App\Interfaces\ListEntityInterface;
use App\Repository\AnnouncementListRepository;
use App\Trait\ListEntityTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AnnouncementListRepository::class)]
class AnnouncementList extends BaseEntity implements ListEntityInterface
{
    use ListEntityTrait;

    #[ORM\OneToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(name: "user_id")]
    private ?User $user = null;

    #[ORM\OneToMany(mappedBy: 'announcementList', targetEntity: NotifyForBackStock::class)]
    private Collection $notifiesForBackStock;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->notifiesForBackStock = new ArrayCollection();
    }

    /**
     * @return Collection<int, NotifyForBackStock>
     */
    public function getNotifyForBackStocks(): Collection
    {
        return $this->notifiesForBackStock;
    }

    public function addNotifyForBackStock(NotifyForBackStock $notifyForBackStock): self
    {
        if (!$this->notifiesForBackStock->contains($notifyForBackStock)) {
            $this->notifiesForBackStock->add($notifyForBackStock);
            $notifyForBackStock->setAnnouncementList($this);
        }

        return $this;
    }

    public function removeNotifyForBackStock(NotifyForBackStock $notifyForBackStock): self
    {
        if ($this->notifiesForBackStock->removeElement($notifyForBackStock)) {
            if ($notifyForBackStock->getAnnouncementList() === $this) {
                $notifyForBackStock->setAnnouncementList(null);
            }
        }

        return $this;
    }
}
