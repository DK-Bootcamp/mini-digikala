<?php

namespace App\Entity;

use App\Repository\VoucherRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: VoucherRepository::class)]
class Voucher extends BaseEntity
{
    #[ORM\Column]
    private ?int $amount = null;

    #[ORM\Column(nullable: true)]
    private ?int $minOriginalTotalPrice = null;

    #[ORM\Column]
    private ?DateTimeImmutable $startAt = null;

    #[ORM\Column]
    private ?DateTimeImmutable $expireAt = null;

    #[ORM\OneToMany(mappedBy: 'voucher', targetEntity: VoucherCode::class)]
    private Collection $voucherCodes;

    public function __construct()
    {
        $this->voucherCodes = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->getId();
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getMinOriginalTotalPrice(): ?int
    {
        return $this->minOriginalTotalPrice;
    }

    public function setMinOriginalTotalPrice(?int $minOriginalTotalPrice): void
    {
        $this->minOriginalTotalPrice = $minOriginalTotalPrice;
    }

    public function getStartAt(): ?DateTimeImmutable
    {
        return $this->startAt;
    }

    public function setStartAt(DateTimeImmutable $startAt): self
    {
        $this->startAt = $startAt;

        return $this;
    }

    public function getExpireAt(): ?DateTimeImmutable
    {
        return $this->expireAt;
    }

    public function setExpireAt(DateTimeImmutable $expireAt): self
    {
        $this->expireAt = $expireAt;

        return $this;
    }

    /**
     * @return Collection<int, VoucherCode>
     */
    public function getVoucherCodes(): Collection
    {
        return $this->voucherCodes;
    }

    public function addVoucherCode(VoucherCode $voucherCode): self
    {
        if (!$this->voucherCodes->contains($voucherCode)) {
            $this->voucherCodes->add($voucherCode);
            $voucherCode->setVoucher($this);
        }

        return $this;
    }

    public function removeVoucherCode(VoucherCode $voucherCode): self
    {
        if ($this->voucherCodes->removeElement($voucherCode)) {
            // set the owning side to null (unless already changed)
            if ($voucherCode->getVoucher() === $this) {
                $voucherCode->setVoucher(null);
            }
        }

        return $this;
    }
}
