<?php

namespace App\Entity;

use App\Enum\CartStatus;
use App\Repository\CartRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;

#[ORM\Entity(repositoryClass: CartRepository::class)]
class Cart extends BaseEntity
{
    #[ORM\ManyToOne(inversedBy: 'carts')]
    private ?User $user = null;

    #[ORM\ManyToOne]
    private ?Address $address = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    private ?VoucherCode $voucherCode = null;

    #[ORM\OneToMany(mappedBy: 'cart', targetEntity: CartItem::class)]
    private Collection $cartItems;

    #[ORM\Column(type: Types::STRING, columnDefinition: "ENUM('open', 'close')")]
    private ?string $status = null;

    public function __construct()
    {
        $this->cartItems = new ArrayCollection();
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getAddress(): ?Address
    {
        return $this->address;
    }

    public function setAddress(?Address $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        if (!in_array($status, array(CartStatus::OPEN, CartStatus::CLOSE))) {
            throw new InvalidArgumentException("Invalid status");
        }
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection<int, CartItem>
     */
    public function getCartItems(): Collection
    {
        return $this->cartItems;
    }

    public function addCartItem(CartItem $cartItem): self
    {
        if (!$this->cartItems->contains($cartItem)) {
            $this->cartItems->add($cartItem);
            $cartItem->setCart($this);
        }

        return $this;
    }

    public function removeCartItem(CartItem $cartItem): self
    {
        if ($this->cartItems->removeElement($cartItem)) {
            // set the owning side to null (unless already changed)
            if ($cartItem->getCart() === $this) {
                $cartItem->setCart(null);
            }
        }

        return $this;
    }

    public function getFinalPrice(): float|int
    {
        return $this->getOriginalTotalPrice() + $this->getShipmentCost() - $this->getDiscountAmount();
    }

    public function getDiscountAmount(): int
    {
        return $this->voucherCode?->getVoucher()->getAmount() ?? 0;
    }

    public function getShipmentCost(): int
    {
        return $this->getAddress()?->estimateShipmentCost() ?? 0;
    }

    public function getOriginalTotalPrice(): float|int
    {
        $originalTotalPrice = 0;
        foreach ($this->cartItems as $item) {
            $originalTotalPrice += $item->getTotalPrice();
        }
        return $originalTotalPrice;
    }

    public function __toString(): string
    {
        return $this->getId();
    }

    public function getVoucherCode(): ?VoucherCode
    {
        return $this->voucherCode;
    }

    public function setVoucherCode(?VoucherCode $voucherCode): self
    {
        $this->voucherCode = $voucherCode;

        return $this;
    }
}
