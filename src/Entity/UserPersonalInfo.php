<?php

namespace App\Entity;

use App\Repository\UserPersonalInfoRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: UserPersonalInfoRepository::class)]
class UserPersonalInfo extends BaseEntity
{
    #[ORM\Column(length: 255, nullable: true)]
    private ?string $nin = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $firstName = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $LastName = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTime $birthDayDate = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $user = null;

    public function getNin(): ?string
    {
        return $this->nin;
    }

    public function setNin(string $nin): self
    {
        $this->nin = $nin;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->LastName;
    }

    public function setLastName(string $LastName): self
    {
        $this->LastName = $LastName;

        return $this;
    }

    public function getBirthDayDate(): ?\DateTime
    {
        return $this->birthDayDate;
    }

    public function setBirthDayDate(\DateTime $birthDayDate): self
    {
        $this->birthDayDate = $birthDayDate;

        return $this;
    }

    public function setEmail(?string $email): self
    {
        $this->getUser()->setEmail($email);

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->getUser()->getEmail();
    }

    public function setPhoneNumber(String $phoneNumber): self
    {
        $this->getUser()->setPhoneNumber($phoneNumber);

        return $this;
    }

    public function getPhoneNumber(): string
    {
        return $this->getUser()->getPhoneNumber();
    }

    public function setPassword(string $password): string
    {
        return $this->getUser()->setPassword($password);
    }

    public function getPassword(): string
    {
        return $this->getUser()->getPassword();
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function __toString(): string
    {
        return 'Personal Info';
    }
}
