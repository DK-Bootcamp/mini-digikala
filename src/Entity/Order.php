<?php

namespace App\Entity;

use App\Enum\OrderStatus;
use App\Repository\OrderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;

#[ORM\Entity(repositoryClass: OrderRepository::class)]
#[ORM\Table(name: '`order`')]
class Order extends BaseEntity
{
    #[ORM\Column(type: Types::STRING, columnDefinition: "ENUM('pending', 'processing', 'sent', 'delivered', 'canceled', 'failed')")]
    private ?string $status = null;

    #[ORM\ManyToOne(inversedBy: 'orders')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $user = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Payment $payment = null;

    #[ORM\ManyToOne(inversedBy: 'orders')]
    private ?Address $address = null;

    #[ORM\OneToMany(mappedBy: 'invoice', targetEntity: OrderItem::class)]
    private Collection $orderItems;

    #[ORM\Column(length: 255)]
    private string $totalPrice;

    #[ORM\Column(length: 255)]
    private string $discountPrice;

    #[ORM\Column(length: 255)]
    private string $shipmentPrice;

    #[ORM\Column(length: 255)]
    private string $finalPrice;

    #[ORM\Column(length: 255)]
    private string $deliveryDate;

    #[ORM\Column(length: 255)]
    private string $paymentMethod;

    #[ORM\Column(length: 255)]
    private string $transactionId;

    public function __construct()
    {
        $this->orderItems = new ArrayCollection();
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        if (!in_array($status, array(OrderStatus::PENDING, OrderStatus::PROCESSING, OrderStatus::SENT, OrderStatus::DELIVERED, OrderStatus::CANCELED, OrderStatus::FAILED))) {
            throw new InvalidArgumentException("Invalid status");
        }
        $this->status = $status;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getPayment(): ?Payment
    {
        return $this->payment;
    }

    public function setPayment(Payment $payment): self
    {
        $this->payment = $payment;

        return $this;
    }

    public function getAddress(): ?Address
    {
        return $this->address;
    }

    public function setAddress(?Address $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return Collection<int, OrderItem>
     */
    public function getOrderItems(): Collection
    {
        return $this->orderItems;
    }

    public function addOrderItem(OrderItem $orderItem): self
    {
        if (!$this->orderItems->contains($orderItem)) {
            $this->orderItems->add($orderItem);
            $orderItem->setInvoice($this);
        }

        return $this;
    }

    public function removeOrderItem(OrderItem $orderItem): self
    {
        if ($this->orderItems->removeElement($orderItem)) {
            // set the owning side to null (unless already changed)
            if ($orderItem->getInvoice() === $this) {
                $orderItem->setInvoice(null);
            }
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getTotalPrice(): string
    {
        return $this->totalPrice;
    }

    /**
     * @param string $totalPrice
     */
    public function setTotalPrice(string $totalPrice): void
    {
        $this->totalPrice = $totalPrice;
    }

    /**
     * @return string
     */
    public function getDiscountPrice(): string
    {
        return $this->discountPrice;
    }

    /**
     * @param string $discountPrice
     */
    public function setDiscountPrice(string $discountPrice): void
    {
        $this->discountPrice = $discountPrice;
    }

    /**
     * @return string
     */
    public function getShipmentPrice(): string
    {
        return $this->shipmentPrice;
    }

    /**
     * @param string $shipmentPrice
     */
    public function setShipmentPrice(string $shipmentPrice): void
    {
        $this->shipmentPrice = $shipmentPrice;
    }

    /**
     * @return string
     */
    public function getFinalPrice(): string
    {
        return $this->finalPrice;
    }

    /**
     * @param string $finalPrice
     */
    public function setFinalPrice(string $finalPrice): void
    {
        $this->finalPrice = $finalPrice;
    }

    /**
     * @return string
     */
    public function getDeliveryDate(): string
    {
        return $this->deliveryDate;
    }

    /**
     * @param string $deliveryDate
     */
    public function setDeliveryDate(string $deliveryDate): void
    {
        $this->deliveryDate = $deliveryDate;
    }

    /**
     * @param string $paymentMethod
     */
    public function setPaymentMethod(string $paymentMethod): void
    {
        $this->paymentMethod = $paymentMethod;
    }

    /**
     * @return string
     */
    public function getPaymentMethod(): string
    {
        return $this->paymentMethod;
    }

    /**
     * @param string $transactionId
     */
    public function setTransactionId(string $transactionId): void
    {
        $this->transactionId = $transactionId;
    }

    /**
     * @return string
     */
    public function getTransactionId(): string
    {
        return $this->transactionId;
    }

    public function __toString(): string
    {
        return $this->getId();
    }
}
