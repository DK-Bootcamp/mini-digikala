<?php

namespace App\Entity;

use App\Repository\NotifyForBackStockRepository;
use DateTimeInterface;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: NotifyForBackStockRepository::class)]
class NotifyForBackStock
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTimeInterface $notifyAt = null;

    #[ORM\ManyToOne(targetEntity: AnnouncementList::class,inversedBy: 'notifyForBackStocks')]
    #[ORM\JoinColumn(nullable: false)]
    private ?AnnouncementList $announcementList = null;

    #[ORM\ManyToOne(targetEntity: Product::class)]
    #[ORM\JoinColumn(nullable: false)]
    private ?Product $product = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNotifyAt(): ?DateTimeInterface
    {
        return $this->notifyAt;
    }

    public function setNotifyAt(DateTimeInterface $notifyAt): self
    {
        $this->notifyAt = $notifyAt;

        return $this;
    }

    public function getAnnouncementList(): ?AnnouncementList
    {
        return $this->announcementList;
    }

    public function setAnnouncementList(?AnnouncementList $announcementList): self
    {
        $this->announcementList = $announcementList;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }
}
