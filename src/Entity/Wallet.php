<?php

namespace App\Entity;

use App\Enum\WalletStatus;
use App\Error\WalletError;
use App\Exception\LogicException;
use App\Repository\WalletRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;

#[ORM\Entity(repositoryClass: WalletRepository::class)]
class Wallet extends BaseEntity
{
    #[ORM\OneToOne(inversedBy: 'wallet', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $user = null;

    #[ORM\Column]
    private int $balance = 0;

    #[ORM\Column(type: Types::STRING, columnDefinition: "ENUM('active', 'inactive')")]
    private ?string $status = null;

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getBalance(): int
    {
        return $this->balance;
    }

    public function setBalance(int $balance): self
    {
        $this->balance = $balance;

        return $this;
    }

    /**
     * @throws LogicException
     */
    public function checkBalance(int $amount): bool
    {
        if ($this->getBalance() < $amount){
            throw new LogicException(WalletError::NOT_ENOUGH_BALANCE);
        }
        return true;
    }

    public function updateBalance(int $amount): self
    {
        $this->setBalance($this->getBalance() + $amount);
        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        if (!in_array($status, array(WalletStatus::ACTIVE, WalletStatus::INACTIVE))) {
            throw new InvalidArgumentException("Invalid status");
        }
        $this->status = $status;

        return $this;
    }
}
