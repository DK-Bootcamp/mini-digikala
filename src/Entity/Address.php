<?php

namespace App\Entity;

use App\Constants\WarehouseCoordinates;
use App\Repository\AddressRepository;
use App\Service\RedisService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use OpenApi\Attributes as OA;


#[ORM\Entity(repositoryClass: AddressRepository::class)]
class Address extends BaseEntity
{
    #[ORM\Column(length: 255)]
    private ?string $details = null;

    #[ORM\Column(length: 10)]
    private ?string $postalCode = null;

    #[ORM\Column(type: Types::STRING)]
    private string $latitude;

    #[ORM\Column(type: Types::STRING)]
    private string $longitude;

    #[ORM\ManyToOne(inversedBy: 'addresses')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $user = null;

    #[ORM\OneToMany(mappedBy: 'address', targetEntity: Order::class)]
    private Collection $orders;

    public function __construct()
    {
        $this->orders = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->getId();
    }

    public function getDetails(): ?string
    {
        return $this->details;
    }

    public function setDetails(string $details): self
    {
        $this->details = $details;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(?string $postalCode): void
    {
        $this->postalCode = $postalCode;
    }

    public function getLatitude(): string
    {
        return $this->latitude;
    }

    public function setLatitude(string $latitude): void
    {
        $this->latitude = $latitude;
    }

    public function getLongitude(): string
    {
        return $this->longitude;
    }

    public function setLongitude(string $longitude): void
    {
        $this->longitude = $longitude;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection<int, Order>
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(Order $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders->add($order);
            $order->setAddress($this);
        }

        return $this;
    }

    public function removeOrder(Order $order): self
    {
        if ($this->orders->removeElement($order)) {
            // set the owning side to null (unless already changed)
            if ($order->getAddress() === $this) {
                $order->setAddress(null);
            }
        }

        return $this;
    }

    #[OA\Property(type: "array", items: new OA\Items(type: "string"))]
    public function getShipmentInfo(): array
    {
        $distance = $this->getDistanceFromWarehouse();
        $date = $this->estimateShipmentDate();
        $cost = $this->estimateShipmentCost();
        return [
            'distance' => $distance,
            'date' => $date,
            'cost' => $cost
        ];
    }

    public function estimateShipmentCost(): int
    {
        $distance = $this->getDistanceFromWarehouse();
        return $distance < 100 ? 35000 : ($distance < 200 ? 30000 : 20000);
    }

    public function estimateShipmentDate(): string
    {
        $distance = $this->getDistanceFromWarehouse();
        $day = $distance < 100 ? 1 : ($distance < 200 ? 2 : 3);
        $now = date('Y-m-d');
        return date('Y-m-d', strtotime($now . " + {$day} days"));
    }

    public function getDistanceFromWarehouse(): ?string
    {
        $client = RedisService::getClient();
        $client->geoadd('addressCoordinate', $this->getLongitude(), $this->getLatitude(), $this->getId());
        $client->geoadd(
            'addressCoordinate',
            WarehouseCoordinates::LONGITUDE,
            WarehouseCoordinates::LATITUDE,
            WarehouseCoordinates::NAME);
        return $client->geodist('addressCoordinate', $this->getId(), WarehouseCoordinates::NAME, 'Km');
    }
}
