<?php

namespace App\Constants;

class ListMessage
{

    public const ADD_SUCCESSFULLY = [
        'message' => 'list added successfully',
        'code' => 400000000,
    ];

    public const DELETE_SUCCESSFULLy = [
        'message' => 'list deleted successfully',
        'code' => 4000000001,
    ];

    public const PRODUCT_DELETE_SUCCESSFULLY = [
        'message' => 'product delete successfully',
        'code' => 4000000002,
    ];
}