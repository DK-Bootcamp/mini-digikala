<?php

namespace App\Constants;

final class UserProperty
{
    public const PHONE_NUMBER = 'phoneNumber';

    public const EMAIL = "email";
}