<?php

namespace App\Constants;

class PersonalInfoMessage
{
    public const NOT_NULL_FIRST_NAME = 'پر کردن فیلد اسم الزامی است';

    public const NOT_BLANK_FIRST_NAME = 'پر کردن فیلد اسم الزامی است';

    public const MIN_LENGTH_FIRST_NAME = 'فیلد اسم حداقل باید شامل {{ limit }} کاراکتر باشد';

    public const NOT_NULL_LAST_NAME = 'پر کردن فیلد نام خانوادگی الزامی است';

    public const NOT_BLANK_LAST_NAME = 'پر کردن فیلد نام خانوداگی الزامی است';

    public const MIN_LENGTH_LAST_NAME = 'فیلد نام خانوداگی اسم حداقل باید شامل {{ limit }} کاراکتر باشد';

    public const NOT_NULL_NIN = '';

    public const NOT_BLANK_NIN = '';

    public const INVALID_NIN = '';

    public const NOT_NULL_BIRTHDAY_DATE = 'پر کردن فیلد تاریخ الزامیست';

    public const NOT_BLANK_BIRTHDAY_DATE = 'پر کردن فیلد تاریخ الزامیست';

    public const INVALID_BIRTHDAY_DATE = 'تاریخ ورودی نامعتبر است';
}