<?php

namespace App\Constants;

final class WarehouseCoordinates
{
    public const LATITUDE = '35.69717218921845';
    public const LONGITUDE = '51.169607598095894';
    public const NAME = 'dk';

}