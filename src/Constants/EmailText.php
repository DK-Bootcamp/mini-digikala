<?php

namespace App\Constants;

final class EmailText
{
    public const VERIFYING_EMAIL_ADDRESS = 'برای تایید ایمیل خود روی لینک زیر کلیک کنید' . PHP_EOL;

    public const OTP = 'کدتایید:' . PHP_EOL;
}