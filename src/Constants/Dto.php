<?php

namespace App\Constants;

class Dto
{
    public const DTO_DIR = 'App\\' . 'DTO\\' . 'User\\';

    public const DTO_POSTFIX = 'Dto';
}