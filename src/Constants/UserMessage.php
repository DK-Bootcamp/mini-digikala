<?php

namespace App\Constants;

final class UserMessage
{
    public const PASSWORD_RESET_SUCCESSFULLY = [
        'message' => 'رمز عبور با موفقیت تغییر یافت',
        'code' => 9000_00,
    ];

}