<?php

namespace App\Constants;

final class EmailSubject
{
    public const VERIFYING_EMAIL_ADDRESS = ' تایید ایمیل';

    public const OTP = 'ارسال کد تایید';
}