
# Mini Digikala
An online shop with PHP language, using Symfony framework. with basic entities of login, register, product, cart, order, payment, and also significant services like search (Algolia), cache (Redis), queue (rabbitMQ), cron, SMS, and some other cool features.

More complete explanations are available in [this presentation](https://docs.google.com/presentation/d/e/2PACX-1vRe5llQmCgemckWAFzY7qNUA1-eOS728P-H2Y6CJWfx8hAROHuB1gECAHamBzBwjjK7Hu1Hvf6-e8WI/pub?start=false&loop=false&delayms=5000)

## Setup to Run

Just clone the project and run the following command:

```bash
docker compose build && docker compose up
```

The app is now available at http://localhost:8080 .

### Makefile

Magic comes to life; you can use these easily, instead of the previous command.

`make build`: build project

`make up`: up project containers

`make start`: build and up project

`make down`: stop containers

`make bash`: connect to container

### install requirements
```bash
composer install
```


#### JWT configuration
```bash
php bin/console lexik:jwt:generate-keypair
```

### swagger
List of APIs with their description and usage is documented at
 http://localhost:8080/api/doc

#### Cron job
At first you should add
`* * * * * php  /path/to/Project Directory/bin/console cron:run 1>> /dev/null 2>&1
` to your terminal crontab

then run `symfony console cron:run`

for creating cron job you should run `symfony console cron:create` and then symfony asked you about command , name for your cron job and also schedule for your cron command

`symfony console cron:start` will starting all your cron jobs and `symfony console cron:stop` will stopping all your cron jobs

for see all your cron jobs run `symfony console cron:create`

for deleting cron jobs run `symfony console cron:disable` to disable your cron jobs and then tun `symfony console cron:delete <cron job name>`

And also you can to this boring stuff, much easier with such a beautiful and simple ui in our easy admin=) 
