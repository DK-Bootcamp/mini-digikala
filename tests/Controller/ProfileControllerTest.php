<?php

namespace App\Tests\Controller;

use App\Repository\UserRepository;
use App\Tests\TestUtils;
use Exception;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class ProfileControllerTest extends WebTestCase
{
    /**
     * @throws Exception
     */
    public function testGetPersonalInfo(): void
    {
        $client = static::createClient([
            'base_url' => $_ENV['BASE_URL']
        ]);
        TestUtils::loginTestUser($client, static::getContainer()->get(UserRepository::class));
        $client->request('GET', '/profile/personal-info');
        $response = $client->getResponse();
        $personalInfo = json_decode($response->getContent(), true);

        $this->assertResponseIsSuccessful();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertIsInt($personalInfo['id']);
        $this->assertEquals('09121111111', $personalInfo['phoneNumber']);
        $this->assertEquals('testuser@gmail.com', $personalInfo['email']);
        $this->assertNull($personalInfo['firstName']);
        $this->assertNull($personalInfo['lastName']);
        $this->assertNull($personalInfo['nin']);
        $this->assertNull($personalInfo['birthDayDate']);
    }

    /**
     * @throws Exception
     */
    public function testShowAllComments(): void
    {
        $client = static::createClient([
            'base_url' => $_ENV['BASE_URL']
        ]);
        TestUtils::loginTestUser($client, static::getContainer()->get(UserRepository::class));
        $client->request('GET', '/profile/comments?id=6');
        $response = $client->getResponse();
        $userComments = json_decode($response->getContent(), true);

        $this->assertResponseIsSuccessful();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertIsInt($userComments[0]['comment_id']);
        $this->assertEquals('Ipsa vero quam et sit earum vel sit omnis. Necessitatibus expedita et est animi ratione numquam iusto perferendis. Officia qui dignissimos repellat omnis voluptatem sunt.', $userComments[0]['descriptions']);
        $this->assertEquals(3,$userComments[0]['vote']);
        $this->assertEquals('foobar',$userComments[0]['product']);
        $this->assertEquals(12,$userComments[0]['user_id']);
    }
}