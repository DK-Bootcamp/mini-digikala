<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class CategoryControllerTest extends WebTestCase
{
    public function testGetCategories(): void
    {
        $client = static::createClient([
            'base_url' => $_ENV['BASE_URL']
        ]);
        $client->request('GET', '/categories');
        $response = $client->getResponse();

        $this->assertResponseIsSuccessful();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $content = json_decode($response->getContent(), true);
        $this->assertCount(2, $content);
    }

    public function testCategoriesAlgolia(): void
    {
        $client = static::createClient([
            'base_url' => $_ENV['BASE_URL']
        ]);
        $client->request('GET', '/categories/algolia?name=E&page=0');
        $response = $client->getResponse();

        $this->assertResponseIsSuccessful();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $content = json_decode($response->getContent(), true);

        $this->assertGreaterThan(0, $content['nbHits']);
    }

    public function testCategoriesFiltering(): void
    {
        $client = static::createClient([
            'base_url' => $_ENV['BASE_URL']
        ]);
        $client->request('GET', 'categories/1/products?offset=0&limit=3');
        $response = $client->getResponse();

        $this->assertResponseIsSuccessful();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $content = json_decode($response->getContent(), true);

        $this->assertGreaterThanOrEqual(0, count($content));
    }
}
