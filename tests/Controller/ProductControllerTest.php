<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class ProductControllerTest extends WebTestCase
{
    public function testProductsAlgolia(): void
    {
        $client = static::createClient([
            'base_url' => $_ENV['BASE_URL']
        ]);
        $client->request('GET', '/products/algolia?name=ca&page=0');
        $response = $client->getResponse();

        $this->assertResponseIsSuccessful();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $content = json_decode($response->getContent(), true);

        $this->assertGreaterThan(0, $content['nbHits']);
    }

    public function testProductsFiltering(): void
    {
        $client = static::createClient([
            'base_url' => $_ENV['BASE_URL']
        ]);
        $client->request('GET', '/products/?offset=0&limit=3');
        $response = $client->getResponse();

        $this->assertResponseIsSuccessful();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $content = json_decode($response->getContent(), true);

        $this->assertGreaterThanOrEqual(0, $content);
    }
}
