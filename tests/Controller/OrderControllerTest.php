<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class OrderControllerTest extends WebTestCase
{

    public function testOrdersFiltering(): void
    {
        $client = static::createClient([
            'base_url' => $_ENV['BASE_URL']
        ]);
        $client->request('GET', '/orders/?offset=0&limit=10');
        $response = $client->getResponse();

        $this->assertResponseIsSuccessful();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $content = json_decode($response->getContent(), true);

        $this->assertGreaterThanOrEqual(0, count($content));
    }
}
