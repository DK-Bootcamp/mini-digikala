<?php

namespace App\Tests\Controller;

use App\Enum\Roles;
use App\Repository\UserRepository;
use App\Repository\VoucherCodeRepository;
use App\Tests\Factory\UserFactory;
use App\Tests\TestUtils;
use Exception;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use function Zenstruck\Foundry\faker;

class VoucherControllerTest extends WebTestCase
{
    /**
     * @throws Exception
     */
    public function testCreateVoucher(): void
    {
        $client = static::createClient([
            'base_url' => $_ENV['BASE_URL']
        ]);
        TestUtils::loginTestUser($client, static::getContainer()->get(UserRepository::class));

        $now = date('Y-m-d');
        $twoDaysLater = date('Y-m-d', strtotime($now . " + 3 days"));
        $user = UserFactory::createOne([
            'roles' => [Roles::USER],
            'phoneNumber' => '09121111112',
            'email' => faker()->email()
        ]);
        $requestBody = [
            'amount' => 30000,
            'startAt' => $now,
            'expireAt' => $twoDaysLater,
            'minOriginalTotalPrice' => 100000,
            'usersId' => [$user->getId()]
        ];
        $client->request('POST', '/vouchers', content: json_encode($requestBody));
        $response = $client->getResponse();
        $voucher = json_decode($response->getContent(), true);

        $this->assertResponseIsSuccessful();
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
        $this->assertIsInt($voucher['id']);
        $this->assertEquals($requestBody['amount'], $voucher['amount']);
        $this->assertEquals($requestBody['startAt'], $voucher['startAt']);
        $this->assertEquals($requestBody['expireAt'], $voucher['expireAt']);
        $this->assertEquals($requestBody['minOriginalTotalPrice'], $voucher['minOriginalTotalPrice']);

        $voucherCodeRepository = static::getContainer()->get(VoucherCodeRepository::class);
        $voucherCode = $voucherCodeRepository->findOneBy(['user' => $user->getId()]);
        $this->assertNotNull($voucherCode->getCode());
        $this->assertEquals($user->getId(), $voucherCode->getUser()->getId());
        $this->assertEquals($voucher['id'], $voucherCode->getVoucher()->getId());
    }
}
