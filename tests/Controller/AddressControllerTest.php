<?php

namespace App\Tests\Controller;

use App\Repository\UserRepository;
use App\Tests\TestUtils;
use Exception;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class AddressControllerTest extends WebTestCase
{
    /**
     * @throws Exception
     */
    public function testCreateAddress()
    {
        $client = static::createClient([
            'base_url' => $_ENV['BASE_URL']
        ]);
        TestUtils::loginTestUser($client, static::getContainer()->get(UserRepository::class));

        $requestBody = [
            'details' => 'test address',
            'postalCode' => 1111111111,
            'latitude' => '13.361389',
            'longitude' => '38.115556'
        ];
        $client->request('POST', '/addresses', content: json_encode($requestBody));
        $response = $client->getResponse();
        $address = json_decode($response->getContent(), true);

        $this->assertResponseIsSuccessful();
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
        $this->assertIsInt($address['id']);
        $this->assertIsInt($address['userId']);
        $this->assertEquals($requestBody['details'], $address['details']);
        $this->assertEquals($requestBody['postalCode'], $address['postalCode']);
        $this->assertEquals($requestBody['latitude'], $address['latitude']);
        $this->assertEquals($requestBody['longitude'], $address['longitude']);

    }
}
