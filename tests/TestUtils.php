<?php

namespace App\Tests;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;

class TestUtils
{
    public static function loginTestUser(KernelBrowser $client, UserRepository $userRepository): void
    {
        $testUser = $userRepository->findOneBy(['email' => 'testuser@gmail.com']);
        $client->loginUser($testUser);
    }
}