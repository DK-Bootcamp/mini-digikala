<?php

namespace App\Tests\Factory;

use App\Entity\PublicList;
use App\Repository\PublicListRepository;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @extends ModelFactory<PublicList>
 *
 * @method static PublicList|Proxy createOne(array $attributes = [])
 * @method static PublicList[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static PublicList|Proxy find(object|array|mixed $criteria)
 * @method static PublicList|Proxy findOrCreate(array $attributes)
 * @method static PublicList|Proxy first(string $sortedField = 'id')
 * @method static PublicList|Proxy last(string $sortedField = 'id')
 * @method static PublicList|Proxy random(array $attributes = [])
 * @method static PublicList|Proxy randomOrCreate(array $attributes = [])
 * @method static PublicList[]|Proxy[] all()
 * @method static PublicList[]|Proxy[] findBy(array $attributes)
 * @method static PublicList[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static PublicList[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static PublicListRepository|RepositoryProxy repository()
 * @method PublicList|Proxy create(array|callable $attributes = [])
 */
final class PublicListFactory extends ModelFactory
{
    public function __construct()
    {
        parent::__construct();

        // TODO inject services if required (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services)
    }

    protected function getDefaults(): array
    {
        return [
            // TODO add your default values here (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories)
            'name' => self::faker()->text(10),
            'descriptions' =>self::faker()->text(),
            'user' => UserFactory::createOne(),
        ];
    }

    protected function initialize(): self
    {
        // see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
        return $this
            // ->afterInstantiate(function(PublicList $publicList): void {})
        ;
    }

    protected static function getClass(): string
    {
        return PublicList::class;
    }
}
